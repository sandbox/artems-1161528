<?php

/**
 * @file
 * GML Course system
 * 
 * Courses list display for admin
 *
 */

/**
 *   Display courses list
 *
 *   @return - page
 */ 
 function courses_display($record = NULL) {
  $output = common_html_small_font_class( true );
  $output .= common_html_bordered_intable_links_class(true);
  $output .= drupal_get_form('courses_display_form', $record);
  $output .= common_html_bordered_intable_links_class(false);
  $output .= common_html_small_font_class( false );

  return $output;
}

/**
 *   Display courses list form
 *
 *   @return - form
 */ 
function courses_display_form(&$form_state, $record) {
  $flag_access_maintainer = user_access('administer courses');

  function cmp_state($a, $b) {
    if ($a['active'] == $b['active']) {
      return 0;
    }
    return ($a['active'] < $b['active']) ? 1 : -1;
  }

  $courses = course_fetch_courses();
  // sort courses (first active, then inactive)

  usort($courses, 'cmp_state');

  $form = array();

  foreach ($courses as $i => $course) {
    $form[$i]['title'] = array(
      '#type' => 'markup',
      '#value' => l($course['title'], 'course/' . $course['id']),
    );

    $form[$i]['weight'] = array(
      '#type' => 'markup',
      '#value' => $course['weight'],
    );

    $form[$i]['active'] = array(
      '#type' => 'value',
      '#value' => $course['active'],
    );

    $form[$i]['reg_open'] = array(
      '#type' => 'markup',
      '#value' => common_get_colored_yesno( $course['reg_open']),
    );


    //display links for administator
    if ( $flag_access_maintainer ) {
      $form[$i]['assigns'] = array(
        '#value' => l(t('assignments'), 'course/' . $course['id'] . '/assign'),
      );

      $form[$i]['groups'] = array(
        '#value' => l(t('groups'), 'course/' . $course['id'] . '/group'),
      );

      $form[$i]['students'] = array(
        '#value' => l(t('students'), 'course/' . $course['id'] . '/student'),
      );

      $form[$i]['points'] = array(
        '#value' => l(t('points'), 'course/' . $course['id'] . '/points'),
      );

      $form[$i]['edit'] = array(
        '#value' => l(t('edit'), 'course/' . $course['id'] . '/edit'),
      );

      $form[$i]['delete'] = array(
        '#value' => l(t('delete'), 'course/' . $course['id'] . '/delete'),
      );
    }
    else {
      $form[$i]['assigns'] = array(
        '#value' => l(t('assignments'), 'course/' . $course['id'] ),
      );

      $form[$i]['students'] = array(
        '#value' => l(t('students'), 'course/' . $course['id'] . '/student'),
      );
    }
  }

  return $form;
}

/**
 *   Display courses list form - theme function
 *
 *   @return - themed form
 */ 
 function theme_courses_display_form($form) {
  $flag_access_maintainer = user_access('administer courses');
  //load custom css
  drupal_add_css(drupal_get_path('module', 'gml_course') . '/misc/gml_course.css');

  $prefix_once = '<div style="width: 400px;">';
  $postfix_once = '</div>';

  $last_status = -1;
  foreach ($form as $name => $element) {
    if (isset($element['title']) && is_array($element['title'])) {
      $status = $element['active']['#value'];
	  //displays status camption in table
      if ($status != $last_status) {
        if ( $flag_access_maintainer ) {
          $colspan = 9;
        }
        else {
          $colspan = 3;
        }
        $rows[] = array(array(
            'data' => $status ? t('Active') : t('Inactive'),
            'class' => 'state',
            'colspan' => $colspan,
          ));
        $last_status = $status;
      }

      $course_title = drupal_render($element['title']);

      if ( !$flag_access_maintainer ) {
        $course_title = $prefix_once . $course_title . $postfix_once;
      }

      $currentrow = array(
        array(
          'data' => $course_title,
          'class' => 'course',
        ),
      );

      if ( $flag_access_maintainer ) {
        array_push( $currentrow, drupal_render($element['weight']),  
							drupal_render($element['reg_open']), 
							drupal_render($element['assigns']), 
							drupal_render($element['groups']), 
							drupal_render($element['students']), 
						    drupal_render($element['points']), 
							drupal_render($element['edit']), 
							drupal_render($element['delete']) );
      }
      else {
        array_push( $currentrow, drupal_render($element['assigns']), 
							drupal_render($element['students']) );
      }

      $rows[] = $currentrow;

      unset($form[$name]);
    }
  }

  $header = array(t('Title'));

  if ( $flag_access_maintainer ) {
    array_push( $header, t('Weight'), t('Registration'), array('data' => t('Components'), 'colspan' => 4), array('data' => t('Operations'), 'colspan' => 3) );
  }
  else {
    array_push( $header, array('data' => t('Details'), 'colspan' => 2) );
  }

  $output = theme('table', $header, $rows,   array('id' => 'courses'));
  $output .= drupal_render($form);

  return $output;
}

/**
 *   Course display page
 *	 Displays different information about current selected course
 *
 *   @return - page
 */ 
 function course_view($in_course_id) {

  $flag_access_maintainer = user_access('administer courses');

  $hr_style = '<div style="margin: 0px 23% 4px 0px; height: 1px; background-color: #000000;"></div> <br>';

  $course = course_fetch_course($in_course_id);

  $output = common_html_small_font_class( true );
  $output .= '<h2 class="title">' . t('General info') . '</h2>';
  $output .= '<dl>';
  $output .= '<dt>' . t('Title') . '</dt><dd><h3><font color="#000000">&laquo;' . $course['title'] . '&raquo;</font></h3></dd>';
  $output .= '<dt>' . t('Short title') . '</dt><dd><h3><font color="#000000">&laquo;' . $course['short_title'] . '&raquo;</font></h3></dd>';
  $output .= '<dt>' . t('Registration open') . '</dt><dd><h3>' . common_get_colored_yesno( $course['reg_open'] )  . '</h3></dd>';
  $output .= '<dt>' . t('Registration keyword') . '</dt><dd><h3><font color="#000000">&laquo;' . $course['reg_keyword'] . '&raquo;</font></h3></dd>';
  $output .= '</dl>' . $hr_style;



  $output .= '<h2 class="title">' . t('Assignments') . '</h2>';

  $output .= '<p>';
  $output .= assigns_generic_display($in_course_id);
  $output .= '</p>';

  //Show group for admin
  if ( $flag_access_maintainer ) {
    $output .= '<br><br>' . $hr_style;

    $output .= '<h2 class="title">' . t('Groups') . '</h2>';
    $output .= '<p>';
    $output .= groups_admin_display($in_course_id);
    $output .= '</p>';
  }

  $output .= common_html_small_font_class( false );

  return $output;
}

/**
 *   Course display delete page
 *
 *   @return - page
 */ 
function course_delete($in_course_id = NULL) {
  return drupal_get_form('course_delete_form', $in_course_id);
}

/**
 *   Course display delete confirm form
 *
 *   @return - form
 */ 
function course_delete_form(&$form_state, $in_course_id) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  $form['course_id'] = array(
    '#type' => 'value',
    '#value' => $in_course_id,
  );
  $course = course_fetch_course($in_course_id);

  return confirm_form(
    $form,  
    t('Are you sure you want to delete the course "%name"?', array('%name' => $course['title'])),  
    'course', 
    '',  
    t('Delete'),  
    t('Cancel'));

}

/**
 *   Course display delete confirm form - submit handler
 *
 *  
 */ 
 function course_delete_form_submit($form, &$form_state) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  } 

  course_delete_from_db( $form_state['values']['course_id'] );

  drupal_flush_all_caches();

  drupal_goto('course');
}

/**
 *   Course add/edit display 
 *
 *  @return - page
 */ 
function course_add_edit($in_course_id = NULL) {
  $output = drupal_get_form('course_add_edit_form', $in_course_id);

  return $output;
}

/**
 *   Course add/edit display form
 *
 *  @return - form
 */ 
function course_add_edit_form(&$form_state, $in_course_id) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  if ($in_course_id) {
    $course = course_fetch_course($in_course_id);
    $form['id'] = array(
      '#type' => 'value',
      '#value' => $in_course_id,
    );
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Course title'),
    '#default_value' => $course['title'],
    '#required' => TRUE,
  );

  $form['short_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Short Title'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Course short title'),
    '#default_value' => $course['short_title'],
    '#required' => FALSE,
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#description' => t('Course weight'),
    '#default_value' => $course['weight'],
    '#required' => FALSE,
  );

  $form['reg_open'] = array(
    '#type' => 'checkbox',
    '#title' => t('Registration open'),
    '#description' => t('Allow users regisrations'),
    '#default_value' => $course['reg_open'],
    '#required' => FALSE,
  );

  $form['reg_keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('Registration keyword'),
    '#description' => t('Secret keyword for registration'),
    '#default_value' => $course['reg_keyword'],
    '#required' => FALSE,
  );

  $form['active'] = array(
    '#type' => 'checkbox',
    '#title' => ('Active'),
    '#default_value' => $course['active'],
    '#description' => t('Check this box to show this course in the list of ongoing courses'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $in_course_id ? t('Submit') : t('Create new course'),
  );

  return $form;

}

/**
 *   Course add/edit display form - submit handler
 *
 *  
 */ 
 function course_add_edit_form_submit($form, &$form_state) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  if ($form_state['values']['id']) {
    course_update_course($form_state['values']['id'], $form_state['values']['title'], $form_state['values']['short_title'], $form_state['values']['active'], $form_state['values']['weight'], $form_state['values']['reg_open'], $form_state['values']['reg_keyword']);
    drupal_flush_all_caches();
    drupal_goto('course/' . $form_state['values']['id']);
  }
  else {
    course_insert_to_db($form_state['values']['title'], $form_state['values']['short_title'], $form_state['values']['active'], $form_state['values']['weight'], $form_state['values']['reg_open'], $form_state['values']['reg_keyword']);
    drupal_flush_all_caches();
    drupal_goto('course');
  }

}

?>