<?php

/**
 * @file
 * GML Course system
 * 
 * Supervisor subsytem db functions(check assings etc.)
 * 
 */
 
 /**
 *   Returns array of courses that current supervisor(user) can check
 *
 *   
 */ 
function supervisor_get_my_courses() {
  $user_id = $GLOBALS['user']->uid;

  // administrators have full access

  if ( user_access('administer courses') ) {
    return course_fetch_courses();
  }

  $result = db_fetch_array(
				db_query( "SELECT {gmlcourse_supgroups2permissions}.course_id
							FROM {gmlcourse_supgroups2permissions},
								 {gmlcourse_users2supgroups}
							WHERE 
							  {gmlcourse_users2supgroups}.user_id = %d AND
							  {gmlcourse_users2supgroups}.group_id = {gmlcourse_supgroups2permissions}.group_id AND
							  {gmlcourse_supgroups2permissions}.course_id = -1", 
							  $user_id ) );

  // supervisor group has access to all courses

  if ( $result ) {
    return course_fetch_courses();
  }

  $result = db_query( "SELECT {gmlcourse_courses}.*
						FROM {gmlcourse_courses}
						WHERE {gmlcourse_courses}.id IN 
						  ( SELECT {gmlcourse_supgroups2permissions}.course_id
							FROM {gmlcourse_supgroups2permissions},
								 {gmlcourse_users2supgroups}
							WHERE
								 {gmlcourse_users2supgroups}.user_id = %d AND
								 {gmlcourse_users2supgroups}.group_id = {gmlcourse_supgroups2permissions}.group_id AND
								 {gmlcourse_supgroups2permissions}.course_id <> -1 )
						ORDER BY {gmlcourse_courses}.active DESC,
								 {gmlcourse_courses}.weight DESC,
							     {gmlcourse_courses}.id ASC", 
						$user_id );

  $courses = array();

  while ( $course = db_fetch_array( $result ) ) {
    $courses[] = $course;
  }

  return $courses;
}

 /**
 *   Returns list of assings info for given course
 *
 *   
 */ 
function supervisor_get_all_assigns_unfetched( $in_course_id ) {
  $result = db_query( "SELECT {gmlcourse_assigns}.*
					  FROM {gmlcourse_assigns}
					  WHERE {gmlcourse_assigns}.course_id = %d
					  ORDER BY {gmlcourse_assigns}.number ASC", 
					  $in_course_id );

  return $result;
}

/**
 *   Returns struct(assing id->assing array) of assings info for given course
 *
 *   
 */ 
function supervisor_get_my_assigns_struct( $in_course_id ) {
  $user_id = $GLOBALS['user']->uid;

  // administrators have full access

  if ( user_access('administer courses') ) {
    $result = supervisor_get_all_assigns_unfetched( $in_course_id );
  }
  else {
    $result = db_fetch_array(
				  db_query( "SELECT {gmlcourse_supgroups2permissions}.assign_id
							  FROM {gmlcourse_supgroups2permissions},
								   {gmlcourse_users2supgroups}
							  WHERE 
								{gmlcourse_users2supgroups}.user_id = %d AND
								{gmlcourse_users2supgroups}.group_id = {gmlcourse_supgroups2permissions}.group_id AND
								{gmlcourse_supgroups2permissions}.course_id = %d AND
								{gmlcourse_supgroups2permissions}.assign_id = -1", 
								$user_id, $in_course_id ) );

    // supervisor group has access to all assigns of this course

    if ( $result ) {
      $result = supervisor_get_all_assigns_unfetched( $in_course_id );
    }
    else {
      $result = db_query( "SELECT {gmlcourse_assigns}.*
							FROM {gmlcourse_assigns}
							WHERE {gmlcourse_assigns}.course_id = %d AND
									{gmlcourse_assigns}.id IN 
							  ( SELECT {gmlcourse_supgroups2permissions}.assign_id
								FROM {gmlcourse_supgroups2permissions},
									 {gmlcourse_users2supgroups}
								WHERE
									 {gmlcourse_users2supgroups}.user_id = %d AND
									 {gmlcourse_users2supgroups}.group_id = {gmlcourse_supgroups2permissions}.group_id AND
									 {gmlcourse_supgroups2permissions}.course_id = %d AND
									 {gmlcourse_supgroups2permissions}.assign_id <> -1 )
							ORDER BY {gmlcourse_assigns}.number ASC", 
							$in_course_id, $user_id, $in_course_id );
    }
  }

  $assign_ids = array();
  $assign_info = array();

  while ( $assign = db_fetch_array( $result ) ) {
    $assign_ids[] = $assign['id'];
    $assign_info[$assign['id']] = $assign;
  }

  return array('ids' => $assign_ids, 'info' => $assign_info);
}

/**
 *   Returns array of assings arrays for given course
 *
 *   
 */ 
function supervisor_get_all_assigns( $in_course_id ) {
  $result = db_query( "SELECT {gmlcourse_assigns}.*
					  FROM {gmlcourse_assigns}
					  WHERE {gmlcourse_assigns}.course_id = %d
					  ORDER BY {gmlcourse_assigns}.number ASC", 
					  $in_course_id );

  $assigns = array();

  while ( $assign = db_fetch_array( $result ) ) {
    $assigns[] = $assign;
  }

  return $assigns;
}

/**
 *   Returns array of assings arrays for given course that current supervisor(user) can check
 *
 *   
 */ 
 function supervisor_get_my_assigns( $in_course_id ) {
  $user_id = $GLOBALS['user']->uid;

  // administrators have full access

  if ( user_access('administer courses') ) {
    return supervisor_get_all_assigns( $in_course_id );
  }

  $result = db_fetch_array(
				db_query( "SELECT {gmlcourse_supgroups2permissions}.assign_id
							FROM {gmlcourse_supgroups2permissions},
								 {gmlcourse_users2supgroups}
							WHERE 
							  {gmlcourse_users2supgroups}.user_id = %d AND
							  {gmlcourse_users2supgroups}.group_id = {gmlcourse_supgroups2permissions}.group_id AND
							  {gmlcourse_supgroups2permissions}.course_id = %d AND
							  {gmlcourse_supgroups2permissions}.assign_id = -1", 
							  $user_id, $in_course_id ) );

  // supervisor group has access to all assigns of this course

  if ( $result ) {
    return supervisor_get_all_assigns( $in_course_id );
  }

  $result = db_query( "SELECT {gmlcourse_assigns}.*
						FROM {gmlcourse_assigns}
						WHERE {gmlcourse_assigns}.course_id = %d AND
								{gmlcourse_assigns}.id IN 
						  ( SELECT {gmlcourse_supgroups2permissions}.assign_id
							FROM {gmlcourse_supgroups2permissions},
								 {gmlcourse_users2supgroups}
							WHERE
								 {gmlcourse_users2supgroups}.user_id = %d AND
								 {gmlcourse_users2supgroups}.group_id = {gmlcourse_supgroups2permissions}.group_id AND
								 {gmlcourse_supgroups2permissions}.course_id = %d AND
								 {gmlcourse_supgroups2permissions}.assign_id <> -1 )
						ORDER BY {gmlcourse_assigns}.number ASC", 
						$in_course_id, $user_id, $in_course_id );

  $assigns = array();

  while ( $assign = db_fetch_array( $result ) ) {
    $assigns[] = $assign;
  }

  return $assigns;
}

/**
 *   Checks that user with given user_id have supervisro access to course with given course_id
 *
 *   
 */ 
function supervisor_verify_course_access($in_user_id, $in_course_id, $account = null) {
  $result = db_fetch_array(
				db_query( "SELECT {gmlcourse_supgroups2permissions}.course_id
							FROM {gmlcourse_supgroups2permissions},
								 {gmlcourse_users2supgroups}
							WHERE 
							  {gmlcourse_users2supgroups}.user_id = %d AND
							  {gmlcourse_users2supgroups}.group_id = {gmlcourse_supgroups2permissions}.group_id AND
							  ( {gmlcourse_supgroups2permissions}.course_id = -1 OR
								{gmlcourse_supgroups2permissions}.course_id = %d )", 
							  $in_user_id, $in_course_id ) );

  if ( $result ) {
    return true;
  }
  else {
    return false;
  }
}

/**
 *   Returns sql query part for WHERE clause based on input filter data from interface
 *
 *   
 */ 
function supervisor_make_sql_query_filter($in_filter, $in_field_name) {
  if ( $in_filter == NULL ) {
    return '';
  }
  if ( count($in_filter) <= 0 ) {
    return '';
  }

  $sql = ' AND ( ';
  $cnt = 0;

  foreach ($in_filter as $value) {
    if ( !is_numeric( $value ) ) {
      continue;
    }

    if ( $cnt > 0 ) {
      $sql .= ' OR ';
    }
    else {
      $cnt++;
    }

    $sql .= $in_field_name . ' = ' . $value;
  }

  $sql .= ' )';

  return $sql;
}

/**
 *   Query db for works from course with given filter
 *
 *   
 */ 
function supervisor_work_query_sql($in_course_id, $in_filter) {
  if ( !is_numeric( $in_course_id ) ) {
    return array();
  }

  $sql = "SELECT {gmlcourse_works}.*,
			{gmlcourse_assigns}.number AS assign_number,
			{gmlcourse_assigns}.title AS assign_title,
			{gmlcourse_assigns}.url AS assign_url,
			{gmlcourse_work_statuses}.title AS status_title,
			{gmlcourse_groups}.title AS group_title,
			{gmlcourse_userinfo}.*
			FROM {gmlcourse_works}, {gmlcourse_assigns},
				 {gmlcourse_work_statuses}, {gmlcourse_students2groups},
				 {gmlcourse_groups}, {gmlcourse_userinfo}
			WHERE {gmlcourse_assigns}.course_id = $in_course_id AND
			{gmlcourse_assigns}.id = {gmlcourse_works}.assign_id AND
			{gmlcourse_works}.status = {gmlcourse_work_statuses}.id AND
			{gmlcourse_students2groups}.student_id = {gmlcourse_works}.student_id AND
			{gmlcourse_groups}.id = {gmlcourse_students2groups}.group_id AND
			{gmlcourse_groups}.course_id = {gmlcourse_assigns}.course_id AND
			{gmlcourse_userinfo}.student_id = {gmlcourse_works}.student_id";

  $sql_filter  = supervisor_make_sql_query_filter( $in_filter['assign'], '{gmlcourse_assigns}.id' );
  $sql_filter .= supervisor_make_sql_query_filter( $in_filter['group'], '{gmlcourse_students2groups}.group_id' );
  $sql_filter .= supervisor_make_sql_query_filter( $in_filter['name'], '{gmlcourse_works}.student_id' );
  $sql_filter .= supervisor_make_sql_query_filter( $in_filter['status'], '{gmlcourse_works}.status' );
  $sql_filter .= supervisor_make_sql_query_filter( $in_filter['confirmed'], '{gmlcourse_works}.confirmed' );

  $sql .= $sql_filter;

  $count = "SELECT COUNT({gmlcourse_works}.id)
			FROM {gmlcourse_works}, {gmlcourse_assigns},
				 {gmlcourse_students2groups}
			WHERE {gmlcourse_assigns}.course_id = $in_course_id AND
			{gmlcourse_assigns}.id = {gmlcourse_works}.assign_id AND
			{gmlcourse_students2groups}.student_id = {gmlcourse_works}.student_id";

  $count .= $sql_filter;
  //returns sql result and items counts
  return array("sql" => $sql, "count" => $count);
}

/**
 *   Returns sql query to sort work list
 *
 *   
 */
function supervisor_workslist_tablesort_sql($in_header) {
  $ts = tablesort_init($in_header);

  if ($ts['sql']) {
    $sql = db_escape_string($ts['sql']);
    $sort = drupal_strtoupper(db_escape_string($ts['sort']));
    $result = " ORDER BY $sql $sort";

    switch ( $ts['sql'] ) {
      case 'group_title':

        $result .= ', last_name ASC, first_name ASC, middle_name ASC, assign_number ASC, date ASC';

        break;

      case 'login':

        $result .= ', assign_number ASC, date ASC';

        break;

      case 'last_name':

        $result .= ", first_name $sort, middle_name $sort, assign_number ASC, date ASC";

        break;

      case 'assign_number':

        $result .= ', last_name ASC, first_name ASC, middle_name ASC, date ASC';

        break;

      case 'filesize':

        $result .= ', assign_number ASC, last_name ASC, first_name ASC, middle_name ASC';

        break;

      case 'status_title':

        $result .= ', last_name ASC, first_name ASC, middle_name ASC, assign_number ASC, date ASC';

        break;

      case 'date':

        $result .= ', assign_number ASC, last_name ASC, first_name ASC, middle_name ASC';

        break;
    }
  }
  else {
    $result = '';
  }

  return $result;
}

/**
 *   Returns sorted sql results for work list
 *
 *   
 */
function supervisor_subm_query_tablelist($in_filter, $in_course_id) {
  if ( !is_numeric( $in_course_id ) ) {
    return array();
  }

  if ( $in_filter == NULL ) {
    return array();
  }
  if ( count($in_filter) <= 0 ) {
    return array();
  }

  $selects = array();

  $cnt = 0;

  foreach ($in_filter as $assign_id) {
    if ( !is_numeric( $assign_id ) ) {
      continue;
    }

    $tablename = 'gmlcourse_assign_' . $assign_id;

    $sql = "SELECT $cnt AS assign_unique_num,
			$tablename.check_date AS check_date,
			gmlcourse_assigns.id AS assign_id,
			gmlcourse_assigns.number AS assign_number,
			gmlcourse_assigns.title AS assign_title,
			gmlcourse_assigns.url AS assign_url,
			gmlcourse_submission_statuses.title AS submission_status_title,
			gmlcourse_groups.title AS group_title,
			gmlcourse_userinfo.*
			FROM $tablename, gmlcourse_assigns,
				 gmlcourse_submission_statuses, gmlcourse_students2groups,
				 gmlcourse_groups, gmlcourse_userinfo
			WHERE gmlcourse_assigns.course_id = $in_course_id AND
			gmlcourse_assigns.id = $assign_id AND
			$tablename.status = gmlcourse_submission_statuses.id AND
			gmlcourse_students2groups.student_id = $tablename.student_id AND
			gmlcourse_groups.id = gmlcourse_students2groups.group_id AND
			gmlcourse_groups.course_id = gmlcourse_assigns.course_id AND
			gmlcourse_userinfo.student_id = $tablename.student_id";

    $selects[] = $sql;

    $cnt++;
  }

  return $selects;
}

/**
 *   Returns elements count with given filter
 *
 *   
 */
function supervisor_subm_query_count($in_filter, $in_course_id) {
  if ( !is_numeric( $in_course_id ) ) {
    return array();
  }

  if ( $in_filter == NULL ) {
    return array();
  }
  if ( count($in_filter) <= 0 ) {
    return array();
  }

  $selects = array();

  $cnt = 0;

  foreach ($in_filter as $assign_id) {
    if ( !is_numeric( $assign_id ) ) {
      continue;
    }

    $tablename = 'gmlcourse_assign_' . $assign_id;

    $sql = "SELECT $cnt AS assign_unique_num,
			$tablename.student_id AS student_id,
			gmlcourse_submission_statuses.id AS status_id,
			gmlcourse_groups.id AS group_id
			FROM $tablename, gmlcourse_submission_statuses,
			  gmlcourse_students2groups, gmlcourse_groups
			WHERE $tablename.status = gmlcourse_submission_statuses.id AND
			gmlcourse_students2groups.student_id = $tablename.student_id AND
			gmlcourse_groups.id = gmlcourse_students2groups.group_id AND
			gmlcourse_groups.course_id = $in_course_id";

    $selects[] = $sql;

    $cnt++;
  }

  return $selects;
}

/**
 *   Returns submission query result with given filter
 *
 *   
 */
function supervisor_submission_query_sql($in_course_id, $in_filter) {

  $selects = supervisor_subm_query_tablelist( $in_filter['assign'], $in_course_id );

  if ( count( $selects ) > 0 ) {
    $sql_filter = '';
    $sql_filter .= supervisor_make_sql_query_filter( $in_filter['group'], '{gmlcourse_students2groups}.group_id' );
    $sql_filter .= supervisor_make_sql_query_filter( $in_filter['name'], '{gmlcourse_students2groups}.student_id' );
    $sql_filter .= supervisor_make_sql_query_filter( $in_filter['submission_status'], '{gmlcourse_submission_statuses}.id' );

    $sql = '';

    $flag_union = false;

    foreach ($selects as $select) {
      if ( $flag_union ) {
        $sql .= ' UNION ALL ';
      }
      else {
        $flag_union = true;
      }

      $sql .= $select . $sql_filter;
    }

    $selects = supervisor_subm_query_count( $in_filter['assign'], $in_course_id );

    $count = '';

    $flag_union = false;

    foreach ($selects as $select) {
      if ( $flag_union ) {
        $count .= ' UNION ALL ';
      }
      else {
        $flag_union = true;
      }

      $count .= $select . $sql_filter;
    }

    $count = 'SELECT COUNT(*) FROM ( ' . $count . ' ) AS count';
  }
  else {
    $sql = '';
    $count = '';
  }

  return array("sql" => $sql, "count" => $count);
}

/**
 *   Returns sql query for sorting submissions table
 *
 *   
 */
function supervisor_submissionslist_tablesort_sql($in_header) {
  $ts = tablesort_init($in_header);

  if ($ts['sql']) {
    $sql = db_escape_string($ts['sql']);
    $sort = drupal_strtoupper(db_escape_string($ts['sort']));
    $result = " ORDER BY $sql $sort";

    switch ( $ts['sql'] ) {
      case 'group_title':

        $result .= ', last_name ASC, first_name ASC, middle_name ASC, assign_unique_num ASC';

        break;

      case 'login':

        $result .= ', assign_unique_num ASC';

        break;

      case 'last_name':

        $result .= ", first_name $sort, middle_name $sort, assign_unique_num ASC";

        break;

      case 'assign_unique_num':

        $result .= ', last_name ASC, first_name ASC, middle_name ASC';

        break;

      case 'submission_status_title':

        $result .= ', last_name ASC, first_name ASC, middle_name ASC, assign_unique_num ASC';

        break;
    }
  }
  else {
    $result = '';
  }

  return $result;
}

/**
 *   Returns sorted sql results for submissions table
 *
 *   
 */
function supervisor_get_subm_filtered_works( $in_id_filter, $in_filter, $in_sql_sorting ) {
  if ( $in_id_filter == NULL ) {
    return array();
  }
  if ( count($in_id_filter) <= 0 ) {
    return array();
  }

  $sql = "SELECT {gmlcourse_works}.*,
			{gmlcourse_work_statuses}.title AS work_status_title
			FROM {gmlcourse_works}, {gmlcourse_work_statuses}
			WHERE {gmlcourse_works}.status = {gmlcourse_work_statuses}.id";

  $sql_filter = supervisor_make_sql_query_filter( $in_filter['work_status'], '{gmlcourse_works}.status' );
  $sql_filter .= supervisor_make_sql_query_filter( $in_filter['confirmed'], '{gmlcourse_works}.confirmed' );

  $sql .= $sql_filter;

  $id_filter = '';

  $cnt = 0;

  foreach ( $in_id_filter as $assign_id => $students ) {
    if ( !is_numeric( $assign_id ) ) {
      continue;
    }

    $user_filter = '';

    $cnt2 = 0;

    foreach ( $students as $student_id ) {
      if ( !is_numeric( $student_id ) ) {
        continue;
      }

      if ( $cnt2 > 0 ) {
        $user_filter .= ' OR ';
      }
      else {
        $cnt2++;
      }

      $user_filter .= 'gmlcourse_works.student_id = ' . $student_id;
    }

    if ( $user_filter ) {
      if ( $cnt > 0 ) {
        $id_filter .= ' OR ';
      }
      else {
        $cnt++;
      }
      $id_filter .= '( gmlcourse_works.assign_id = ' . $assign_id . ' AND ( ' . $user_filter . ' ) )';
    }
  }

  if ( $id_filter ) {
    $id_filter = ' AND ( ' . $id_filter . ' )';
  }

  $sql .= $id_filter;

  $sql .= $in_sql_sorting;

  $result = db_query( $sql );

  $works = array();
  while ($work = db_fetch_array($result)) {
    $works[] = $work;
  }

  return $works;
}

/**
 *   Returns works array for submission
 *
 *   
 */
function supervisor_get_subm_works( $in_student_id, $in_assign_id, $in_confirmed ) {

  $sql = "SELECT {gmlcourse_works}.*,
			{gmlcourse_work_statuses}.title AS work_status_title
			FROM {gmlcourse_works}, {gmlcourse_work_statuses}
			WHERE {gmlcourse_works}.status = {gmlcourse_work_statuses}.id
			 AND ( gmlcourse_works.assign_id = %d AND ( gmlcourse_works.student_id = %d ) ) ";
  if ($in_confirmed) {
    $sql .= 'AND ( gmlcourse_works.confirmed = 1 )';
  }

  $result = db_query( $sql, $in_assign_id, $in_student_id );

  $works = array();
  while ($work = db_fetch_array($result)) {
    $works[] = $work;
  }

  return $works;
}

/**
 *   Returns sql sorting clause for submisson list
 *
 *   
 */
function supervisor_submlist_works_tablesort_sql($in_header) {
  $ts = tablesort_init($in_header);

  if ($ts['sql']) {
    $sql = db_escape_string($ts['sql']);
    $sort = drupal_strtoupper(db_escape_string($ts['sort']));
    $result = " ORDER BY $sql $sort";

    switch ( $ts['sql'] ) {
      case 'confirmed':

        $result .= ', date ASC';

        break;

      case 'work_status_title':

        $result .= ', date ASC';

        break;
    }
  }
  else {
    $result = '';
  }

  return $result;
}

/**
 *   Returns works id for given course id
 *
 *   
 */
function supervisor_get_work_ids_for_course($in_course_id) {
  $result = db_query(
			"SELECT {gmlcourse_works}.id
				FROM {gmlcourse_works}, {gmlcourse_assigns}
				WHERE {gmlcourse_assigns}.course_id = %d AND
				{gmlcourse_assigns}.id = {gmlcourse_works}.assign_id", $in_course_id);

  $works = array();
  while ($work = db_fetch_array($result)) {
    $works[] = $work;
  }

  return $works;
}

/**
 *   Checks that given user can check given work for given course
 *
 *   
 */
function supervisor_verify_course_work_pair($in_user_id, $in_course_id, $in_work_id, $in_access_check = FALSE ) {
  if ( $in_access_check ) {
    $user = user_load( array('uid' => $in_user_id) );
    if ( !user_access( 'access supervisor front-end', $user ) ) {
      return false;
    }
  }

  $work = db_fetch_array( db_query( "SELECT {gmlcourse_works}.id, {gmlcourse_works}.assign_id
										FROM {gmlcourse_works}, {gmlcourse_assigns}
										WHERE {gmlcourse_assigns}.course_id = %d AND
										{gmlcourse_assigns}.id = {gmlcourse_works}.assign_id AND
										{gmlcourse_works}.id = %d", 
										$in_course_id, $in_work_id) );

  if ( $work ) {
    $result = db_fetch_array(
				  db_query( "SELECT {gmlcourse_supgroups2permissions}.assign_id
							  FROM {gmlcourse_supgroups2permissions},
								   {gmlcourse_users2supgroups}
							  WHERE 
								{gmlcourse_users2supgroups}.user_id = %d AND
								{gmlcourse_users2supgroups}.group_id = {gmlcourse_supgroups2permissions}.group_id AND
								( {gmlcourse_supgroups2permissions}.course_id = -1 OR
								  {gmlcourse_supgroups2permissions}.course_id = %d ) AND
								( {gmlcourse_supgroups2permissions}.assign_id = -1 OR
								  {gmlcourse_supgroups2permissions}.assign_id = %d )", 
								$in_user_id, $in_course_id, $work['assign_id'] ) );

    if ( $result ) {
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
}

/**
 *   Checks that given user can check given assign for given course
 *
 *   
 */
function supervisor_verify_course_assign_num_pair($in_user_id, $in_course_id, $in_assign_num, $in_access_check = FALSE ) {
  if ( $in_access_check ) {
    $user = user_load( array('uid' => $in_user_id) );
    if ( !user_access( 'access supervisor front-end', $user ) ) {
      return false;
    }
  }

  $assign = assign_fetch_assign_by_number( $in_course_id, $in_assign_num );

  if ( $assign ) {
    $result = db_fetch_array(
				  db_query( "SELECT {gmlcourse_supgroups2permissions}.assign_id
							  FROM {gmlcourse_supgroups2permissions},
								   {gmlcourse_users2supgroups}
							  WHERE 
								{gmlcourse_users2supgroups}.user_id = %d AND
								{gmlcourse_users2supgroups}.group_id = {gmlcourse_supgroups2permissions}.group_id AND
								( {gmlcourse_supgroups2permissions}.course_id = -1 OR
								  {gmlcourse_supgroups2permissions}.course_id = %d ) AND
								( {gmlcourse_supgroups2permissions}.assign_id = -1 OR
								  {gmlcourse_supgroups2permissions}.assign_id = %d )", 
								$in_user_id, $in_course_id, $assign['id'] ) );

    if ( $result ) {
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
}

/**
 *   Returns work array with supervisor info
 *
 *   
 */
function supervisor_fetch_work_with_info( $in_course_id, $in_work_id ) {
  $result = db_fetch_array( db_query("SELECT {gmlcourse_courses}.title AS course_title,
										{gmlcourse_works}.*,
										{gmlcourse_userinfo}.*,
										{gmlcourse_assigns}.number AS assign_number,
										{gmlcourse_assigns}.title AS assign_title,
										{gmlcourse_assigns}.url AS assign_url,
										{gmlcourse_assigns}.show_results AS assign_show_results,
										{gmlcourse_assigns}.flags AS assign_flags
									  FROM {gmlcourse_works}, {gmlcourse_courses},
											{gmlcourse_assigns}, {gmlcourse_userinfo}
									  WHERE {gmlcourse_courses}.id = %d AND
											{gmlcourse_works}.id = %d AND
											{gmlcourse_assigns}.id = {gmlcourse_works}.assign_id AND
											{gmlcourse_userinfo}.student_id = {gmlcourse_works}.student_id", 
									  $in_course_id, $in_work_id) );

  return( $result );
}

/**
 *   Returns assignment array with supervisor info
 *
 *   
 */
 function supervisor_fetch_assignment_with_info( $in_course_id, $in_assign_num ) {
  $result = db_fetch_array( db_query("SELECT {gmlcourse_courses}.title AS course_title,
										{gmlcourse_assigns}.number AS assign_number,
										{gmlcourse_assigns}.id AS assign_id,
										{gmlcourse_assigns}.title AS assign_title,
										{gmlcourse_assigns}.url AS assign_url,
										{gmlcourse_assigns}.show_results AS assign_show_results,
										{gmlcourse_assigns}.flags AS assign_flags
									  FROM {gmlcourse_courses},	{gmlcourse_assigns}
									  WHERE {gmlcourse_courses}.id = %d AND
										    {gmlcourse_assigns}.course_id = {gmlcourse_courses}.id AND
											{gmlcourse_assigns}.number = %d", 
									  $in_course_id, $in_assign_num) );

  return( $result );
}

?>