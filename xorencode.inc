<?php

class XOREncode
{ 

    function encode($string,$key)
	{         
        for($i=0,$j=0; $i<strlen($string); $i++,$j++)
		{ 
           if( $j == strlen($key) ) { $j=0; } 
		  {
			$string[$i] = $string[$i]^$key[$j];             
		  }
		} 
        return $string; 
    } 

    function decode($encstring, $key)
	{         
        for($i=0,$j=0; $i<strlen($encstring); $i++,$j++)
		{ 
            if( $j == strlen($key) ) { $j=0; } 
            $encstring[$i] = $key[$j]^$encstring[$i]; 
             
        } 

        return $encstring; 
    } 
} 

?>