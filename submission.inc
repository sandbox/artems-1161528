<?php

/**
 * @file
 * GML Course system
 * 
 * Submission db functions
 *
 */
 
 /**
 *   Add new submission to db
 *
 *   
 */ 
function submission_add_submission(
  $in_course_id, 
  $in_assign_number, 
  $in_student_id, 
  $in_supervisor_id, 
  $in_components, 
  $in_status, 
  $in_node_id = 0,
  $in_check_date = '1970-01-01 00:00:00') {
  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);

  if ($in_components) {
    // make array of column names

    $colnames = array_keys($in_components);


    // make array of column values

    $comp_values = array();
    foreach ($in_components as $component_value) {
      $comp_values[] = $component_value ? $component_value : 0;
    }

    // query database

    $result = db_query("
      INSERT INTO %s 
      (student_id, supervisor_id, status, nid, check_date, %s) 
      VALUES (%d, %d, %d, %d, '%s', %s)",  
      $tablename, 
      implode(',', $colnames), 
      $in_student_id, 
      $in_supervisor_id, 
      $in_status, 
	  $in_node_id, 
	  $in_check_date,
      implode(',', $comp_values));
  }
  else {
    // query database

    $result = db_query("
      INSERT INTO %s 
      (student_id, supervisor_id, status, nid, check_date) 
      VALUES (%d, %d, %d, %d, '%s')",  
      $tablename, 
      $in_student_id, 
      $in_supervisor_id, 
      $in_status, 
	  $in_node_id,
	  $in_check_date);
  }

  return TRUE;
}


/**
 *   Makes array with components and their values to add to db
 *   
 *   
 */ 
function _submission_make_array_of_column_names_with_values( $in_components, $in_course_id, $in_assign_number) {
  $colsetnames = array();

  $components = component_fetch_components($in_course_id, $in_assign_number);

  $comp_keys = array_keys( $in_components );
  $comp_keys_nums = explode("_", $comp_keys, 2 );

  $comp_values = array_values( $in_components );
  //actual component name - comp_%number
  for ( $i = 0; $i < sizeof( $in_components ); ++$i ) {
    $value = $comp_values[$i] ? $comp_values[$i] : 'NULL';
    $colsetnames[] = "comp_" . $components[$i]['db_number'] . "=" . $value;
  }

  return $colsetnames;
}

/**
 *   Update submission 
 *   
 *   
 */ 
 function submission_update_submission(
  $in_course_id, 
  $in_assign_number, 
  $in_student_id, // key for submission!

  $in_supervisor_id, 
  $in_components, 
  $in_status) {
  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number); //construct table name

  $colsetnames = _submission_make_array_of_column_names_with_values( $in_components, $in_course_id, $in_assign_number );


  // query database

  $result = db_query("
    UPDATE %s SET supervisor_id=%d, status=%d, %s WHERE student_id=%d",  
    $tablename, 
    $in_supervisor_id, 
    $in_status, 
    implode(',', $colsetnames), 
    $in_student_id);

}

/**
 *   Update submission late data
 *   
 *   
 */ 
 function submission_update_submission_late_data(
  $in_course_id, 
  $in_assign_number, 
  $in_student_id, // key for submission!

  $in_component_text
  ) {
  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);

  // query database

  $result = db_query("
    UPDATE %s SET %s WHERE student_id=%d",  
    $tablename, 
    $in_component_text, 
    $in_student_id);

}

/**
 *   Update submission data for supervisor - used in work check
 *   
 *   
 */ 
 function submission_update_submission_supervisor_data(
  $in_course_id, 
  $in_assign_number, 
  $in_student_id, // key for submission!

  $in_supervisor_id, 
  $in_components, 
  $in_status, 
  $in_check_date,
  $in_was_on_appeal) {
  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);

  $colsetnames = _submission_make_array_of_column_names_with_values( $in_components, $in_course_id, $in_assign_number );

  db_query("
    UPDATE %s SET supervisor_id = %d, check_date = '%s', status = %d, was_on_appeal = %d, %s WHERE student_id=%d",  
    $tablename, 
    $in_supervisor_id, 
	$in_check_date, 
	$in_status, 
	$in_was_on_appeal,
    implode(',', $colsetnames), 
    $in_student_id);

}

/**
 *   Fetch array of submission arrays
 *   
 *   
 */ 
 function submission_fetch_submissions($in_course_id, $in_assign_number) {
  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);

  // make col names  

  $assign_components = component_fetch_components($in_course_id, $in_assign_number);
  
  foreach ($assign_components as   $i => $component) {
    $colnames[] = 'comp_' . $component['db_number'];
  }

  $result = db_query(
    "SELECT student_id, supervisor_id, users.name, check_date, was_on_appeal,  %s
    FROM %s, users
    WHERE users.uid=%s.student_id", 
    implode(',', $colnames), 
    $tablename, 
    $tablename
  );

  $submissions = array();
  while ($column = db_fetch_array($result)) {
    // append

    $submissions[] = $column;
  }

  return $submissions;

}

/**
 *   Fetch array of submission arrays with additional data for checking
 *   
 *   
 */ 
function submission_fetch_submissions_brief($in_course_id, $in_assign_number) {
  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);

  // make col names  

  $assign_components = component_fetch_components($in_course_id, $in_assign_number);

  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);

  if (!$assign['auto_late_penality']) {
    array_pop($assign_components);
  }

  foreach ($assign_components as   $i => $component) {
    $colnames[] = 'comp_' . $component['db_number'];
  }

  $result = db_query(
    "SELECT supervisor_id, check_date, gmlcourse_userinfo.*, gmlcourse_submission_statuses.title as status, %s, %s.was_on_appeal
    FROM {%s}, {gmlcourse_userinfo}, {gmlcourse_submission_statuses}
    WHERE {gmlcourse_userinfo}.student_id = %s.student_id AND {%s}.status = {gmlcourse_submission_statuses}.id", 
    $colnames ? implode(', ', $colnames) : 0, 
    $tablename, 
	$tablename, 
    $tablename, 
    $tablename );


  $submissions = array();
  while ($column = db_fetch_array($result)) {

    if ( is_array( $column ) ) {
      $column['comp_sum'] = (float) 0;

      // Forcing database to sum up values may lead to unexpected float consistency issues.

      // Thus we're doing it in php.

      if ( $colnames ) {
        foreach ( $colnames as $colname ) {
          $column['comp_sum'] += floatval( $column[$colname] );
        }
      }
    }

    // append

    $submissions[] = $column;
  }
  return $submissions;


}

/**
 *   Fetch submission array for student
 *   
 *   
 */ 
function submission_fetch_submission_by_student_id($in_course_id, $in_assign_number, $in_student_id) {
  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);
  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);

  // make col names  

  $assign_components = component_fetch_components($in_course_id, $in_assign_number);

  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);

  if (!$assign['auto_late_penality']) {
    array_pop($assign_components);
  }

  foreach ($assign_components as   $i => $component) {
    $colnames[] = 'comp_' . $component['db_number'];
  }

  $result = db_fetch_array( db_query(
					"SELECT student_id, users.name, supervisor_id, {%s}.nid, %s.status, %s.was_on_appeal, check_date, %s
					FROM {%s}, {users}
					WHERE student_id = %d AND student_id = users.uid", 
					$tablename, 
					$tablename, 
					$tablename, 
					$colnames ? implode(', ', $colnames) : 0, 
					$tablename, 
					$in_student_id ) );

  if ( !is_array( $result ) ) {
    return $result;
  }

  $result['show_results'] = $assign['show_results'];

  // Forcing database to sum up values may lead to unexpected float consistency issues.

  // Thus we're doing it in php.


  $result['comp_sum'] = (float) 0;

  if ( $colnames ) {
    foreach ( $colnames as $colname ) {
      $result['comp_sum'] += floatval( $result[$colname] );
    }
  }

  return $result;

}

/**
 *   Returns drupal node id for submission 
 *   
 *   
 */ 
function submission_get_node_id($in_course_id, $in_assign_number, $in_student_id) {
  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);

  $result = db_fetch_array( db_query(
					"SELECT {%s}.nid
					FROM {%s}, {users}
					WHERE student_id = %d AND student_id = users.uid", 
					$tablename, $tablename, 
					$in_student_id ) );

  return $result['nid'];
}

/**
 *   Delete submission node id
 *   
 *   
 */ 
function submission_delete_node($in_course_id, $in_assign_number, $in_student_id) {
  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);

  $result = db_fetch_array( db_query(
					"SELECT nid
					FROM {%s}, {users}
					WHERE student_id = %d AND student_id = users.uid", 
					$tablename, 
					$in_student_id ) );

  node_delete($result['nid']);
}


/**
 *   Delete submission from db
 *   
 *   
 */ 
function submission_delete_from_db($in_course_id, $in_assign_number, $in_student_id, $in_intransaction = FALSE) {
  if (!$in_intransaction) {
    db_query("START TRANSACTION");
  }

  submission_delete_node($in_course_id, $in_assign_number, $in_student_id);

  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);
  $result = db_query("DELETE FROM %s where student_id=%d", $tablename, $in_student_id);

  work_delete_all_from_db($in_course_id, $in_assign_number, $in_student_id, TRUE);

  if (!$in_intransaction) {
    db_query("COMMIT");
  }
}

/**
 *   Delete all submissions from db
 *   Used in assing delete process
 *   
 */ 
function submission_delete_all_from_db($in_course_id, $in_assign_number, $in_intransaction = FALSE) {
  if (!$in_intransaction) {
    db_query("START TRANSACTION");
  }

  $submissions = submission_fetch_submissions($in_course_id, $in_assign_number);

  foreach ($submissions as $submission) {
    submission_delete_from_db($in_course_id, $in_assign_number, $submission['student_id'], TRUE);
  }

  if (!$in_intransaction) {
    db_query("COMMIT");
  }
}

?>