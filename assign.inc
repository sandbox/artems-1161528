<?php

/**
 * @file
 * GML Course system
 * 
 * DB layer functions for assigns.
 * Handles all interaction with drupal db layer
 *
 */




/**
 * This adds new assign record and creates table for assign submission results
 *   also this adds "late penality" component
 */
function assign_add_assign($in_title, $in_short_title, $in_number, $in_start, $in_finish, $in_url, $in_allow_works, $in_show_results, $in_course_id, $in_flags, $in_notify_send = 0, $in_notify_user_id = 0, $in_type_id = 0, $in_auto_penality = 1, $in_appeal_settings = 1) {
  db_query("
    INSERT INTO {gmlcourse_assigns} 
    (title, short_title, number, start, finish, url, allow_works, show_results, course_id, flags, type, notify_send, notify_user_id, auto_late_penality) 
    VALUES ('%s', '%s', %d, '%s', '%s', '%s', %d, %d, %d, %d, %d, %d, %d, %d)",  
    $in_title, 
	$in_short_title, 
    $in_number, 
    $in_start, 
    $in_finish, 
    $in_url, 
	$in_allow_works, 
	$in_show_results, 
    $in_course_id, 
	$in_flags, 
	$in_type_id, 
	$in_notify_send, 
	$in_notify_user_id, 
	$in_auto_penality,
	$in_appeal_settings);

  // create table if there was no one

  $tablename = _component_make_assign_table_name($in_course_id, $in_number);
  _component_create_table($tablename);
  //create component for auto late calculations

  component_add_component($in_course_id, $in_number, t('Late penality'), 9999, 0); //9999 - means that it always be last components in assign and that we will have no more than 9998 components



}

/**
 *   Function to edit existing assign
 *   simply edit assign table
 */

function assign_update_assign($in_assign_id, $in_title, $in_short_title, $in_number, $in_start, $in_finish, $in_url, $in_allow_works, $in_show_results, $in_flags, $in_notify_send = 0, $in_notify_user_id = 0, $in_type_id = 0, $in_auto_penality = 1, $in_appeal_settings = 1) {
  db_query("
    UPDATE {gmlcourse_assigns} SET 
    title = '%s', short_title = '%s', number = %d, start = '%s', finish = '%s', url = '%s', allow_works = %d, show_results = %d, flags = %d, type = %d, notify_send = %d, notify_user_id = %d, auto_late_penality = %d, appeal_settings = %d 
    WHERE id=%d",  
    $in_title, 
	$in_short_title, 
    $in_number, 
    $in_start, 
    $in_finish, 
    $in_url, 
	$in_allow_works, 
	$in_show_results, 
	$in_flags, 
	$in_type_id, 
	$in_notify_send, 
	$in_notify_user_id, 
	$in_auto_penality, 
	$in_appeal_settings, 
    $in_assign_id);


}

/**
 *   Function to delete existing assign
 *   also all related materials will be deleted. Can be processed with transaction.
 */
function assign_delete_from_db($in_assign_id, $in_intransaction = FALSE) {
  $assign = assign_fetch_assign($in_assign_id);

  if (!$in_intransaction) {
    db_query("START TRANSACTION");
  }

  submission_delete_all_from_db($assign['course_id'], $assign['number'], TRUE);
  component_delete_all_from_db($assign['course_id'], $assign['number'], TRUE);
  db_query("DELETE FROM {gmlcourse_assigns} WHERE id = %d", $in_assign_id);
  supgroup_delete_assign_from_db( $assign['course_id'], $in_assign_id );

  if (!$in_intransaction) {
    db_query("COMMIT");
  }
}


/**
 *   Function to delete existing assign
 *   also all related materials will be deleted. Can be processed with transaction.
 */
function assign_delete_all_from_db($in_course_id, $in_intransaction = FALSE) {
  if (!$in_intransaction) {
    db_query("START TRANSACTION");
  }

  $assigns = assign_fetch_assigns($in_course_id);

  foreach ($assigns as $assign) {
    assign_delete_from_db($assign['id'], TRUE);
  }

  if (!$in_intransaction) {
    db_query("COMMIT");
  }
}

/**
 *   Function to fetch assign info by id
 *   
 *
 *  @return assign info array
 */
function assign_fetch_assign($in_assign_id) {
  $result = db_query("SELECT * FROM {gmlcourse_assigns} WHERE id=%d", $in_assign_id);

  return db_fetch_array($result);

}

/**
 *   Function to fetch assign info from course in and number
 *   
 *   @return assign info array
 */
function assign_fetch_assign_by_number($in_course_id, $in_assign_number) {
  $result = db_query(
    "SELECT * FROM {gmlcourse_assigns} 
         WHERE number=%d AND course_id=%d", $in_assign_number, $in_course_id);

  return db_fetch_array($result);
}

/**
 *   Function to fetch all assigns for course
 *   
 *   @return array of (assign info array)
 */
function assign_fetch_assigns($in_course_id) {
  $result = db_query("SELECT * FROM {gmlcourse_assigns} WHERE course_id=%d ORDER BY number ASC", $in_course_id);
  $assigns = array();
  while ($assign = db_fetch_array($result)) {
    $assigns[] = $assign;
  }

  return $assigns;
}


/**
 *   Function return late for given work
 *   @param in_work - work array
 *   
 *   @return late in days
 */
function assign_get_work_late_days($in_course_id, $in_assign_number, $in_work) {
  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);
  $assign_time_arr = date_str2arr($assign['finish']);
  $work_time_arr = date_str2arr($in_work['date']);


  $assign_time = mktime(23, 59, 59, $assign_time_arr[1], $assign_time_arr[2], $assign_time_arr[0]);
  $work_time = mktime($work_time_arr[3], $work_time_arr[4], $work_time_arr[5], $work_time_arr[1], $work_time_arr[2], $work_time_arr[0]);

  $diff_seconds = - $assign_time + $work_time;
  return ceil($diff_seconds / 60.0 / 60.0 / 24.0); // diff in days

}

/**
 *   Function enable results show for assign
 *   Toggles flag in db array for assign 
 *  
 */
function assign_update_show_results( $in_assign_id, $in_show_results ) {
  db_query("UPDATE {gmlcourse_assigns} SET show_results = %d WHERE id = %d", $in_show_results, $in_assign_id);
}

?>