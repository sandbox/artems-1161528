<?php

/**
 * @file
 * GML Course system
 * 
 *  Students works statistics pages
 *
 */

/**
 *   Display student points page for course
 *
 *   
 */ 
function stat_course_points( $in_course_id ) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }
  //can also download data as CSV
  $output = '';

  $output .= '<br/>';

  $output .= l("Download statistics (CSV)", "course/" . $in_course_id . "/points/exportcsv");

  $assigns = assign_fetch_assigns( $in_course_id );

  $assign_types = status_get_assign_types();

  if ( !$assigns ) {
    return $output;
  }

  $assigns_num = count( $assigns );

  $submissions = array();

  $submissions_addlast_num = array();
  $submissions_customsum = array();

  $points_array = array();
  $points_array['points_sum'] = 0.0;

  $customsum_array = array();

  $i = 0;
  //prepare colums for each assing
  foreach ( $assigns as $assign ) {
    if ($assign['short_title'] == NULL) {
      $assign_title = mb_substr($assign['title'], 0, 10, 'UTF-8');
    }
    else {
      $assign_title = $assign['short_title'];
    }
    $curr_assign_type = $assign_types[$assign['type']];
    $submissions[$i]['title'] = $assign_title;
    $submissions[$i]['data'] = submission_fetch_submissions_brief( $in_course_id, $assign['number'] );

    if ($curr_assign_type['last']) {
      $submissions_addlast_num[$assign['type']] = $i;
    }
    if ($curr_assign_type['sum']) {
      $submissions_customsum[$assign['type']][$i] = 1;
      $customsum_array[$assign['type']] = 0.0;
    }
    $submissions[$i]['assign_type'] = $assign['type'];
    $points_array[$i] = 0.0;
    $i++;
  }

  $students_raw = student_get_students_attending_course( $in_course_id );

  $students = array();
  //make students assings arrays
  foreach ( $students_raw as $student ) {
    $students[$student['student_id']] = $student;
    $students[$student['student_id']]['points'] = $points_array;
    $students[$student['student_id']]['points']['custom_sum'] = $customsum_array;
	$students[$student['student_id']]['appeal_flag'] = array();
  }

  for ( $i = 0; $i < $assigns_num; $i++ ) {
    $data = $submissions[$i]['data'];

  //calculate assings points sum
    foreach ( $data as $subm ) {
      $students[$subm['student_id']]['points'][$i] += $subm['comp_sum'];

      $students[$subm['student_id']]['points']['points_sum'] += $subm['comp_sum'];
	  
	  $students[$subm['student_id']]['appeal_flag'][$i] = $subm['was_on_appeal'];
     
	 //if need  - sum only last assing result
	 if ($assign_types[$submissions[$i]['assign_type']]['last']) {
        if ($i == $submissions_addlast_num[$submissions[$i]['assign_type']]) {

          for ( $j = $i-1; $j > 0; $j-- ) {
            if ($assign_types[$submissions[$j]['assign_type']]['last']) {
              $students[$subm['student_id']]['points']['points_sum'] -= $students[$subm['student_id']]['points'][$j];
            }
          }
        }
      }

      if ($assign_types[$submissions[$i]['assign_type']]['sum']) {
        if ($submissions_customsum[$submissions[$i]['assign_type']][$i]) {
          $students[$subm['student_id']]['points']['custom_sum'][$submissions[$i]['assign_type']] += $subm['comp_sum'];
        }
      }
    }
  }

  $groups = array();
  $groups_raw = group_fetch_groups($in_course_id);

  foreach ( $groups_raw as $group ) {
    $groups[$group['title']]['title'] = $group['title'];
    $groups[$group['title']]['students'] = array();
  }

  foreach ( $students as $student ) {
    if ( !$groups[$student['group_title']] ) {
      $groups[$student['group_title']]['title'] = $student['group_title'];
      $groups[$student['group_title']]['students'] = array();
    }

    $groups[$student['group_title']]['students'][] = $student;
  }

  asort($groups);

  $header = array(t('Login'), t('Name'));

  for ( $i = 0; $i < $assigns_num; $i++ ) {
    array_push( $header, $submissions[$i]['title'] );
  }

  foreach ( array_keys($submissions_customsum) as $customsum ) {
    $curr_assign_type = $assign_types[$customsum];
    if ($curr_assign_type['sum']) {
      array_push( $header, t('Sum for <br>') . mb_substr($curr_assign_type['title'], 0, 25, 'UTF-8') );
    }
  }

  array_push( $header, t('Total') );

  $prefix1_once = '<div style="width: 85px;">';
  $postfix1_once = '</div>';
  $prefix2_once = '<div style="width: 350px;">';
  $postfix2_once = '</div>';
  $prefix3_once = '<div style="width: 30px;">';
  $postfix3_once = '</div>';

  foreach ( $groups as $group ) {
    $output .= '<br><h3>' . $group['title'] . '</h3><div style="height: 5px;"></div>';

    $rows = array();

	//set points for each students in group
    foreach ( $group['students'] as $student ) {
      $row = array(
        $prefix1_once . $student['login'] . $postfix1_once,
        $prefix2_once . $student['last_name'] . ' ' . $student['first_name'] . ' ' . $student['middle_name'] . $postfix2_once,
      );
    
      for ( $i = 0; $i < $assigns_num; $i++ ) {
        if ( floatval( $student['points'][$i] ) != 0.0 ) {
		   $points = common_str_get_grade_string( $student['points'][$i] );  
        }
        else {
          $points = $student['points'][$i];
        }
	  
	//make italic font for assing sum, if student was on appeal for current assign	  
	  if ($assigns[$i]['appeal_settings']) {
	    if ($student['appeal_flag'][$i]) {
		  $points = '<i>' . $points . '</i>';  
		}
	  }

        array_push( $row, $prefix3_once . $points . $postfix3_once );
      }

      if (is_array($student['points']['custom_sum'])) {
        foreach ($student['points']['custom_sum'] as $custom_sum) {
          if ( floatval($custom_sum ) != 0.0 ) {
            $points = common_str_get_grade_string( $custom_sum );
          }
          else {
            $points = $custom_sum;
          }
          array_push( $row, $prefix3_once . $points . $postfix3_once );
        }
      }

      if ( floatval( $student['points']['points_sum'] ) != 0.0 ) {
        $points = common_str_get_grade_string( $student['points']['points_sum'] );
      }
      else {
        $points = $student['points']['points_sum'];
      }

      array_push( $row, $prefix3_once . $points . $postfix3_once );

      $rows[] = $row;
    }

    $output .= theme('table', $header, $rows);
  }

  return $output;
}

/**
 *   Display student points page for course
 *
 *   
 */ 
function stat_course_points_export_csv( $in_course_id ) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  $course = course_fetch_course( $in_course_id );

  $assigns = assign_fetch_assigns( $in_course_id );

  $assign_types = status_get_assign_types();

  if ( !$assigns ) {
    return $output;
  }

  $assigns_num = count( $assigns );

  $submissions = array();

  $submissions_addlast_num = array();
  $submissions_customsum = array();

  $points_array = array();
  $points_array['points_sum'] = 0.0;

  $customsum_array = array();

  $i = 0;

  //prepare results arrays
  foreach ( $assigns as $assign ) {
    if ($assign['short_title'] == NULL) {
      $assign_title = mb_substr($assign['title'], 0, 10, 'UTF-8');
    }
    else {
      $assign_title = $assign['short_title'];
    }
    $curr_assign_type = $assign_types[$assign['type']];
    $submissions[$i]['title'] = $assign_title;
    if ($assign['auto_late_penality']) {
      $components = component_fetch_components($in_course_id, $assign['number']);
      $late_comp = array_pop($components);
      $submissions[$i]['late_comp_name'] = "comp_" . $late_comp['db_number'];
    }
    $submissions[$i]['auto_late_penality'] = $assign['auto_late_penality'];
    $submissions[$i]['data'] = submission_fetch_submissions_brief( $in_course_id, $assign['number'] );
    if ($curr_assign_type['last']) {
      $submissions_addlast_num[$assign['type']] = $i;
    }
    if ($curr_assign_type['sum']) {
      $submissions_customsum[$assign['type']][$i] = 1;
      $customsum_array[$assign['type']] = 0.0;
    }
    $submissions[$i]['assign_type'] = $assign['type'];
    $points_array[$i] = 0.0;
    $i++;
  }

  $students_raw = student_get_students_attending_course( $in_course_id );

  $students = array();

  foreach ( $students_raw as $student ) {
    $students[$student['student_id']] = $student;
    $students[$student['student_id']]['points'] = $points_array;
    $students[$student['student_id']]['points']['custom_sum'] = $customsum_array;
  }

  //calculate points sums for assings
  for ( $i = 0; $i < $assigns_num; $i++ ) {
    $data = $submissions[$i]['data'];

    foreach ( $data as $subm ) {
      $students[$subm['student_id']]['points'][$i] += $subm['comp_sum'];

      $students[$subm['student_id']]['points']['points_sum'] += $subm['comp_sum'];
      if ($assign_types[$submissions[$i]['assign_type']]['last']) {
        if ($i == $submissions_addlast_num[$submissions[$i]['assign_type']]) {

          for ( $j = $i-1; $j > 0; $j-- ) {
            if ($assign_types[$submissions[$j]['assign_type']]['last']) {
              $students[$subm['student_id']]['points']['points_sum'] -= $students[$subm['student_id']]['points'][$j];
            }
          }
        }
      }

      if ($assign_types[$submissions[$i]['assign_type']]['sum']) {
        if ($submissions_customsum[$submissions[$i]['assign_type']][$i]) {
          $students[$subm['student_id']]['points']['custom_sum'][$submissions[$i]['assign_type']] += $subm['comp_sum'];
        }
      }
    }
  }

  $groups = array();
  $groups_raw = group_fetch_groups($in_course_id);

  foreach ( $groups_raw as $group ) {
    $groups[$group['title']]['title'] = $group['title'];
    $groups[$group['title']]['students'] = array();
  }

  foreach ( $students as $student ) {
    if ( !$groups[$student['group_title']] ) {
      $groups[$student['group_title']]['title'] = $student['group_title'];
      $groups[$student['group_title']]['students'] = array();
    }

    $groups[$student['group_title']]['students'][] = $student;
  }

  asort($groups);

  //prepare table header
  $header = array(t('Login'), t('Name'));

  for ( $i = 0; $i < $assigns_num; $i++ ) {
    array_push( $header, $submissions[$i]['title'] );
    if ($submissions[$i]['auto_late_penality']) {
      array_push( $header, t('Late for ') . $submissions[$i]['title'] );
    }
  }

  foreach ( array_keys($submissions_customsum) as $customsum ) {
    $curr_assign_type = $assign_types[$customsum];
    if ($curr_assign_type['sum']) {
      array_push( $header, t('Sum for ') . $curr_assign_type['title'] );
    }
  }

  array_push( $header, t('Total') );

  $csv = "";
  $csv .= implode(";", $header);
  $csv .= "\n";
  //start csv array
  foreach ( $groups as $group ) {

    $csv .= $group['title'];
    for ( $i = 1; $i < sizeof($header); $i++ ) {
      $csv .= ";";
    }
    $csv .= "\n";

    $rows = array();

    foreach ( $group['students'] as $student ) {
      $row = array(
        $student['login'],
        $student['last_name'] . ' ' . $student['first_name'] . ' ' . $student['middle_name'],
      );

      for ( $i = 0; $i < $assigns_num; $i++ ) {
        if ( floatval( $student['points'][$i] ) != 0.0 ) {
          $points = common_str_get_grade_string( $student['points'][$i] );
        }
        else {
          $points = $student['points'][$i];
        }

        array_push( $row, str_replace(".", ",", $points) );

        if (isset($student['late'][$i])) {
          if ( floatval( $student['late'][$i] ) != 0.0 ) {
            $points = common_str_get_grade_string( $student['late'][$i] );
          }
          else {
            $points = $student['late'][$i];
          }
          array_push( $row, str_replace(".", ",", $points) );

        }

      }


      if (is_array($student['points']['custom_sum'])) {
        foreach ($student['points']['custom_sum'] as $custom_sum) {
          if ( floatval($custom_sum ) != 0.0 ) {
            $points = common_str_get_grade_string( $custom_sum );
          }
          else {
            $points = $custom_sum;
          }
          array_push( $row, str_replace(".", ",", $points));
        }
      }

      if ( floatval( $student['points']['points_sum'] ) != 0.0 ) {
        $points = common_str_get_grade_string( $student['points']['points_sum'] );
      }
      else {
        $points = $student['points']['points_sum'];
      }

      array_push( $row, str_replace(".", ",", $points) );

      $csv .= implode(";", $row);
    }


  }
  //setup headers
  header("Content-type: application/octet-stream");
  header("Content-Disposition: attachment; filename=course_stat.csv");
  header("Pragma: no-cache");
  header("Expires: 0");
  //data output
  echo $csv;

}

?>