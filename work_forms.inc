<?php


/**
 * @file
 * GML Course system
 * 
 * Works administration interface
 *
 */

/**
 *   Display works list page
 *
 *   
 */ 
function works_admin_display($in_course_id, $in_assign_number, $in_student_id) {

  $output = common_html_small_font_class( true );
  $output .= drupal_get_form('works_admin_display_form', $in_course_id, $in_assign_number, $in_student_id);
  $output .= common_html_small_font_class( false );

  return $output;
}


/**
 *   Display works list page - form
 *
 *   
 */ 
 function works_admin_display_form(&$form_state, $in_course_id, $in_assign_number, $in_student_id) {


  $form = array();

  $works = work_fetch_works($in_course_id, $in_assign_number, $in_student_id);

  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);

  foreach ($works as $i => $work) {
    $form[$i]['number'] = array(
      '#value' => ($i + 1),
    );

    $form[$i]['date'] = array(
      '#value' => $work['date'],
    );

    $form[$i]['status'] = array(
      '#value' => $work['status_title'],
    );

    $work_path = work_get_upload_name($in_course_id, $in_assign_number, $in_student_id, $work['id'], TRUE); // relative path


    $form[$i]['download'] = array(
      '#value' => l(t('download'), 'system/files/' . $work_path),
    );


    $form[$i]['edit'] = array(
      '#value' => l(t('edit'), 'course/' . $in_course_id . '/assign/' . $in_assign_number .  '/submissions/' . $in_student_id . '/works/' . $work['id'] . '/edit'),
    );

    $form[$i]['delete'] = array(
      '#value' => l(t('delete'), 'course/' . $in_course_id . '/assign/' . $in_assign_number .  '/submissions/' . $in_student_id  . '/works/' . $work['id'] . '/delete'),
    );
  }


  return $form;
}

/**
 *   Display works list page - theme functions
 *
 *   
 */ 
function theme_works_admin_display_form($form) {

  foreach ($form as $name => $element) {
    if (isset($element['number']) && is_array($element['number'])) {
      $rows[] = array(
        drupal_render($element['number']),
        drupal_render($element['date']),
        drupal_render($element['status']),
        drupal_render($element['download']),
        drupal_render($element['edit']),
        drupal_render($element['delete']),
      );
      unset($form[$name]);
    }
  }

  $header = array(
    t('N'),
    t('Date'),
    t('Status'),
    array(
      'data' => t('Operations'),
      'colspan' => 3,
    ),
  );

  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;


}

/**
 *   Display works edit page
 *
 *   
 */ 
 function work_add_edit($in_course_id, $in_assign_number, $in_student_id, $in_work_id = NULL) {

  $output = drupal_get_form('work_add_edit_form', $in_course_id, $in_assign_number, $in_student_id, $in_work_id);

  return $output;
}

/**
 *   Works edit page - form
 *
 *   
 */ 
function work_add_edit_form(&$form_state, $in_course_id, $in_assign_number, $in_student_id, $in_work_id = NULL) {

  $edit = array();

  $form['#attributes']['enctype'] = 'multipart/form-data';

  $form['course_id'] = array(
    '#type' => 'value',
    '#value' => $in_course_id,
  );

  $form['assign_number'] = array(
    '#type' => 'value',
    '#value' => $in_assign_number,
  );

  $form['student_id'] = array(
    '#type' => 'value',
    '#value' => $in_student_id,
  );

  if ($in_work_id) {
    $form['work_id'] = array(
      '#type' => 'value',
      '#value' => $in_work_id,
    );

    $work = work_fetch_work($in_work_id);
  }
  if ( !$work ) {
    // adding work?
    $datetime = date("d.m.Y H:i");
  }
  else {
    $dbdate = $work['date'];
    $datetime = db_date_to_edit_date($dbdate);
  }

  $form['date'] = array(
    '#type' => 'textfield',
    '#title' => t('date and time'),
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t('date and time of submission in "dd.mm.yyyy hh:mm" format'),
    '#default_value' => $datetime,
    '#required' => TRUE,
  );

  $form['work_upload'] = array(
    '#type' => 'file',
    '#title' => t('Work file upload (' . own_file_upload_max_size() . 'MB max)'),
    '#size' => 40,
    '#required' => FALSE,
  ); 


  // prepare array with status names

  $statuses = status_get_statuses("work");
  $status_names = array();
  foreach ($statuses as $status) {
    $status_names[$status['id']] = $status['title'];
  }

  // form select with statuses

  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Work status'),
    '#description' => t('Work status'),
    '#default_value' => ($work['status'] ? $work['status'] : 1),
    '#options' => $status_names,
    '#required' => TRUE,
  );

  // work confirmed flag

  $form['confirmed'] = array(
    '#type' => 'select',
    '#title' => t('Confirmed'),
    '#description' => t('Where the work is confirmed'),
    '#default_value' => ($work['confirmed'] ? $work['confirmed'] : 0),
    '#options' => array(
      0 => 'No',
      1 => 'Yes',
    ),
    '#required' => TRUE,
  );
  
  // work comment, visible only for supervisors

  $form['supervisor_private_comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Supervisor private comment'),
    '#description' => common_get_comment_footer(t('Supervisor private comment')),
    '#default_value' => $work['supervisor_private_comment'],
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t($work ? 'Submit' : 'Add work'),
    '#weight' => 30,
  );

  return $form;
}


/**
 *   Works edit page - form validator
 *
 *   
 */ 
 function work_add_edit_form_validate($form, &$form_state) {

  $validators = array(
    'file_validate_extensions' => array('zip'), 
    'file_validate_size' => array(own_file_upload_max_size() * 1024 * 1024), //verify max upload size 
  );


  if ($file = file_save_upload('work_upload', $validators)) {
    $form_state['values']['work_upload_path'] = $file->filepath; //check saved file
  }
  else {
    if (!$form_state['values']['work_id']) {			//unsuccessful upload
      form_set_error('work_upload', "invalid upload");
    }
  }


}

/**
 *  Works edit page - form submit
 *
 *   
 */ 
function work_add_edit_form_submit($form, &$form_state) {
  // convert date to internal format

  $form_state['values']['date'] = edit_date_to_db_date($form_state['values']['date']);

  if ($form_state['values']['work_id']) {
    work_update(
      $form_state['values']['work_id'],   //value

      'work_upload', 
      $form_state['values']['course_id'], 
      $form_state['values']['assign_number'], 
      $form_state['values']['student_id'], 
      $form_state['values']['date'], 
      $form_state['values']['status'], 
      $form_state['values']['confirmed'], 
      $form_state['values']['supervisor_private_comment']);
  }
  else {
    work_insert(
      'work_upload', 
      $form_state['values']['course_id'], 
      $form_state['values']['assign_number'], 
      $form_state['values']['student_id'], 
      $form_state['values']['date'], 
      $form_state['values']['status'], 
      $form_state['values']['confirmed'], 
      $form_state['values']['supervisor_private_comment']);

  }

  drupal_goto('course/' . $form_state['values']['course_id'] . '/assign/' . $form_state['values']['assign_number'] . '/submissions/' . $form_state['values']['student_id'] . '/works');

}

/**
 *   Display work delete page
 *
 *   
 */ 
function work_delete($in_course_id, $in_assign_number, $in_student_id, $in_work_id) {

  return drupal_get_form('work_delete_page', $in_course_id, $in_assign_number, $in_student_id, $in_work_id);

}

/**
 *   Work delete page - form
 *
 *   
 */ 
 function work_delete_page(&$form_state, $in_course_id, $in_assign_number, $in_student_id, $in_work_id) {
  $form['course_id'] = array(
    '#type' => 'value',
    '#value' => $in_course_id,
  );
  $form['assign_number'] = array(
    '#type' => 'value',
    '#value' => $in_assign_number,
  );
  $form['student_id'] = array(
    '#type' => 'value',
    '#value' => $in_student_id,
  );
  $form['work_id'] = array(
    '#type' => 'value',
    '#value' => $in_work_id,
  );

  $user = user_load(array("uid" => $in_student_id));


  return confirm_form(
  $form, 
    t('Are you sure you want to delete the work of %name?', array('%name' => $user->name)), 
    'course/' . $in_course_id . '/assign/' . $in_assign_number . '/submissions/' . $in_student_id . '/works', 
    '', 
    t('Delete'), 
    t('Cancel'));
}


/**
 *   Work delete page - form submit
 *
 *   
 */ 
 function work_delete_page_submit($form, &$form_state) {

  // remove work

  work_delete_from_db($form_state['values']['work_id']);

  drupal_goto('course/' . $form_state['values']['course_id'] . '/assign/' . $form_state['values']['assign_number'] . '/submissions/' . $form_state['values']['student_id'] . '/works');
}


?>