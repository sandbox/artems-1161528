<?php

/**
 * @file
 * GML Course system
 * 
 * Common functions used by other modules
 * Small code snippets and log functions
 *
 */

 /**
 *   Return "No access" form and log event
 *   
 */
function form_return_noaccess( $in_log = false, $in_show_warning = false,  
								$in_test_user = true, $in_message = NULL, $in_link = NULL) {
  $user_id = $GLOBALS['user']->uid;

  if ( $in_test_user ) {
    if ( $user_id == '0' ) {
      $in_log = false;
    }
  }

  if ( $in_log ) {
    common_log_event( $in_show_warning, false, $in_message, $in_link );
  }
  else if ( $in_show_warning ) {
    drupal_set_message( $in_message, 'error' );
  }

  $output = drupal_access_denied();

  return $output;
}


/**
 *   Return "No access" form and log event
 *   Also can show warnings to user
 */
 function common_log_event( $in_show_warning = false, $in_test_user = true, $in_message = NULL, $in_message_params, $in_link = NULL, $in_type = 'warning' ) {
  $user_id = $GLOBALS['user']->uid;

  if (( $in_test_user ) && ( $user_id == '0' )) {
    return;
  }

  if ( $in_show_warning ) {
    drupal_set_message( t('Nice try. Your attempt has been logged.'), 'error' );
  }

  if ( $in_message ) {
    $in_message .= '<br>';
  }

  switch ( $in_type ) {
    case 'warning':

      $title = 'GML warning';

      watchdog( $title, $in_message . 'Details' . ' : ua [' . check_plain($_SERVER['HTTP_USER_AGENT']) . '].', $in_message_params, WATCHDOG_NOTICE, $in_link );
 
      break;

    case 'notice':

      $title = 'GML notice';

      watchdog( $title, $in_message, $in_message_params, WATCHDOG_NOTICE, $in_link );
      break;

    default:

      $title = 'GML other';

      watchdog( $title, $in_message . 'Details' . ' : ua [' . check_plain($_SERVER['HTTP_USER_AGENT']) . '].', $in_message_params, WATCHDOG_NOTICE, $in_link );
      break;

  }

}


/**
 *   Generate text representation of given size in bytes
 *
 *   @return text sting
 */
function common_str_filesize_to_string( $in_filesize, $in_precision ) {
  $isize = intval( $in_filesize );
  $fsize = (float) $isize;

  $prec = pow( 10.0, (float) ( intval( $in_precision ) ) );

  if ( $isize > ( 1024 * 1024 ) ) {
    $fsize /= ( 1024.0 * 1024.0 );
    $fsize = (float) ( (int) ( $prec * $fsize ) ) / $prec;

    $output = $fsize . ' MiB';
  }
  else if ( $isize > 1024 ) {
    $fsize /= ( 1024.0 );
    $fsize = (float) ( (int) ( $prec * $fsize ) ) / $prec;

    $output = $fsize . ' KiB';
  }
  else {
    $output = $isize . ' B';
  }

  return $output;
}


/**
 *   Makes collapsible block
 *
 *   @return themed html code
 */
 function common_make_collapsible_block( $in_title, $in_body ) {
  $output = theme('fieldset', array('#collapsible' => TRUE, '#title' => t($in_title), '#children' => '<div>' . $in_body . '</div>'));

  return $output;
}


/**
 *   Count comments of given node_id
 *
 *   @return comment count
 */
 function common_get_comments_count( $in_node_id ) {
  

  $sql = "SELECT COUNT(comments.cid) FROM {comments} WHERE nid=%d";

  return db_result(db_query($sql, $in_node_id));
}


/**
 *   Makes collapsible block - collapsed by default
 *
 *   @return themed html code
 */
 function common_make_collapsible_collapsed_block( $in_title, $in_body ) {
  $output = theme('fieldset', array('#collapsible' => TRUE, '#collapsed' => TRUE, '#title' => t($in_title), '#children' => '<div width="400">' . $in_body . '</div>'));

  return $output;
}


/**
 *   Format collapsible comment
 *
 *   @return themed html code
 */
function common_make_collapsible_comment( $in_title, $in_comment ) {
  if ( strlen( $in_comment ) <= 0 ) {
    return '';
  }

  $body = '<div style="margin: 5px 0px 5px 10px;">' .
			common_str_parse_comment( $in_comment ) .
			'</div>';
  return common_make_collapsible_block( $in_title, $body );
}


/**
 *   Format late date text
 *   It late not 0, then text will be red
 *   @return themed html code
 */
 function common_str_get_colored_late_text( $in_late, $in_penalty = NULL) {
  $late_days = (int) $in_late;

  $late_penalty_text = $in_penalty ? $late_days . ' (' . $in_penalty . ')' : $late_days;

  if ( $late_days > 0 ) {
    $result = '<font color="#ee0000">' . $late_penalty_text . '</font>';
  }
  else {
    $result = $late_penalty_text;
  }

  return $result;
}


/**
 *   Format supervisor contact url 
 *   
 *   @return themed html code
 */
 function common_str_get_supervisor_string_by_id( $in_sup_id ) {
  //setting up access to user details, we'll need that later

  $profile_lastname = variable_get('gmlcourse_student_lastname', CONST_PROFILE_LASTNAME_DEFAULT);
  $profile_middlename = variable_get('gmlcourse_student_middlename', CONST_PROFILE_MIDDLENAME_DEFAULT);
  $profile_firstname = variable_get('gmlcourse_student_firstname', CONST_PROFILE_FIRSTNAME_DEFAULT);

  if ( $in_sup_id ) {
    $supervisor_user = user_load( array('uid' => $in_sup_id) );

    if ( $supervisor_user ) {
      $supervisor_name = common_html_bordered_links_class(true) .
							  check_plain($supervisor_user->$profile_lastname) . ' ' .
							  check_plain($supervisor_user->$profile_firstname) .
							  ' ' .
							  l("(contact)", 'user/' . $in_sup_id . '/contact') .
							  common_html_bordered_links_class(false);
    }
    else {
      $supervisor_name = '<i>' . t('Information is obsolete') . '</i>';
    }

  }
  else {
    $supervisor_name = '<i>' . t('None') . '</i>';
  }

  return $supervisor_name;
}


/**
 *   Format supervisor contact url
 *   
 *   @return themed html code
 */
 function common_str_get_check_date_string( $in_date ) {
  if ( $in_date != '1970-01-01 00:00:00' ) {
    $date = db_date_to_edit_date_use_html( $in_date );
  }
  else {
    $date = '<i>' . t('Never') . '</i>';
  }

  return $date;
}


/**
 *   Format grade text
 *   
 *   @return grade text
 */
 function common_str_get_grade_string( $in_grade ) {
  if ( $in_grade === NULL ) {
    return '0.0';
  }

  if ( floor( floatval( $in_grade ) ) == floatval( $in_grade ) ) {
    $result = $in_grade . '.0';
  }
  else {
    $result = $in_grade;
  }

  return $result;
}


/**
 *   Add special GML css class
 *   
 *   @return html code
 */
 function common_html_bordered_links_class( $in_begin ) {
  if ( $in_begin ) {
    return '<div id="gmlborderedlinks">';
  }
  else {
    return '</div>';
  }
}


/**
 *   Add special GML css class
 *   
 *   @return html code
 */
 function common_html_bordered_intable_links_class( $in_begin ) {
  if ( $in_begin ) {
    return '<div id="gmlborderedtbllinks">';
  }
  else {
    return '</div>';
  }
}


/**
 *   Add special GML css class
 *   
 *   @return html code
 */
 function common_html_small_font_class( $in_begin ) {
  if ( $in_begin ) {
    return '<div id="gmlcontent">';
  }
  else {
    return '</div>';
  }
}


/**
 *   Add special GML css class
 *   
 *   @return html code
 */
 function common_html_bordered_class( $in_begin, $color = '#505050' ) {
  if ( $in_begin ) {
    return '<div style="border: ' . $color . ' 1px solid; padding: 5px; margin: 0px;">';
  }
  else {
    return '</div>';
  }
}


/**
 *   Output is green Yes or red No
 *   
 *   @return html code
 */
 function common_get_colored_yesno( $in_flag ) {
  if ( $in_flag ) {
    $result = '<font color=#10a010>' . t('Yes') . '</font>';
  }
  else {
    $result = '<font color=#a02020>' . t('No') . '</font>';
  }

  return $result;
}


/**
 *   Function to synchronize drupal user database with GML courses user database
 *   You can run it after installing module
 *   All existing GML Course user data will be purged
 */
function common_synchronize_database() {
  $profile_lastname = variable_get('gmlcourse_student_lastname', CONST_PROFILE_LASTNAME_DEFAULT);
  $profile_middlename = variable_get('gmlcourse_student_middlename', CONST_PROFILE_MIDDLENAME_DEFAULT);
  $profile_firstname = variable_get('gmlcourse_student_firstname', CONST_PROFILE_FIRSTNAME_DEFAULT);

  db_query("START TRANSACTION");

  db_query("DELETE FROM {gmlcourse_userinfo}");

  $result = db_query("SELECT uid FROM {users}");

  while ( $user_db = db_fetch_array( $result ) ) {
    $user = user_load( array('uid' => $user_db['uid']) );

    db_query( "INSERT INTO {gmlcourse_userinfo}
				( student_id, login, first_name, middle_name, last_name )
				VALUES ( %d, '%s', '%s', '%s', '%s' )", 
				$user_db['uid'], 
				check_plain($user->name), 
				check_plain($user->$profile_firstname), 
				check_plain($user->$profile_middlename), 
				check_plain($user->$profile_lastname) );
  }

  db_query("COMMIT");
}


/**
 *   Flushes drupal menu cache
 * 
 * 
 */
 function common_clear_menu_cache() {
  menu_router_build(TRUE);
  menu_cache_clear_all();
 // drupal_flush_all_caches();  //this guranteed to work
}


/**
 *   Returns footer for comment input box
 * 
 *   @return html code
 */
 function common_get_comment_footer( $in_title ) {
  $result = '<font size="1" color="#a2a3a5">' .
			'[ <b>Limited BBCode</b> is <font color="#e8b3b5">disabled</font>' .
			', <b>HTML</b> is <font color="#e8b3b5">disabled</font> ]';
  $result .= '</font><br>' . $in_title;

  return $result;
}


/**
 *   Returns days since assign finished
 *   used to hide old assign result on student page
 *   @return days count
 */
 function common_get_days_since_assign_finished( $in_assign_date ) {
  $assign_time_arr = date_str2arr( $in_assign_date );
  $current_time_arr = date_str2arr( date("Y-m-d H:i:s") );

  $assign_time = mktime(23, 59, 59, $assign_time_arr[1], $assign_time_arr[2], $assign_time_arr[0]);
  $current_time = mktime($current_time_arr[3], $current_time_arr[4], $current_time_arr[5], 
							$current_time_arr[1], $current_time_arr[2], $current_time_arr[0]);

  $diff_seconds = $current_time - $assign_time;
  return $diff_seconds / 60 / 60 / 24;
}


/**
 *   Returns text describing maximum allowed works per assign
 *   
 *   @return text
 */
function common_get_max_num_works_for_assign_text( $in_assign_id ) {
  $maxnum = variable_get('gmlcourse_max_works_per_assign', GML_COURSE_MAX_WORKS_PER_ASSIGN);

  $result = t('Maximum allowed number of works for this assignment is ') . $maxnum . '.';

  return $result;
}


/**
 *   Used to encode js sting in student interface
 *   
 *   @return text
 */
 function common_encode_js_string( $in_string ) {
  if ( !is_string( $in_string ) ) {
    return '';
  }

  $result = check_plain( $in_string );
  $result = str_replace( '\\', '\\\\', $result );
  $result = str_replace( '&', '\\&', $result );
  $result = str_replace( chr(10), '<br>', $result );
  $result = str_replace( chr(13), '', $result );

  return $result;
}


/**
 *   Returns status link css class name
 *   
 *   @return text
 */
function common_get_status_link_class() {
  return 'gmlstatus';
}


/**
 *   Check if array is empty
 *   
 *   @return binary
 */
function common_is_array_empty( $in_array ) {
  if ( !$in_array ) {
    return TRUE;
  }
  if ( !is_array( $in_array ) ) {
    return TRUE;
  }
  if ( count( $in_array ) <= 0 ) {
    return TRUE;
  }

  return FALSE;
}

function common_str_parse_comment( $in_comment )
{
  // FIXME
  // todo : implement bbcode
  $result = str_replace( "\n", '<br>', check_plain( $in_comment ) );

  return $result;
}


/**
 *   Generates text used ti notify supervisor about work confirmation
 *   
 *   @return text
 */
function common_supervisor_notify_mail_text($in_course_title,   $in_assign_title, $in_student_name, $in_date, $in_check_url) {

  $text .= t('Hello!') . '<br/><br/>';
  $text .= $in_date . t(' user with name ') . $in_student_name . t(' confirmed new work for') . '<br/>';
  $text .= t('course') . ' <i>"' . $in_course_title . '"</i> ' . t('and assign') . ' <i>"' . $in_assign_title . '"</i>';
  $text .= '<br/>';
  $text .= t('You can check this work using ') . l('check url', $in_check_url, array('absolute' => TRUE));
  $text .= '<br/><br/><br/>';
  $text .= '<i>' .t('Courses system') . '.</i>';

  return $text;
}

?>