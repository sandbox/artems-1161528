<?php

/**
 * @file
 * GML Course system
 * 
 * Db layer function for assign components subsystem
 *
 */


/**
 *   Make assign table name from course id and assign number
 *
 *   @return - assign table name   
 */ 
function _component_make_assign_table_name($in_course_id, $in_assign_number) {
  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);
  $tablename = 'gmlcourse_assign_' . $assign['id'];

  return $tablename;
}


/**
 *   Make assign table name from assign id
 *
 *   @return - assign table name   
 */ 
function _component_make_assign_table_name_by_id($in_assign_id) {
  $tablename = 'gmlcourse_assign_' . $in_assign_id;

  return $tablename;
}


/**
 *   Fetch components for assign
 *
 *   @return - array of components
 */ 
function component_fetch_components($in_course_id, $in_assign_number) {

  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);
  _component_create_table($tablename); //make sure that components table exists

  $result = db_query(
    "SELECT * FROM {gmlcourse_assign_components} WHERE course_id=%d and assign_number=%d ORDER by number", $in_course_id, $in_assign_number);

  $components = array();
  while ($component = db_fetch_array($result)) {
    $components[] = $component;
  }

  return $components;
}


/**
 *   Get components table schema
 *
 *   @return - db table schema
 */
 function _component_get_table_schema() {
  $table = array(
    'description' => '',
    'fields' => array(
      'student_id' => array(
        'description' => '',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => '',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
	  'was_on_appeal' => array(
        'description' => '',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'nid' => array(
        'description' => '',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'supervisor_id' => array(
        'description' => '',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'comment' => array(
        'description' => '',
        'type' => 'text',
        'not null' => FALSE,
      ),
      'check_date' => array(
        'description' => '',
        'type' => 'datetime',
        'not null' => TRUE,
        'default' => '1970-01-01 00:00:00',
      ),
    ),
    'primary key' => array('student_id'),
  );

  return $table;
}


/**
 *   Make sure that input table exists
 *   If not  - we will create it
 *   
 */
 function _component_create_table($in_tablename) {

  if (!db_table_exists($in_tablename)) {
    db_create_table(&$result, $in_tablename, _component_get_table_schema());
  }

}


/**
 *   Find assign components by lofic number 
 *
 *   @return - component array 
 */
 function _find_component_by_num($in_number, $in_course_id, $in_assign_number) {
  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);

  $components = component_fetch_components($in_course_id, $in_assign_number);

  foreach ($components as $component) {
    if ($component['number'] == $in_number) {
      return $component['db_number'];
    }

  }
}


/**
 *   Add component to db
 *
 *   
 */
 function component_add_component($in_course_id, $in_assign_number, $in_title, $in_number, $in_max, $in_intransaction = FALSE) {
  if (!$in_intransaction) {
    db_query("START TRANSACTION");
  }

  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);

  db_query("
    INSERT INTO {gmlcourse_assign_components} 
    (course_id, assign_number, max, number, title) 
    VALUES (%d, %d, %d, %d, '%s')",  
    $in_course_id, 
	$in_assign_number, 
	$in_max, 
	$in_number, 
	$in_title);

  $number = db_last_insert_id('gmlcourse_assign_components', 'db_number');
  //here we add special field to assign submission table
  db_add_field(&$ret, $tablename, 'comp_' . $number, array(
    'type' => 'float',
    'default' => 0,
  ));

  if (!$in_intransaction) {
    db_query("COMMIT");
  }
}

/**
 *   Update component info in db
 *
 *   
 */
 function component_update_component($in_course_id, $in_assign_number, $in_old_number, $in_title, $in_number, $in_max, $in_intransaction = FALSE) {

  if (!$in_intransaction) {
    db_query("START TRANSACTION");
  }


  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);


  db_query("
    UPDATE {gmlcourse_assign_components} SET 
    max = %d, number = %d, title = '%s'
    WHERE number = %d and course_id = %d and assign_number = %d",  
    $in_max, 
	$in_number, 
    $in_title, 
    $in_old_number, 
    $in_course_id, 
    $in_assign_number
	);

  if (!$in_intransaction) {
    db_query("COMMIT");
  }

}


/**
 *   Fetch component by logical number
 *
 *   @return component array 
 */
function component_fetch_component_by_number($in_course_id, $in_assign_number, $in_component_number) {
  $result = db_query(
    "SELECT * FROM {gmlcourse_assign_components} WHERE course_id=%d and assign_number=%d and number=%d", $in_course_id, $in_assign_number, $in_component_number);

  $comp = db_fetch_array($result);

  return $comp;
}


/**
 *   Delete component from db
 *   Also all assign result for this component will be purged
 *   
 */
 function component_delete_from_db($in_course_id, $in_assign_number, $in_component_number, $in_intransaction = FALSE) {

  if (!$in_intransaction) {
    db_query("START TRANSACTION");
  }

  $component = component_fetch_component_by_number($in_course_id, $in_assign_number, $in_component_number);
  //delete info
  $result = db_query(
    "DELETE FROM {gmlcourse_assign_components} WHERE course_id=%d and assign_number=%d and number=%d", $in_course_id, $in_assign_number, $in_component_number);

  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);
  //delete results fro this component in assign table
  db_drop_field(&$ret, $tablename, 'comp_' . $component['db_number']);

  if (!$in_intransaction) {
    db_query("COMMIT");
  }

}

/**
 *   Delete all components from db
 *   Used in assign deletion process
 *   
 */
function component_delete_all_from_db($in_course_id, $in_assign_number, $in_intransaction = FALSE) {
  $tablename = _component_make_assign_table_name($in_course_id, $in_assign_number);

  $result = db_query(
    "DELETE FROM {gmlcourse_assign_components} WHERE course_id=%d and assign_number=%d", $in_course_id, $in_assign_number);
  //drop assign results table
  db_drop_table(&$ret, $tablename);

}

?>