<?php

/**
 * @file
 * GML Course system
 * 
 * Works administration db functions
 *
 */

/**
 *   Add new group to db
 *
 *   
 */ 
function group_add_group($in_title, $in_weight, $in_course_id) {
  db_query("
    INSERT INTO {gmlcourse_groups}
    (title, weight, course_id)
    VALUES ('%s', %d, %d)", 
    $in_title, 
    $in_weight, 
    $in_course_id);
}

/**
 *   Update group info in db
 *
 *   
 */ 
function group_update_group($in_group_id, $in_title, $in_weight) {
  db_query("
    UPDATE {gmlcourse_groups} SET
    title='%s', weight=%d
    WHERE id=%d",  
    $in_title, 
    $in_weight, 
    $in_group_id);
}

/**
 *   Delete group info from db
 *   It removes only course-student relations, so you can get orphan students after using it
 *   
 */ 
function group_delete_from_db($in_group_id) {
  db_query("START TRANSACTION");

  student_delete_all_from_group($in_group_id, TRUE);
  db_query("DELETE FROM {gmlcourse_groups} WHERE id=%d", $in_group_id);

  db_query("COMMIT");
}


/**
 *   Delete group info from db
 *   It removes only course-student relations, so you can get orphan students after using it
 *   
 */ 
function group_delete_all_from_db($in_course_id, $in_transaction) {
  if (!$in_intransaction) {
    db_query("START TRANSACTION");
  }

  // remove students 

  $groups = group_fetch_groups($in_course_id);
  foreach ($groups as $group) {
    student_delete_all_from_group($group['id'], TRUE);
  }

  db_query("DELETE FROM {gmlcourse_groups} WHERE course_id=%d", $in_course_id);

  if (!$in_intransaction) {
    db_query("COMMIT");
  }
}

/**
 *   Fetch group info by group name
 *   
 *   
 */ 
 function group_in_course_fetch_by_name($in_course_id, $in_group_name) {
  $result = db_query("SELECT * FROM {gmlcourse_groups} WHERE title='%s' AND course_id=%d", $in_group_name, $in_course_id);
  return db_fetch_array($result);
}

/**
 *   Fetch group info by id
 *   
 *   
 */ 
function group_fetch_group($in_group_id) {
  $result = db_query("SELECT * FROM {gmlcourse_groups} WHERE id=%d", $in_group_id);

  return db_fetch_array($result);

}

/**
 *   Fetch groups info arrays fro given course
 *   
 *   
 */ 
 function group_fetch_groups($in_course_id) {
  $result = db_query("SELECT * FROM {gmlcourse_groups} WHERE course_id=%d ORDER BY weight ASC", $in_course_id);
  $groups = array();
  while ($group = db_fetch_array($result)) {
    $groups[] = $group;
  }

  return $groups;
}


?>