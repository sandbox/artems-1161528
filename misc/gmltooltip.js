
var topColor, subColor, ContentInfo;

var mouse_X;
var mouse_Y;

var tip_active = 0;

ContentInfo = "";

topColor = "#d2d8ff";
subColor = "#eceeff";

function update_tip_pos()
{
  var tooltip = document.getElementById('GMLToolTip');

  tooltip.style.left = mouse_X + 20 + "px";
  tooltip.style.top  = mouse_Y + "px";
}

if ( window.Event && document.captureEvents )
	document.captureEvents(Event.MOUSEMOVE);

var ie = document.all?true:false;

document.onmousemove = getMouseXY;

function getMouseXY(e)
{
  if (!e) var e = window.event || window.Event;

  if ( !ie )
  { 
	mouse_X = e.pageX;
	mouse_Y = e.pageY;
  }
  else
  {
	// grab the x-y pos.s if browser is IE
	mouse_X = event.clientX + document.body.scrollLeft;
	mouse_Y = event.clientY + document.body.scrollTop;
  }
  
  if (mouse_X < 0){mouse_X = 0;}
  if (mouse_Y < 0){mouse_Y = 0;}

  if(tip_active) { update_tip_pos(); }
}

function EnterContent(TTitle, TContent)
{
  ContentInfo = '<div style="border: #404040 1px solid;">'+
				  '<div style="width: 100%; background-color: '+topColor+'; border-bottom: #202020 1px solid;">'+
					  '<div align=center style="padding: 0px 5px;"><font class="tooltiptitle">'+
					  TTitle+
					  '</font></div>'+
				  '</div>'+
				  '<div style="width: 100%; background-color: '+subColor+';">'+
					  '<div align=center style="padding: 0px 5px;"><font class="tooltipcontent">'+
					  TContent+
					  '</div>'+
				  '</div>'+
				'</div>';
}

function tip_it(which, TTitle, TContent)
{
  var tooltip = document.getElementById('GMLToolTip');

  if(which)
  {
	update_tip_pos();
	tip_active = 1;
	tooltip.style.visibility = "visible";
	EnterContent(TTitle, TContent);
	tooltip.innerHTML = ContentInfo;
  }
  else
  {
	tip_active = 0;
	tooltip.style.visibility = "hidden";
  }
}