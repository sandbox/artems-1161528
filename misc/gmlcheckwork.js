if (Drupal.jsEnabled)
{
  $(document).ready(GMLComputeSummaryGrade);
}

function GMLFillEmptyGradeFieldsWithZeroes()
{
  $(".gmlgradebox").each(function() {

   if(this.value.length==0) {
        this.value = 0;
   }
  });
}

function GMLComputeSummaryGrade()
{
  var grade = 0;

  $(".gmlgradebox").each(function() {

   //add only if the value is number
   if(!isNaN(this.value) && this.value.length!=0) {
        grade += parseFloat(this.value);
   }
  });
   
  //make output looking in old style way ;) 
  if ( !isNaN(grade) )
  {
	if ( ( grade % 1.0 ) == 0  ) grade = grade.toFixed(1);
	else if ( ( grade % 0.01 ) != 0  ) grade = grade.toFixed(2);
  }
  else
  {
	grade = 'error';
  }
  
 $(".gmlsummarygrade").html(grade);
  

}