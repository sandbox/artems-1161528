// Depends on some Drupal js functions.

var workhlelemclass;
workhlelemclass = 'gmltrhighlightstatic';

if (Drupal.jsEnabled)
{
 $(document).ready(GMLcollapseAutoAttach);
}

function GMLcollapseAutoAttach()
{
  
  $('.gmlcollapsible').click(function()
	{
	  var worklinktr;
	  var tagname = this.id;
	  var fieldset = document.getElementsByTagName(tagname);
	  fieldset = fieldset[0].parentNode;
	

	  worklinktr = this.parentNode.parentNode.parentNode;
     $(tagname).toggle('slow');
	// $(worklinktr).localScroll();   //temprorary disabled functionality
    //  $(fieldset).toggleClass('collapsed');
	

	  $(worklinktr).toggleClass(workhlelemclass);

      return false;
    });
 
}


function GMLCollapseAllElementsOnPage(collapse)
{
  var worklinks = document.getElementsByTagName('a');
  var worklink;
  var worklinktr;
  
  
  if ( collapse )
	{
	 
     $('.gmlassignline').removeClass(workhlelemclass);
	 $('.gmlhidden').hide();
	 
	}
	else
	{
	 $('.gmlassignline').addClass(workhlelemclass);
	 $('.gmlhidden').show();
	 
	}

  
}

function GMLcollapseEnsureErrorsVisible(fieldset)
{
  if (! ($(fieldset).is('collapsed')) )
  {
    return;
  }

  var inputs = [];

  inputs = inputs.concat(fieldset.getElementsByTagName('input'));
  inputs = inputs.concat(fieldset.getElementsByTagName('textarea'));
  inputs = inputs.concat(fieldset.getElementsByTagName('select'));

  for (var j = 0; j<3; j++)
  {
    for (var i = 0; i < inputs[j].length; i++)
    {
      if ($(inputs[j][i]).is('error'))
	  {
        return $(fieldset).toggleClass('collapsed'); //was removeClass('collapsed');
      }
    }
  }
}

function GMLcollapseScrollIntoView(node)
{
  var h = self.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || 0;
  var offset = self.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
  var pos = absolutePosition(node);

  if (pos.y + node.scrollHeight > h + offset)
  {
    if (node.scrollHeight > h)
	{
      window.scrollTo(0, pos.y);
    }
	else
	{
      window.scrollTo(0, pos.y + node.scrollHeight - h);
    }
  }
}
