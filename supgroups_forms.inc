<?php

/**
 * @file
 * GML Course system
 * 
 * User interface functions for supervisor groups control
 *
 */
 
 /**
 *   List supervisor groups
 *
 *   
 */ 
function supgroups_list_supgroups() {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  $header = array(t('Group title'), t('Weight'), array(
      'data' => t('Operations'),
      'colspan' => 4,
    ));
  //custom table
  $prefix_once = '<div style="width: 500px">';
  $postfix_once = '</div>';

  $prefix2_once = '<div style="width: 300px">';
  $postfix2_once = '</div>';

  $prefix3_once = '<div style="width: 120px">';
  $postfix3_once = '</div>';

  $sub_prefix_once = '<div style="width: 380px">';
  $sub_postfix_once = '</div>';

  $subtable_prefix_once = '<div style="margin-left: 10px;">';
  $subtable_postfix_once = '</div>';

  $rows = array();

  $supgroups = supgroups_fetch_supgroups();

  foreach ( $supgroups as $supgroup ) {
    $guheader = array(t('Login'), t('Name'));
    $gurows = array();

    $members = supgroups_fetch_members( $supgroup['group_id'] );

    foreach ( $members as $member ) {
      $member_name = $member['last_name'] . ' ' . $member['first_name'] . ' ' . $member['middle_name'];

      $gurows[] = array(
        $member['login'],
        $sub_prefix_once . $member_name . $sub_postfix_once,
      );
    }

    $gaheader = array(t('Course'), t('Assignment'));
    $garows = array();

    $permissions = supgroup_fetch_supgroup_permissions( $supgroup['group_id'] );

    $rows_courses['active'] = array();
    $rows_courses['inactive'] = array();

    foreach ( $permissions as $permission ) {
      $type = 'active';

      if ( $permission['course_id'] > 0 ) {
        $course = course_fetch_course( $permission['course_id'] );
        $course_name = $course['title'];

        if ( !$course['active'] ) {
          $type = 'inactive';
        }
      }
      else {
        $course_name = '<i>' . t('Any') . '</i>';
      }

      $course_name = $prefix2_once . $course_name . $postfix2_once;

      if ( $permission['assign_id'] > 0 ) {
        $assign = assign_fetch_assign( $permission['assign_id'] );
        $assign_name = $assign['title'];
      }
      else {
        $assign_name = '<i>' . t('Any') . '</i>';
      }

      $assign_name = $prefix3_once . $assign_name . $postfix3_once;

      $rows_courses[$type][] = array($course_name, $assign_name);
    }

    if ( count( $rows_courses['inactive'] ) > 0 ) {
      $rows_courses['splitter'] = array(array(array(
            'data' => '<b>' . t('Inactive') . '</b>',
            'colspan' => 2,
          )));
    }
    else {
      $rows_courses['splitter'] = array();
    }

    $garows = array_merge( $rows_courses['active'], $rows_courses['splitter'], $rows_courses['inactive'] );
    //collapsed info
    $contents = '<div style="margin: 5px 0px;">' . common_html_bordered_class( true );
    $contents .= '<b>' . t('Group overview') . '</b><br><br>';
    $contents .= t('Group members') . '<br>';
    $contents .= $subtable_prefix_once . theme( 'table', $guheader, $gurows ) . $subtable_postfix_once;
    $contents .= '<br>';
    $contents .= t('Group access') . '<br>';
    $contents .= $subtable_prefix_once . theme( 'table', $gaheader, $garows ) . $subtable_postfix_once;
    $contents .= '<br>';
    if ( $supgroup['description'] ) {
      $contents .= '<b>' . t('Description') . '</b><br>';
      $contents .= $subtable_prefix_once . common_str_parse_comment( $supgroup['description'] ) . $subtable_postfix_once;
      $contents .= '<br>';
    }
    $contents .= common_html_bordered_class( false ) . '</div>';

    $contents = theme('fieldset', array(
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => $supgroup['title'],
      '#children' => $contents,
      '#attributes' => array('class' => 'gmlgreyborder'),
    ) );

    $rows[] = array(
      array(
        'class' => 'comment',
        'data' => $prefix_once . $contents . $postfix_once,
      ),
      array(
        'data' => $supgroup['weight'],
        'valign' => 'top',
      ),
      array(
        'data' => l( t('edit'), 'supervisors/groups/' . $supgroup['group_id'] . '/edit' ),
        'valign' => 'top',
      ),
      array(
        'data' => l( t('delete'), 'supervisors/groups/' . $supgroup['group_id'] . '/delete' ),
        'valign' => 'top',
      ),
      array(
        'data' => l( t('members'), 'supervisors/groups/' . $supgroup['group_id'] . '/members' ),
        'valign' => 'top',
      ),
      array(
        'data' => l( t('access'), 'supervisors/groups/' . $supgroup['group_id'] . '/access' ),
        'valign' => 'top',
      ),
    );
  }

  $output = common_html_small_font_class( true );
  $output .= common_html_bordered_intable_links_class( true );
  $output .= theme( 'table', $header, $rows );
  $output .= common_html_bordered_intable_links_class( false );
  $output .= common_html_small_font_class( false );

  return $output;
}

/**
 *   Add/edit supervisor group page
 *
 *   
 */ 
function supgroup_add_edit( $in_group_id = NULL ) {
  return drupal_get_form('supgroup_add_edit_form', $in_group_id);
}

/**
 *   Add/edit supervisor group page - form
 *
 *   
 */ 
 function supgroup_add_edit_form(&$form_state,   $in_group_id ) {

  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  $flag_new = true;

  $edit = array();

  if ( $in_group_id ) {
    $edit = supgroup_fetch_supgroup( $in_group_id );

    if ( $edit ) {
      $form['group_id'] = array(
        '#type' => 'value',
        '#value' => $edit['group_id'],
      );
      $flag_new = false;
    }
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Group title'),
    '#default_value' => $edit['title'],
    '#required' => TRUE,
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $edit['weight'],
    '#description' => t('Group weight'),
    '#required' => FALSE,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#rows' => 4,
    '#title' => t('Description'),
    '#default_value' => $edit['description'],
    '#description' => t('Group description'),
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t( $flag_new ? 'Create new group' : 'Submit' ),
    '#weight' => 30,
  );

  return $form;
}

/**
 *   Add/edit supervisor group page - form submit handler
 *
 *   
 */ 
 function supgroup_add_edit_form_submit($form, &$form_state) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  if ( $form_state['values']['group_id'] ) {
    supgroup_update_group( $form_state['values']['group_id'], $form_state['values']['title'], $form_state['values']['weight'], $form_state['values']['description'] );

    drupal_set_message( t('Supervisor group "%title" has been updated.', array('%title' => $form_state['values']['title']) ) );
  }
  else {
    supgroup_add_group( $form_state['values']['title'], $form_state['values']['weight'], $form_state['values']['description'] );

    drupal_set_message( t('Supervisor group "%title" has been added.', array('%title' => $form_state['values']['title']) ) );
  }

  drupal_goto('supervisors/groups');
}

/**
 *   Delete supervisor group page
 *
 *   
 */ 
 function supgroup_delete( $in_group_id ) {

  return drupal_get_form('supgroup_delete_page', $in_group_id);
}

/**
 *   Delete supervisor group page - form
 *
 *   
 */ 
 function supgroup_delete_page(&$form_state,   $in_group_id ) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  $supgroup = supgroup_fetch_supgroup( $in_group_id );

  if ( !$supgroup ) {
    drupal_goto( 'supervisors/groups' );
    return;
  }

  $form['group_id'] = array(
    '#type' => 'value',
    '#value' => $in_group_id,
  );

  return confirm_form(
    $form, 
    t('Are you sure you want to delete the supervisor group') . '<br>&laquo;' . $supgroup['title'] . '&raquo;?', 
    'supervisors/groups', 
    '<br>' . t('This action can not be undone.') . '<br><br>', 
    t('Delete'), 
    t('Cancel') );
}

/**
 *   Add/edit supervisor group page - form submit handler
 *
 *   
 */ 
 function supgroup_delete_page_submit($form, &$form_state) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  // remove supervisor group

  supgroup_delete_from_db( $form_state['values']['group_id'] );
  //we need to rebuild menu, will be not in use in future versions
  common_clear_menu_cache();

  drupal_goto('supervisors/groups');
}

/**
 *   Edit supervisor groups members page
 *
 *   
 */ 
 function supgroup_edit_members( $in_group_id ) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  $supgroup = supgroup_fetch_supgroup( $in_group_id );

  if ( !$supgroup ) {
    drupal_goto( 'supervisors/groups' );
    return;
  }

  $members = supgroups_fetch_members( $in_group_id );

  $header = array(t('Login'), t('Name'), t('Operations'));

  $prefix_once = '<div style="width: 500px">';
  $postfix_once = '</div>';

  $rows = array();

  foreach ( $members as $member ) {
    $member_name = $member['last_name'] . ' ' . $member['first_name'] . ' ' . $member['middle_name'];

    $rows[] = array(
      $member['login'],
      $prefix_once . $member_name . $postfix_once,
      l( t('remove'), 'supervisors/groups/' . $supgroup['group_id'] . '/member/' .	$member['user_id'] . '/remove' ),
    );
  }

  $contents = '<div>';
  $contents .= drupal_get_form('supgroup_member_add', $in_group_id);
  $contents .= '</div>';

  $contents = theme('fieldset', array(
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Add supervisor to the group'),
    '#children' => $contents,
    '#attributes' => array('class' => 'gmlgreyborder'),
  ) );

  $output = common_html_small_font_class( true );
  $output .= common_html_bordered_intable_links_class( true );
  $output .= '<br><h3>' . t('Group') . ' &laquo;' . $supgroup['title'] . '&raquo</h3><br><br>';
  $output .= theme( 'table', $header, $rows );
  $output .= '<br><br>';
  $output .= $contents;
  $output .= common_html_bordered_intable_links_class( false );
  $output .= common_html_small_font_class( false );

  return $output;
}

/**
 *   Supervisor group member add page
 *
 *   
 */ 
function supgroup_member_add(&$form_state,   $in_group_id ) {
  $form = array();

  $form['group_id'] = array(
    '#type' => 'value',
    '#value' => $in_group_id,
  );
  //we use member name for add new user
  // we search only for user with supervisor permission
  $form['user_name'] = array(
    '#type' => 'textfield',
    '#title' => t('User name'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Supervisor name (last, first, middle)'),
    '#autocomplete_path' => 'user/autocomplete_userinfo_supervisor',
    '#default_value' => '',
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add users'),
    '#weight' => 30,
  );

  return $form;
}

/**
 *   Supervisor group member add page - submit handler
 *
 *   
 */ 
function supgroup_member_add_submit($form, &$form_state) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  $supgroup = supgroup_fetch_supgroup( $form_state['values']['group_id'] );

  if ( !$supgroup ) {
    drupal_goto( 'supervisors/groups' );
    return;
  }

  $flag_error = true;

  if ( $form_state['values']['user_name'] ) {
    $user = student_get_user_from_name( $form_state['values']['user_name'] );

    if ( $user ) {
      $flag_error = false;

      if ( user_access( 'access supervisor front-end', $user ) ) {
        if ( !supgroup_test_member( $supgroup['group_id'], $user->uid ) ) {
          supgroups_add_member( $supgroup['group_id'], $user->uid );
          drupal_set_message( t('User has been added.'), 'status' );

          common_clear_menu_cache();
        }
        else {
          drupal_set_message( t('Error : specified user is already a member of this group.'), 'error' );
        }
      }
      else {
        drupal_set_message( t('Error : specified user has no supervisor privileges.'), 'error' );
      }
    }
  }

  if ( $flag_error ) {
    drupal_set_message( t('Error : could not find user with such name or multiple users found.'), 'error' );
  }
}

/**
 *   Supervisor group member delete page - submit handler
 *
 *   
 */
function supgroup_member_remove( $in_group_id, $in_member_id ) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  supgroup_remove_member( $in_group_id, $in_member_id );

  common_clear_menu_cache();

  drupal_goto( 'supervisors/groups/' . $in_group_id . '/members' );
}

/**
 *   Supervisor group  - access edit page
 *
 *   
 */
function supgroup_edit_access($in_group_id) {

  $output .= drupal_get_form('supgroup_edit_access_form', $in_group_id);

  return $output;
}

/**
 *   Supervisor group  - access edit page - form
 *
 *   
 */
function supgroup_edit_access_form(&$form_state,   $in_group_id ) {

  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  $supgroup = supgroup_fetch_supgroup( $in_group_id );

  if ( !$supgroup ) {
    drupal_goto( 'supervisors/groups' );
    return;
  }
  //style for active courses
  $active_font_prefix = '<span style="color:#d00000; display:block">';
  $active_font_postfix = '</span>';


  $permissions = supgroup_fetch_supgroup_permissions( $in_group_id );

  $form = array();

  $form['var_group_id'] = array(
    '#type' => 'value',
    '#value' => $in_group_id,
  );

  $contents = '<br><b>' . t('Supervisor group') . ' &laquo;' . $supgroup['title'] . '&raquo; ' .
				t('access settings.') . '</b><br>';

  $form[] = array(
    '#type' => 'markup',
    '#value' => $contents,
  );

  $flag_set = in_array( array(
    'group_id' => $in_group_id,
    'course_id' => '-1',
    'assign_id' => '-1',
  ), $permissions );

  $title = t('Grant this group permission to all the courses and assignments.');
  if ( $flag_set ) {
    $title = $active_font_prefix . $title . $active_font_postfix;
  }

  $form['cb_all_courses'] = array(
    '#type' => 'checkbox',
    '#title' => $title,
    '#default_value' => $flag_set,
  );

  $flag_inactive_course_allowed = false;

  $courses = course_fetch_courses();
  
  function cmp_state($a, $b) {
    if ($a['active'] == $b['active']) {
      return 0;
    }
    return ($a['active'] < $b['active']) ? 1 : -1;
  }

  $courses = course_fetch_courses();
  // sort active courses first

  usort($courses, 'cmp_state');

  $value_items = array();
  $form_courses = array();
  $form_courses['active'] = array();
  $form_courses['inactive'] = array();

  foreach ( $courses as $course ) {
    if ( $course['active'] ) {
      $var = 'active';
      $flag_inactive = false;
    }
    else {
      $var = 'inactive';
      $flag_inactive = true;
    }

    $course_id = $course['id'];
    $course_var = 'course_' . $course_id;
    $course_cb_var = 'cb_' . $course_var;

    $contents = '<div style="margin: 0px 0px 3px 0px;">';

    $form_courses[$var][] = array(
      '#type' => 'markup',
      '#value' => $contents,
    );

    $form_courses[$var][$course_var] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => $course['title'],
      '#attributes' => array('class' => 'gmlgreyborder'),
    );

    $flag_course_allowed = false;

    $contents = '<div style="margin: 5px 0px 10px 0px;">' . common_html_bordered_class( true, '#808080' );

    $form_courses[$var][$course_var][] = array(
      '#type' => 'markup',
      '#value' => $contents,
    );

    $flag_set = in_array( array(
      'group_id' => $in_group_id,
      'course_id' => $course_id,
      'assign_id' => '-1',
    ), $permissions );

    $flag_course_allowed |= $flag_set;
    if ( $flag_inactive ) {
      $flag_inactive_course_allowed |= $flag_set;
    }

    $form_courses[$var][$course_var][$course_cb_var] = array(
      '#type' => 'checkbox',
      '#title' => t('Grant this group permission to all assignments.'),
      '#default_value' => $flag_set,
    );

    $contents = '<div style="background-color: #b0b0b0; height: 1px; width: 60%;"></div>';
    $contents .= '<div style="margin-left: 10px;">';

    $form_courses[$var][$course_var][] = array(
      '#type' => 'markup',
      '#value' => $contents,
    );

    $value_assigns = array();

    $assigns = assign_fetch_assigns( $course['id'] );
    //prepare option for each assign in course
    foreach ( $assigns as $assign ) {
      $assign_id = $assign['id'];
      $assign_var = 'cb_' . $course_var . '_assign_' . $assign_id;

      $flag_set = in_array( array(
        'group_id' => $in_group_id,
        'course_id' => $course_id,
        'assign_id' => $assign_id,
      ), $permissions );

      $flag_course_allowed |= $flag_set;
      if ( $flag_inactive ) {
        $flag_inactive_course_allowed |= $flag_set;
      }

      $form_courses[$var][$course_var][$assign_var] = array(
        '#type' => 'checkbox',
        '#title' => $assign['title'],
        '#default_value' => $flag_set,
      );

      $value_assigns[] = array(
        'name' => $assign_var,
        'assign_id' => $assign_id,
      );
    }

    //add assigns here

    $contents = '</div>';
    $contents .= common_html_bordered_class( false ) . '</div>';
    $form_courses[$var]['course_' . $course['id']][] = array(
      '#type' => 'markup',
      '#value' => $contents,
    );

    $contents = '</div>';
    $form_courses[$var][] = array(
      '#type' => 'markup',
      '#value' => $contents,
    );

    if ( $flag_course_allowed ) {
      $form_courses[$var][$course_var]['#title'] =  $active_font_prefix . $course['title'] . $active_font_postfix;
    }

    $value_items[] = array(
      'name' => $course_cb_var,
      'course_id' => $course_id,
      'items' => $value_assigns,
    );
  }

  $form = array_merge( $form, $form_courses['active'] );

  $form[] = array(
    '#type' => 'markup',
    '#value' => '<br>',
  );

  $title = t('[ Inactive courses ]');
  if ( $flag_inactive_course_allowed ) {
    $title = $active_font_prefix . $title . $active_font_postfix;
  }

  $form['inactive_courses'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => $title,
  );

  $form['inactive_courses'] = array_merge( $form['inactive_courses'], $form_courses['inactive'] );

  $form[] = array(
    '#type' => 'markup',
    '#value' => '<br>',
  );

  $form['var_items'] = array(
    '#type' => 'value',
    '#value' => $value_items,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t( 'Save permissions' ),
    '#weight' => 30,
  );

  return $form;
}



/**
 *   Supervisor group  - access edit page - form submit
 *   Permission setting -1 means allow for all assings for course
 *
 *   Save supgroups assign permissions in db
 */
function supgroup_edit_access_form_submit($form, &$form_state) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  if ( !$form_state['values']['var_items'] ) {
    drupal_set_message( t('Error : Invalid form values.'), 'error' );
    drupal_goto( 'supervisors/groups/' . $form_state['values']['group_id'] . '/access' );
    return;
  }

  $group_id = $form_state['values']['var_group_id'];

  db_query("DELETE FROM {gmlcourse_supgroups2permissions} WHERE group_id = %d", $group_id);

  if ( $form_state['values']['cb_all_courses'] ) {
    db_query("INSERT INTO {gmlcourse_supgroups2permissions}
				(group_id, course_id, assign_id)
				VALUES ( %d, -1, -1 )", 
				$group_id );
  }

  $courses = $form_state['values']['var_items'];

  if ( $courses ) {
    foreach ( $courses as $course ) {
      if ( $form_state['values'][$course['name']] ) {
        db_query("INSERT INTO {gmlcourse_supgroups2permissions}
				  (group_id, course_id, assign_id)
				  VALUES ( %d, %d, -1 )", 
				  $group_id, $course['course_id'] );
      }

      $assigns = $course['items'];

      if ( $assigns ) {
        foreach ( $assigns as $assign ) {
          if ( $form_state['values'][$assign['name']] ) {
            db_query("INSERT INTO {gmlcourse_supgroups2permissions}
					  (group_id, course_id, assign_id)
					  VALUES ( %d, %d, %d )", 
					  $group_id, $course['course_id'], $assign['assign_id'] );
          }
        }
      }
    }
  }

  common_clear_menu_cache();

  drupal_set_message( t('Group access settings have been saved.'), 'status' );
  drupal_goto( 'supervisors/groups' );
}

?>