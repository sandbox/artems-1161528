<?php

/**
 * @file
 * GML Course system
 * 
 * User interaction functions for assigns.
 * Handles all interaction with user and logic
 *
 */

//Flag for student scramble function 
define("ASSIGN_FLAG_SCRAMBLESTUDENTINFO", ( 1 << 0 ) );


/**
 *   Display list of assigns for course
 *   
 */
function assigns_generic_display($in_course_id) {
  $output = common_html_small_font_class( true );

  $output .= drupal_get_form('assigns_generic_display_form', $in_course_id);
  $output .= common_html_small_font_class( false );

  return $output;
}


/**
 *   Display list of assigns for course - form function
 *   
 */
function assigns_generic_display_form(&$form_state, $in_course_id) {
  
  $flag_access_maintainer = user_access('administer courses');

  $assigns = assign_fetch_assigns($in_course_id);

  $form = array();

  foreach ($assigns as $i => $assign) {
    $form[$i]['number'] = array(
      '#value' => $assign['number'],
    );

    $form[$i]['title'] = array(
      '#value' => l($assign['title'], $assign['url']),
    );

    $form[$i]['start'] = array(
      '#value' => date('d-m-Y', strtotime($assign['start'])),
    );

    $form[$i]['finish'] = array(
      '#value' => date('d-m-Y', strtotime($assign['finish'])),
    );

    if ( $flag_access_maintainer ) {
      $form[$i]['works_allowed'] = array('#value' => common_get_colored_yesno( $assign['allow_works'] ));

      $form[$i]['notify_send'] = array('#value' => common_get_colored_yesno( $assign['notify_send'] ));

      if ($assign['type']) {
        $assign_type = status_get_assign_type($assign['type']);
      }
      else {
        $assign_type['title'] = '';
      }
      $form[$i]['type'] = array('#value' => $assign_type['title']);

      $form[$i]['submission'] = array(
        '#value' => l(t('submissions'), 'course/' . $in_course_id . '/assign/' . $assign['number'] . '/submissions'),
        '#prefix' => common_html_bordered_links_class(true),
        '#suffix' => common_html_bordered_links_class(false),
      );

      $form[$i]['components'] = array(
        '#value' => l(t('components'), 'course/' . $in_course_id . '/assign/' . $assign['number'] . '/component'),
        '#prefix' => common_html_bordered_links_class(true),
        '#suffix' => common_html_bordered_links_class(false),
      );

      $form[$i]['edit'] = array(
        '#value' => l(t('edit'), 'course/' . $in_course_id . '/assign/' . $assign['number'] .  '/edit'),
        '#prefix' => common_html_bordered_links_class(true),
        '#suffix' => common_html_bordered_links_class(false),
      );

      $form[$i]['delete'] = array(
        '#value' => l(t('delete'), 'course/' . $in_course_id . '/assign/' . $assign['number'] .  '/delete'),
        '#prefix' => common_html_bordered_links_class(true),
        '#suffix' => common_html_bordered_links_class(false),
      );

      if ( $assign['show_results'] ) {
        $results_link_text = t('hide results');
        $results_link = 'course/' . $in_course_id . '/assign/' . $assign['id'] . '/switchsr/0';
      }
      else {
        $results_link_text = t('show results');
        $results_link = 'course/' . $in_course_id . '/assign/' . $assign['id'] . '/switchsr/1';
      }

      $form[$i]['show_results'] = array(
        '#value' => l( $results_link_text, $results_link ),
        '#prefix' => common_html_bordered_links_class(true),
        '#suffix' => common_html_bordered_links_class(false),
      );
    }
  }

  return $form;
}


/**
 *   Display list of assigns for course - theme function
 *   
 */
function theme_assigns_generic_display_form($form) {

  $flag_access_maintainer = user_access('administer courses');

  foreach ($form as $name => $element) {
    if (isset($element['title']) && is_array($element['title'])) {
      $currentrow = array(
        drupal_render($element['number']),
        drupal_render($element['title']),
        drupal_render($element['start']),
        drupal_render($element['finish']),
      );

      if ( $flag_access_maintainer ) {
        array_push( $currentrow,  
									drupal_render($element['works_allowed']), 
									drupal_render($element['notify_send']), 
									drupal_render($element['type']), 
									drupal_render($element['submission']), 
									drupal_render($element['components']),  
									drupal_render($element['edit']), 
									drupal_render($element['delete']), 
									drupal_render($element['show_results']));
      }

      $rows[] = $currentrow;

      unset($form[$name]);
    }
  }

  $header = array(t('Number'), t('Title'), t('Start'), t('Finish'));

  if ( $flag_access_maintainer ) {
    array_push( $header, t('Works'), t('Notify'), t('Type'), array('data' => t('Operations'), 'colspan' => 5) );
  }

  $output = theme('table', $header, $rows,   array('id' => 'assigns'));
  $output .= drupal_render($form);

  return $output;
}


/**
 *   Function for add and edit assign
 *   
 */
 function assign_add_edit($in_course_id = NULL, $in_assign_number = NULL) {
  $output = drupal_get_form('assign_add_edit_form', $in_course_id, $in_assign_number);

  return $output;
}


/**
 *   Function for add and edit assign - form function
 *   Edit form with all parameters
 */
function assign_add_edit_form(&$form_state, $in_course_id = NULL, $in_assign_number = NULL) {

  if ($in_course_id) {
    $form['course_id'] = array(
      '#type' => 'value',
      '#value' => $in_course_id,
    );
  }

  $edit = array();

  if ($in_assign_number) {
    $edit = assign_fetch_assign_by_number($in_course_id, $in_assign_number);
    $form['assign_id'] = array(
      '#type' => 'value',
      '#value' => $edit['id'],
    );

    list($year, $month, $day) = date_str2arr($edit['start']);
    $start_ar = array(
      'year' => $year,
      'month' => $month,
      'day' => $day,
    );

    list($year, $month, $day) = date_str2arr($edit['finish']);
    $finish_ar = array(
      'year' => $year,
      'month' => $month,
      'day' => $day,
    );

    $allow_works = $edit['allow_works'];
    $show_results = $edit['show_results'];
    $auto_penality = $edit['auto_late_penality'];
	$appeal_settings = $edit['appeal_settings'];

    $notify_send = $edit['notify_send'];
    $notify_user_id = $edit['notify_user_id'];


    $flags = intval($edit['flags']);
    $scramble_info = ($flags & ASSIGN_FLAG_SCRAMBLESTUDENTINFO) ? 1 : 0;
  }
  else {
    $current_date = explode( '.', date("d.m.Y") );
    $start_ar = array(
      'year' => $current_date[2],
      'month' => $current_date[1],
      'day' => $current_date[0],
    );
    $finish_ar = $start_ar;

    $allow_works = 1;
    $show_results = 0;
    $scramble_info = 1;
    $auto_penality = 1;
	$appeal_settings = 1;

    $notify_send = 0;
    $notify_user_id = 0;
  }
  //Notify user - selected user will be notified if some confirm assign for course
  if ($notify_user_id) {
    $notify_user_name = student_get_name_from_id( $notify_user_id );
  }
  else {
    $notify_user_name = '';
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Assign title'),
    '#default_value' => $edit['title'],
    '#required' => TRUE,
  );

  $form['short_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Short title'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Assign short title'),
    '#default_value' => $edit['short_title'],
    '#required' => FALSE,
  );

  $form['number'] = array(
    '#type' => 'textfield',
    '#title' => t('Number'),
    '#size' => 2,
    '#maxlength' => 2,
    '#default_value' => $edit['number'],
    '#description' => t('Assign number (starting from 1)'),
    '#required' => TRUE,
  );

  $form['start'] = array(
    '#type' => 'date',
    '#title' => t('Start date'),
    '#default_value' => array(
      'day' => $start_ar['day'],
      'month' => $start_ar['month'],
      'year' => $start_ar['year'],
    ),
    '#description' => t('Assign start date'),
    '#required' => TRUE,
  );

  $form['finish'] = array(
    '#type' => 'date',
    '#title' => t('Finish date'),
    '#default_value' => array(
      'day' => $finish_ar['day'],
      'month' => $finish_ar['month'],
      'year' => $finish_ar['year'],
    ),
    '#description' => t('Assign finish date'),
    '#required' => TRUE,
  );

  $target_types = array('0' => 'no type');
  $statuses = status_get_assign_types();
  foreach ($statuses as $status) {
    $target_types[$status['id']] = $status['title'];
  }

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Select assign type'),
    '#options' => $target_types,
    '#default_value' => $edit['type'],
  );

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#size' => 60,
    '#maxlength' => 250,
    '#default_value' => $edit['url'],
    '#description' => t('A URL link pointing to assign text.'),
  );


  $form['allow_works'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow students to upload works for this assignment.'),
    '#default_value' => $allow_works,
  );

  $form['auto_penality'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add auto late penality component.'),
    '#default_value' => $auto_penality,
  );
  
  $form['appeal_settings'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show appeal related settings on assings check page.'),
    '#default_value' => $appeal_settings,
  );

  $form['show_results'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show assignment check results on the fly.'),
    '#default_value' => $show_results,
  );

  $form['scramble_info'] = array(
    '#type' => 'checkbox',
    '#title' => t('Scramble student info in supervisor forms until the assignment check results are published.'),
    '#default_value' => $scramble_info,
  );

  $form['notify_user_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Notify user'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Supervisor name (last, first, middle)'),
    '#autocomplete_path' => 'user/autocomplete_userinfo_supervisor',
    '#default_value' => $notify_user_name,
  );

  $form['notify_send'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send e-mail notification when student upload work.'),
    '#default_value' => $notify_send,
  );



  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t($in_assign_number ? 'Submit' : 'Create new assign'),
    '#weight' => 30,
  );


  return $form;
}


/**
 *   Function for add and edit assign - submit form function
 *   will add or update assign with given parameters
 */
 function assign_add_edit_form_submit($form, &$form_state) {

  $start_date_str = date_arr2str($form_state['values']['start']['year'], $form_state['values']['start']['month'], $form_state['values']['start']['day']);
  $finish_date_str = date_arr2str($form_state['values']['finish']['year'], $form_state['values']['finish']['month'], $form_state['values']['finish']['day']);

  $flags = 0;

  $notify_user_id = 0;
  $notify_user_id = student_get_id_from_name($form_state['values']['notify_user_name']);

  if ( $form_state['values']['scramble_info'] ) {
    $flags |= ASSIGN_FLAG_SCRAMBLESTUDENTINFO;
  }

  if ($form_state['values']['assign_id']) {
    assign_update_assign(
      $form_state['values']['assign_id'], 
      $form_state['values']['title'],  
	  $form_state['values']['short_title'],  
	  $form_state['values']['number'], 
      $start_date_str, $finish_date_str, $form_state['values']['url'], 
	  $form_state['values']['allow_works'], $form_state['values']['show_results'], $flags, 
	  $form_state['values']['notify_send'], $notify_user_id, $form_state['values']['type'], 
	  $form_state['values']['auto_penality'], $form_state['values']['appeal_settings']);

    drupal_set_message(t('Assign "%title" has been updated', 
      array('%title' => $form_state['values']['title'])));
  }
  else {
    assign_add_assign(
      $form_state['values']['title'], 
	  $form_state['values']['short_title'], 
	  $form_state['values']['number'], 
      $start_date_str, $finish_date_str, $form_state['values']['url'], 
	  $form_state['values']['allow_works'], $form_state['values']['show_results'], 
      $form_state['values']['course_id'], $flags, 
	  $form_state['values']['notify_send'], $notify_user_id, $form_state['values']['type'], 
	  $form_state['values']['auto_penality'], $form_state['values']['appeal_settings']);


    drupal_set_message(t('Assign "%title" has been added',  
      array('%title' => $form_state['values']['title'])));
  }

  drupal_goto('course/' . $form_state['values']['course_id'] . '/assign');

}


/**
 *   Function for assign delete
 *  
 */
function assign_delete($in_course_id = NULL, $in_assign_number = NULL) {
  return drupal_get_form('assign_delete_page', $in_course_id, $in_assign_number);

}


/**
 *   Function for assign delete - form function
 *   Will ask a question - to confirm deleting
 */
function assign_delete_page(&$form_state, $in_course_id = NULL, $in_assign_number = NULL) {
  // get assign course

  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);

  $form['assign_id'] = array(
    '#type' => 'value',
    '#value' => $assign['id'],
  );
  $form['course_id'] = array(
    '#type' => 'value',
    '#value' => $in_course_id,
  );
  
  return confirm_form(
    $form,  
    t('Are you sure you want to delete the assign %name?', array('%name' => $assign['title'])), 
    'course/' . $in_course_id . '/assign', 
    t('This cannot be undone!'), 
    t('Delete'),  
    t('Cancel'));

}


/**
 *   Function for assign delete - submit form function
 *   Actual deleting of assign. Then redirect to assigns list for course
 */
 function assign_delete_page_submit($form, &$form_state) {
  
  assign_delete_from_db($form_state['values']['assign_id']);

  drupal_goto('course/' . $form_state['values']['course_id'] . '/assign');
}


/**
 *   Function for toggle assign results visibility
 *   
 */
 function assign_set_show_results( $in_course_id, $in_assign_id, $in_state ) {
  $assign = assign_fetch_assign( $in_assign_id );

  if ( $assign ) {
    if ( $in_state ) {
      $state = 1;
      $message = t('Check results are now <b>visible</b> for ') . $assign['title'] . '.';
    }
    else {
      $state = 0;
      $message = t('Check results are now <b>hidden</b> for ') . $assign['title'] . '.';
    }
  }
  else {
    drupal_set_message( t('Error : The specified assign does not exist.'), 'error' );
  }

  assign_update_show_results( $in_assign_id, $state );

  drupal_set_message( $message, 'status' );

  drupal_goto( referer_uri() );
}


?>