<?php

/*
 Download System
 Copyright (C) 2006 Ilya Tisevich

 Seriosly modified in 2010.
  
 Important!
 This file is UTF-8 encoded for UTF-8 text transliteration routine.
  
 Original version comments :

 //Somewhat magic script that will push the server file to the client, taking
 //into account client requests like ranges.
 //
 //This is a modified version of the original of charon-dl
 //http://sf.net/projects/charon-dl
 //
 //Copyright (C) 2005, Michiel "El Muerte" Hendriks

 This version has two routines : one uses builtin file transfer function,
 the other one handles transfers itself.
 */

define("READBUFFER_SIZE", 4096); // read buffer size

define("USE_GZIP", true); // use GZip compression for text/* files


//////////////////////////////////////////////////////////////////////////////

/// 

function _gml_course_files_error_no_access($filename = '') {
  return form_return_noaccess();
}

//////////////////////////////////////////////////////////////////////////////

/// 

function _gml_course_files_error_file_does_not_exist( $filename = '' ) {
  return form_return_noaccess();
}

 /**
 *   Check user permission for file
 *
 *   @return - flag
 */ 
 function _gml_course_files_verify_permission( $in_course_id, $in_assign_id, $in_filename ) {
  $user_id = $GLOBALS['user']->uid;

  if ( $user_id == "0" ) {
    return false;
  }

  if ( user_access('administer courses') ) {
    return true;
  }

  $filename_array = explode( '.', $in_filename );
  if ( count($filename_array) != 2 ) {
    return false;
  } // filename of %int%.zip expected

  if ( !is_numeric( $filename_array[0] ) ) {
    return false;
  }

  $work_id = $filename_array[0];

  if ( user_access('access supervisor front-end') ) {
    if (supervisor_verify_course_work_pair( $user_id, $in_course_id, $work_id )) 
     return true;
  }
  if ( user_access('access own courses') ) {
    if (student_verify_work_ownership_by_scaw_ids( $user_id, $in_course_id, $in_assign_id, $work_id ))
     return true;
  }

  return false;
}

/**
 *   Define mime type check function if we dont have builtin one.
 *
 *   
 */ 
 if ( !function_exists("mime_content_type") ) {
  function mime_content_type($f) {
    $mime = '';
    if (substr(PHP_OS, 0, 3) != 'WIN') {
      // TODO: might need to be improved

    }
    else {
      $f = escapeshellarg($f);
      $mime = trim( exec("file -bi " . $f) );
    }
    if ( $mime && !empty($mime )) {
      return $mime;
    }
    return "application/octet-stream";
  }
}

/**
 *   Reset headers
 *
 *   
 */ 
function __gml_course_files_reset_headers() {
  // if you don't have a clue what's going on here,

  // you're better off with RFC2616 for a kick-off.


  // make sure no caching headers are set

  // in this very case the caching is disabled alltogether

  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Pragma: no-cache");
  header("Expires:");
  // unset other headers we (might) set

  header("Content-Range:");
  header("Content-Length:");
  header("Content-Type:");
  header("Last-Modified:");
  header("Content-Disposition:");
}

//////////////////////////////////////////////////////////////////////////////

/// Send a file to the client. Absolute file path required!

///

/// @param   string   the location of the filename

/// @param   bool     forces the file to be send as an attachment (no inline viewing)

/// @param   string   mime type of this file

///

/// @return  bool     true if the download was handled

function __gml_course_files_process_download( $filename,   $as_attachment, $mimeType = '' ) {
  __gml_course_files_reset_headers();

  if (!is_file($filename)) {
    //trigger_error("Requested download file does not exist: <i>".$filename."</i>", E_USER_ERROR);

    return false;
  }
  if (!is_readable($filename)) {
    //trigger_error("Requested file is not readable: <i>".$filename."</i>", E_USER_ERROR);

    return false;
  }

  // not sure if this is required

  ob_end_clean();

  $fi["path"] = $filename;
  $fi["name"] = basename($filename); // name reported to the browser

  $fi["size"] = filesize($filename);
  $fi["time"] = filemtime($filename);
  $fi["ranges"] = array();

  if ( $as_attachment ) {
    $contentDisposition = "attachment";
  } // inline/attachment

  else {
    $contentDisposition = "inline";
  }

  if ( isset($_SERVER["HTTP_RANGE"]) ) {
    // process ranges

    if (preg_match("/^bytes=(.*)$/", trim($_SERVER["HTTP_RANGE"]), $ranges)) {
      $ranges = explode(",", $ranges[1]);

      for ($i = 0; $i < count($ranges); $i++) {
        if (preg_match("/^([0-9]*)-([0-9]*)$/", $ranges[$i], $r)) {
          if ($r[1] == "") { // -X : last X bytes
            $fi["ranges"][] = array(
              "start" => $fi["size"] -intval($r[2]),
              "stop" => $fi["size"] -1,
              "count" => intval($r[2]),
            );
          }
          else if ($r[2] == "") { // X- : skip first X bytes
            $fi["ranges"][] = array(
              "start" => intval($r[1]),
              "stop" => $fi["size"] -1,
              "count" => $fi["size"] -intval($r[1]),
            );
          }
          else if ($r[1] == "" && $r[2] == "") {
            $fi["ranges"] = array();
            break;
          }
          else { // X-Y : X to Y bytes (0 = 1st byte)
            $fi["ranges"][] = array(
              "start" => intval($r[1]),
              "stop" => intval($r[2]),
              "count" => intval($r[2]) -intval($r[1]) + 1,
            );
          }
        }
        else {
          $fi["ranges"] = array();
          break;
        }
      }
    }

    for ( $i = 0; $i < count($fi["ranges"]); $i++ ) {
      if ( ($fi["ranges"][$i]["start"] < 0) || 
			($fi["ranges"][$i]["stop"] >= $fi["size"]) ) {
        $fi["ranges"] = array();
        break;
      }
    }

    if ( count($fi["ranges"]) == 0 ) { // no valid ranges specified
      header("HTTP/1.1 416 Requested range not satisfiable");
      header("Content-Range: bytes */" . $fi["size"]);
      return true;
    }
  }

  /*
   if ( $_SERVER["HTTP_IF_MODIFIED_SINCE"] )
   {
   if ( $fi["time"] > strftime($_SERVER["HTTP_IF_MODIFIED_SINCE"]) )
   {
   header("HTTP/1.1 304 Not Modified");
   return true;
   }
   }
   */

  // figure out mime type

  $fi["mime"] = trim($mimeType);

  if (empty($fi["mime"])) {
    if (function_exists("mime_content_type")) {
      $fi["mime"] = mime_content_type($filename);
    }
  }

  if (!preg_match("#^(\w*)/(\w*)$#i", $fi["mime"])) {
    $fi["mime"] = "application/octet-stream";
  }

  // send headers


  if (count($fi["ranges"]) > 0) {
    header("HTTP/1.1 206 Partial content");

    if (count($fi["ranges"]) == 1) {
      header("Content-Range: bytes " . $fi["ranges"][0]["start"] . "-" . $fi["ranges"][0]["stop"] . "/" . $fi["size"]);
      header("Content-Length: " . $fi["ranges"][0]["count"]);
    }
    else {
      $byteRangeBoundary = "_BYTE_RANGE_" . md5(microtime());
      header("Content-Type: multipart/byteranges; boundary=" . $byteRangeBoundary);
    }
  }
  else {
    header("HTTP/1.1 200 Ok");
    header("Content-Length: " . $fi["size"]);
  }

  header("Accept-Ranges: bytes");

  if ( count($fi["ranges"]) <= 1 ) {
    header("Content-Type: " . $fi["mime"]);
  }

  header("Last-Modified: " . gmdate("D, d M Y H:i:s \G\M\T", $fi["time"]));
  header("Content-Disposition: " . $contentDisposition . "; filename=\"" . $fi["name"] . "\"");

  // encode text files when accepted

  if (USE_GZIP && preg_match("#^text/#i", $fi["mime"]) && extension_loaded("zlib") && 
       in_array("gzip", explode(",", $_SERVER["HTTP_ACCEPT_ENCODING"]))) {
    ob_start("ob_gzhandler");
    ob_implicit_flush(false); // content length header needs to be updated

  }

  if ($_SERVER["REQUEST_METHOD"] == "HEAD") {
    return true;
  } // no body for you


  $fp = fopen($filename, "rb");
  if (!$fp) {
    header("HTTP/1.1 500 Internal Server Error");
    echo "Unable to open file for reading.";
    return true;
  }

  if (count($fi["ranges"]) == 0) {
    while (!connection_status() && !feof($fp)) {
      echo fread($fp, READBUFFER_SIZE);
    }
  }
  else if (count($fi["ranges"]) == 1) {
    fseek($fp, $fi["ranges"][0]["start"], SEEK_SET);

    while (!connection_status() && (ftell($fp) <= $fi["ranges"][0]["stop"] -READBUFFER_SIZE)) {
      echo fread($fp, READBUFFER_SIZE);
    }
    if ( $fi["ranges"][0]["stop"] - ftell($fp) > 1 ) {
      echo fread( $fp, $fi["ranges"][0]["stop"] -ftell($fp) + 1 );
    }
  }
  else {
    for ($i = 0; $i < count($fi["ranges"]); $i++) {
      echo "\r\n";
      echo "--" . $byteRangeBoundary . "\r\n";
      echo "Content-Type: " . $fi["mime"] . "\r\n";
      echo "Content-Range: bytes " . $fi["ranges"][$i]["start"] . "-" . $fi["ranges"][$i]["stop"] . "/" . $fi["size"] . "\r\n";
      echo "Content-Disposition: " . $contentDisposition . "; filename=" . $fi["name"] . "\r\n";
      echo "\r\n";

      fseek($fp, $fi["ranges"][$i]["start"], SEEK_SET);
      while (!connection_status() && (ftell($fp) <= $fi["ranges"][$i]["stop"] -READBUFFER_SIZE)) {
        echo fread($fp, READBUFFER_SIZE);
      }
      if ($fi["ranges"][$i]["stop"] -ftell($fp) > 1) {
        echo fread($fp, $fi["ranges"][$i]["stop"] -ftell($fp) + 1);
      }
    }
    echo "\r\n";
    echo "--" . $byteRangeBoundary . "--";
  }

  fclose($fp);
  return true;
}



//////////////////////////////////////////////////////////////////////////////

/// Send a file to the client using drupal builtin routine.

/// Absolute file path required!

///

/// @param   string   the location of the filename

/// @param   bool     force attachment mode

/// @param   string   mime type of this file

///

/// @return  bool     true if the download was handled

function __gml_course_files_process_download_basic( $filename, $as_attachment, $flag_needgoodname, $mimeType = '' ) {
  __gml_course_files_reset_headers();

  if (!is_file($filename)) {
    //trigger_error("Requested download file does not exist: <i>".$filename."</i>", E_USER_ERROR);

    return false;
  }
  if (!is_readable($filename)) {
    //trigger_error("Requested file is not readable: <i>".$filename."</i>", E_USER_ERROR);

    return false;
  }

  $fi = array();

  $fi["path"] = $filename;
  $fi["name"] = basename($filename); // name reported to the browser

  $fi["size"] = filesize($filename);
  $fi["time"] = filemtime($filename);
  $fi["ranges"] = array();

  if ( $as_attachment ) {
    $contentDisposition = "attachment";
  } // inline/attachment

  else {
    $contentDisposition = "inline";
  }

  /*
   if ( $_SERVER["HTTP_IF_MODIFIED_SINCE"] )
   {
   if ($fi["time"] > strftime($_SERVER["HTTP_IF_MODIFIED_SINCE"]))
   {
   header("HTTP/1.1 304 Not Modified");
   return true;
   }
   }
   */

  $header_filename = '';
  if ($flag_needgoodname) {
    $header_filename = _file_generate_good_name($fi['name']);
  }
  else {
    $header_filename = $fi['name'];
  }

  // figure out mime type

  $fi["mime"] = trim($mimeType);

  if (empty($fi["mime"])) {
    if (function_exists("mime_content_type")) {
      $fi["mime"] = mime_content_type($filename);
    }
  }

  if (!preg_match("#^(\w*)/(\w*)$#i", $fi["mime"])) {
    $fi["mime"] = "application/octet-stream";
  }

  // send headers


  header("HTTP/1.1 200 Ok");
  header("Content-Length: " . $fi["size"]);
  header("Accept-Ranges: none");
  header("Content-Type: " . $fi["mime"]);
  header("Last-Modified: " . gmdate("D, d M Y H:i:s \G\M\T", $fi["time"]));
  header("Content-Disposition: " . $contentDisposition . "; filename=\"" . $header_filename . "\"");

  if ($_SERVER["REQUEST_METHOD"] == "HEAD") {
    return true;
  } // no body for you

//actual data transfer
  file_transfer( $filename, array() );


  // the routine should normally exit by now

  return true;
}


 /**
 *   Returns huma redable name with additional info (assign number, course name etc)
 *
 *   @return - text
 */ 
function _file_generate_good_name( $in_filename ) {
  $clear_name = substr($in_filename, 0, strpos($in_filename, '.'));

  $alias = _gml_create_desc($clear_name);

  if ($alias != NULL) {
    $result = $alias . '.zip';
  }
  else {
    $result = 'file_' . $clear_name . '.zip';
  }

  return $result;
}


 /**
 *   Defines translitiration for ru/en convertion. You can define your own transliteration routine.
 *	 TRANSLIT
 *   @return - transliterated text
 */ 
function __gml_translit($str) {


  $rus = array(
    "/а/",
    "/б/",
    "/в/",
    "/г/",
    "/ґ/",
    "/д/",
    "/е/",
    "/ё/",
    "/ж/",
    "/з/",
    "/и/",
    "/й/",
    "/к/",
    "/л/",
    "/м/",
    "/н/",
    "/о/",
    "/п/",
    "/р/",
    "/с/",
    "/т/",
    "/у/",
    "/ф/",
    "/х/",
    "/ц/",
    "/ч/",
    "/ш/",
    "/щ/",
    "/ы/",
    "/э/",
    "/ю/",
    "/я/",
    "/ь/",
    "/ъ/",
    "/і/",
    "/ї/",
    "/є/",
    "/А/",
    "/Б/",
    "/В/",
    "/Г/",
    "/ґ/",
    "/Д/",
    "/Е/",
    "/Ё/",
    "/Ж/",
    "/З/",
    "/И/",
    "/Й/",
    "/К/",
    "/Л/",
    "/М/",
    "/Н/",
    "/О/",
    "/П/",
    "/Р/",
    "/С/",
    "/Т/",
    "/У/",
    "/Ф/",
    "/Х/",
    "/Ц/",
    "/Ч/",
    "/Ш/",
    "/Щ/",
    "/Ы/",
    "/Э/",
    "/Ю/",
    "/Я/",
    "/Ь/",
    "/Ъ/",
    "/І/",
    "/Ї/",
    "/Є/",
  );
  $lat = array(
    "a",
    "b",
    "v",
    "g",
    "g",
    "d",
    "e",
    "e",
    "zh",
    "z",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "r",
    "s",
    "t",
    "u",
    "f",
    "h",
    "c",
    "ch",
    "sh",
    "sh'",
    "y",
    "e",
    "yu",
    "ya",
    "'",
    "'",
    "i",
    "i",
    "e",
    "A",
    "B",
    "V",
    "G",
    "G",
    "D",
    "E",
    "E",
    "ZH",
    "Z",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "R",
    "S",
    "T",
    "U",
    "F",
    "H",
    "C",
    "CH",
    "SH",
    "SH'",
    "Y",
    "E",
    "YU",
    "YA",
    "'",
    "'",
    "I",
    "I",
    "E",
  );

  $output = preg_replace($rus, $lat, $str);
  $output = preg_replace('/ +/', '_', $output);

  return $output;
}

 /**
 *   Generates good name for work file
 *
 *   @return - text
 */ 
 function _gml_create_desc($in_work_id) {

 
  $res = db_query("SELECT gmlcourse_works.student_id, gmlcourse_works.assign_id  FROM {gmlcourse_works} WHERE gmlcourse_works.id=%d", $in_work_id);
  $work_data = db_fetch_array($res);
  if ($work_data == NULL) {
    return NULL;
  }


  $res = db_query("SELECT gmlcourse_assigns.title, gmlcourse_assigns.short_title, gmlcourse_assigns.number, gmlcourse_assigns.course_id, gmlcourse_assigns.flags , gmlcourse_assigns.show_results FROM {gmlcourse_assigns} WHERE gmlcourse_assigns.id=%d", $work_data['assign_id']);
  $assign_data = db_fetch_array($res);
  if ($assign_data == NULL) {
    return NULL;
  }
  if ($assign_data['short_title'] == NULL) {
    $assign_title = mb_substr($assign_data['title'], 0, 20, 'UTF-8');
  }
  else {
    $assign_title = $assign_data['short_title'];
  }


  $res = db_query("SELECT gmlcourse_courses.title, gmlcourse_courses.short_title FROM {gmlcourse_courses} WHERE gmlcourse_courses.id=%d", $assign_data['course_id']);
  $course_data = db_fetch_array($res);
  if ($course_data == NULL) {
    return NULL;
  }
  if ($course_data['short_title'] == NULL) {
    $course_title = mb_substr($course_data['title'], 0, 10, 'UTF-8');
  }
  else {
    $course_title = $course_data['short_title'];
  }


  $res = db_query("SELECT gmlcourse_userinfo.last_name FROM {gmlcourse_userinfo} WHERE gmlcourse_userinfo.student_id=%d", $work_data['student_id']);
  $student_data = db_fetch_array($res);
  if ($student_data == NULL) {
    return NULL;
  }

  $works = work_fetch_works($assign_data['course_id'], $assign_data['number'], $work_data['student_id']);
  

  $idx = 0;
  while ($in_work_id !== $works[$idx]['id']) {
    $idx++;
  }

  $idx++;

  $student_name = 'student_' . substr(md5($student_data['last_name'] . $in_work_id), 0, 10);

//if anonym works turned on we will uses "student" for student name

// __gml_translit - is a simple transliteration function  - you can define you own, if you need it
  if ( intval( $assign_data['flags'] ) & ASSIGN_FLAG_SCRAMBLESTUDENTINFO ) {
    if ( $assign_data['show_results'] ) {
      $student_name =  __gml_translit(check_plain($student_data['last_name']));
    }
  }
  else {
    $student_name = __gml_translit(check_plain($student_data['last_name']));
  }

  if (user_access('administer courses')) {
    $student_name =  __gml_translit(check_plain($student_data['last_name']));
  }

  $file_alias = __gml_translit(check_plain($course_title)) . '_' . $assign_data['number'] . '_' . __gml_translit(check_plain($assign_title)) . '_' . $student_name . '_' . $idx;

  return $file_alias;

}

?>