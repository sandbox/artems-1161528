<?php

/**
 * @file
 * GML Course system
 * 
 *  Registration subsystem
 *
 */


define("CONST_GML_REGISTRATION_ENABLED", FALSE);
define("CONST_GML_REGISTRATION_HELP_URL", "");
define("CONST_GML_REGISTRATION_REDIRECT_URL", "");


/**
 *   Display registration page
 *
 *   
 */ 
 function courses_registration_display() {
  $output = common_html_small_font_class( true );
  $output .= drupal_get_form('courses_registration_display_form');
  $output .= common_html_small_font_class( false );

  return $output;
}

/**
 *   Display registration page
 *   Form
 *   
 */ 
 function courses_registration_display_form(&$form_state) {
  
  $target_courses = array('0' => t('Select course'));

  $student_user = array();

  //try to load current user info
  if ($GLOBALS['user']->uid) {
    $student_user = student_fetch_info($GLOBALS['user']->uid);
  }
  
  //if load current user failed - prepare input boxes
  if (empty($student_user)) {

    $courses = course_fetch_courses();

	//new user can register for all courses wtih open registration
    foreach ($courses as $course) {
      if ($course['active'] && $course['reg_open']) {
        $target_courses[$course['id']] = $course['title'];
      }
    }

    $form['Fam'] = array(
      '#type' => 'textfield',
      '#title' => t('Last Name'),
      '#size' => 60,
      '#maxlength' => 250,
      '#description' => t('Enter Last Name'),
      '#required' => TRUE,
    );

    $form['Nam'] = array(
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#size' => 60,
      '#maxlength' => 250,
      '#description' => t('Enter First Name'),
      '#required' => TRUE,
    );

    $form['SurNam'] = array(
      '#type' => 'textfield',
      '#title' => t('Middle name'),
      '#size' => 60,
      '#maxlength' => 250,
      '#description' => t('Enter Middle name'),
      '#required' => TRUE,
    );

    $form['uid'] = array(
      '#type' => 'textfield',
      '#title' => t('Student number'),
      '#size' => 60,
      '#maxlength' => 250,
      '#description' => t('Enter numbers without spaces'),
      '#required' => TRUE,
    );

    $form['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('E-mail'),
      '#size' => 60,
      '#maxlength' => 250,
      '#description' => t('Enter valid e-mail'),
      '#required' => TRUE,
    );

  }
  else {
     //load current user variables to input boxes and make it disabled
	 
    $student_courses = student_get_courses_to_register($GLOBALS['user']->uid);
    //existing user allowed to register for all courses with open registration except already registered courses
    foreach ($student_courses as $course) {
      if ($course['active'] && $course['reg_open']) {
        $target_courses[$course['id']] = $course['title'];
      }
    }

    $form['Fam'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter Last Name'),
      '#size' => 60,
      '#maxlength' => 250,
      '#default_value' => $student_user['last_name'],
      '#description' => t('Enter Last Name'),
      '#disabled' => TRUE,
    );

    $form['Nam'] = array(
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#size' => 60,
      '#maxlength' => 250,
      '#default_value' => $student_user['first_name'],
      '#description' => t('Enter First Name'),
      '#disabled' => TRUE,
    );

    $form['SurNam'] = array(
      '#type' => 'textfield',
      '#title' => t('Middle name'),
      '#size' => 60,
      '#maxlength' => 250,
      '#default_value' => $student_user['middle_name'],
      '#description' => t('Enter Middle name'),
      '#disabled' => TRUE,
    );

    $form['uid'] = array(
      '#type' => 'textfield',
      '#title' => t('Student number'),
      '#size' => 60,
      '#maxlength' => 250,
      '#default_value' => $student_user['login'],
      '#description' => t('Enter numbers without spaces'),
      '#disabled' => TRUE,
    );
  }
  //target course selector  - we can select between courses with allow registration option
  $form['course'] = array(
    '#type' => 'select',
    '#title' => t('Course'),
    '#options' => $target_courses,
    '#default_value' => 0,
    '#maxlength' => 250,
    '#description' => t('Select target course'),
    '#required' => TRUE,
    '#ahah' => array(
      'path' => 'regcourse/autocomplete',
      'wrapper' => 'regcourse-group-select',
      'method' => 'replace',
      'event' => 'change',
      'effect' => 'fade',
    ),
  );
  //group selector will be enabled after course selection
  $form['group'] = array(
    '#type' => 'select',
    '#title' => t('Group'),
    '#options' => array(0 => t('Select course first')),
    '#default_value' => 0,
    '#disabled' => TRUE,
    '#required' => TRUE,
    '#description' => t('Select group'),
    '#prefix' => '<div id="regcourse-group-select">',
    '#suffix' => '</div>',
  );

  //show some help
  $help_url = variable_get('gmlcourse_registration_help_url', CONST_GML_REGISTRATION_HELP_URL);
  if ($help_url <> "") {
    $form['reg_help'] = array(
      '#value' => t('If you have questions - see ') . l('registration help', $help_url) . '.',
    );
  }

  //secret keyword to prevent spam registration
  $form['keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('Keyword'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Enter course keyword'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Register'),
  );


  return $form;
}

/**
 *   Display registration page
 *   Form validate
 *   
 */ 
function courses_registration_display_form_validate($form, &$form_state) {
  
  if ($GLOBALS['user']->uid) {
    $test_user = student_fetch_info($GLOBALS['user']->uid);
  }

  //validate user and mail
  if (empty($test_user)) {

    if (!valid_email_address(check_plain($form_state['values']['mail']))) {
      form_set_error('mail', t('Enter valid email!'));
    }
  }

  if ($form_state['values']['course'] == 0) {
    form_set_error('course', t('Select a course!'));
  }

  if ($form_state['values']['group'] == 0) {
    form_set_error('group', t('Select a group!'));
  }

  $test_course = course_fetch_course(check_plain($form_state['values']['course']));
  $test_group = group_fetch_group(check_plain($form_state['values']['group']));

  //check course-group relations
  if (empty($test_course)) {
    common_log_event( false, false, 'CourseID mismatch!. User = %user Last Name = %lastname CourseID=%courseid GroupID=%groupid ', array('%user' => check_plain($form_state['values']['uid']), '%lastname' => check_plain($form_state['values']['Fam']), '%courseid' => check_plain($form_state['values']['course']), '%groupid' => check_plain($form_state['values']['group']))  );
    form_set_error('course', t('Nice try! You attempt has been logged1!'));
  }

  if (empty($test_group)) {
    common_log_event( false, false, 'CourseID mismatch!. User = %user Last Name = %lastname CourseID=%courseid GroupID=%groupid ', array('%user' => check_plain($form_state['values']['uid']), '%lastname' => check_plain($form_state['values']['Fam']), '%courseid' => check_plain($form_state['values']['course']), '%groupid' => check_plain($form_state['values']['group']))  );
    form_set_error('group', t('Nice try! You attempt has been logged2!'));
  }

  if (!($test_course['active'] && $test_course['reg_open'])) {
    common_log_event( false, false, 'CourseID mismatch!. User = %user Last Name = %lastname CourseID=%courseid GroupID=%groupid ', array('%user' => check_plain($form_state['values']['uid']), '%lastname' => check_plain($form_state['values']['Fam']), '%courseid' => check_plain($form_state['values']['course']), '%groupid' => check_plain($form_state['values']['group']))  );
    form_set_error('course', t('Nice try! You attempt has been logged3!'));
  }

  if (!($test_group['course_id'] == $test_course['id'])) {
    common_log_event( false, false, 'CourseID mismatch!. User = %user Last Name = %lastname CourseID=%courseid GroupID=%groupid ', array('%user' => check_plain($form_state['values']['uid']), '%lastname' => check_plain($form_state['values']['Fam']), '%courseid' => check_plain($form_state['values']['course']), '%groupid' => check_plain($form_state['values']['group']))  );
    form_set_error('group', t('Nice try! You attempt has been logged4!'));
  }


}

/**
 *   Display registration page
 *   Form submit
 *   
 */ 
function courses_registration_display_form_submit($form, &$form_state) {


  if ($GLOBALS['user']->uid) {
    $student_user = student_fetch_info($GLOBALS['user']->uid);
  }

  //check input variables
  $group = check_plain($form_state['values']['group']);

  $keyword = check_plain($form_state['values']['keyword']);

  $course_id = check_plain($form_state['values']['course']);

  $result = FALSE;

  //register new drupal user
  if (empty($student_user)) {


    $id = check_plain($form_state['values']['uid']);

    $last_name = check_plain($form_state['values']['Fam']);

    $first_name = check_plain($form_state['values']['Nam']);

    $middle_name = check_plain($form_state['values']['SurNam']);

    $email = check_plain($form_state['values']['mail']);


    $result = register_new_student_user($group, $id, $email, $last_name, $first_name, $middle_name, TRUE, $keyword, $course_id);

    if ($result) {
      drupal_set_message(t('This were errors in registration. Registration is not completed.'));
    }
    else {
      drupal_goto(check_plain(variable_get('gmlcourse_registration_redirect_url', CONST_GML_REGISTRATION_REDIRECT_URL)));
    }
  }
  //or add course relation to current user
  else {
    $result = register_add_course_student_relation($GLOBALS['user']->uid, $group, $course_id, $keyword);

    if ($result) {
      drupal_set_message(t('This were errors in registration. Registration is not completed.'));
    }
    else {
      drupal_set_message(t('Registration completed. Now you can upload works for selected course.'));
    }

  }


}

/**
 *   AHAH group loader for course
 *   Load groups to selected course and make groups selector
 *   
 */
function regcourse_autocomplete() {


  $form_state = array('submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];


  $form = form_get_cache($form_build_id, $form_state);

  $course_groups = group_fetch_groups($_POST['course']);

  $groups = array(0 => t('Select group'));

  foreach ($course_groups as $group) {
    $groups[$group['id']] = $group['title'];
  }


  $form['group']['#disabled']      = false;
  $form['group']['#default_value'] = 0;
  $form['group']['#options']       = $groups;


  form_set_cache($form_build_id, $form, $form_state);

  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
  );

  $form = form_builder('courses_registration_display_form', $form, $form_state);

  $output = drupal_render($form['group']);
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 *   Add student-course relation
 *   
 *   
 */
function register_add_course_student_relation($id, $group, $in_course_id, $keyword) {
  // find profile fields that we use

  $profile_lastname = variable_get('gmlcourse_student_lastname', CONST_PROFILE_LASTNAME_DEFAULT);
  $profile_middlename = variable_get('gmlcourse_student_middlename', CONST_PROFILE_MIDDLENAME_DEFAULT);
  $profile_firstname = variable_get('gmlcourse_student_firstname', CONST_PROFILE_FIRSTNAME_DEFAULT);
  $student_role_rid = variable_get('gmlcourse_student_role', 3);

  $course = course_fetch_course($in_course_id);

  //check keyword here
  if (!($course['reg_keyword'] == $keyword)) {
    common_log_event( false, false, 'Invalid course keyword. Skipping. User = %user GroupID = %groupid.', array('%user' => $id, '%groupid' => $group) );
    drupal_set_message( t('Error : Invalid course keyword!'), 'error' );
    return TRUE;
  }


  //check if the user exists already

  if ( $id ) {
    $user = user_load( array('uid' => $id) );

    // if user already exists we should just modify his course/group information

    if ( $user ) {
      $user_id = $user->uid;

      if ( student_verify_course_access_by_sc_ids( $user_id, $in_course_id ) ) {
        drupal_set_message( t('Error : You already registered to this course, skipping: '), 'error' );
        common_log_event( false, false, 'Try to register - already registered. Skipping. User = %user GroupID = %groupid.', array('%user' => $id, '%groupid' => $group) );
        return TRUE;
      }
      else {
        db_query("INSERT INTO {gmlcourse_students2groups}
						  ( group_id, student_id )
						  VALUES ( %d, %d )", 
						  $group, $user_id );

        drupal_set_message( t('Course relation was added.'), 'status' );
        common_log_event( false, false, 'Course relation was added. User = %user GroupID = %groupid.', array('%user' => $id, '%groupid' => $group), NULL, 'notice' );
        return FALSE;
      }
    }
  }
  //if something goes wrong
  drupal_set_message( t('Internal error.'), 'error' );
  common_log_event( false, false, 'Error : internal error.', array() );
  return TRUE;
}


/**
 *   Register new drupal user and add it to a course
 *   
 *   
 */
function register_new_student_user($group, $id, $email, $last_name, $first_name, $middle_name, $in_mail_verbose, $keyword, $in_course_id) {
  // find profile fields that we use

  $profile_lastname = variable_get('gmlcourse_student_lastname', CONST_PROFILE_LASTNAME_DEFAULT);
  $profile_middlename = variable_get('gmlcourse_student_middlename', CONST_PROFILE_MIDDLENAME_DEFAULT);
  $profile_firstname = variable_get('gmlcourse_student_firstname', CONST_PROFILE_FIRSTNAME_DEFAULT);
  $student_role_rid = variable_get('gmlcourse_student_role', 3);

  $course = course_fetch_course($in_course_id);

  //check keyword
  if (!($course['reg_keyword'] == $keyword)) {
    common_log_event( false, false, 'Invalid course keyword. Skipping. User = %user GroupID = %groupid.', array('%user' => $id, '%groupid' => $group) );
    drupal_set_message( t('Error : Invalid course keyword!'), 'error' );
    return TRUE;
  }

  //check mail aready in use
  $user = user_load( array('mail' => $email) );
  if ($user) {
    drupal_set_message( t('E-mail you entered already in use! '), 'error' );
    common_log_event( false, false, 'Register: Email already in use: User = %user Mail = %mail', array('%user' => $id, '%mail' => $email) );
    return TRUE;
  }

  //check login already in use
  $user = user_load( array('name' => $id) );
  if ($user) {
    drupal_set_message( t('Login you entered already in use! '), 'error' );
    common_log_event( false, false, 'Register: Login already in use: User = %user Mail = %mail', array('%user' => $id, '%mail' => $email) );
    return TRUE;
  }


  // note: no middle name is ok		
  if ( !$id || !$email || !$last_name || !$first_name) {
    drupal_set_message(t('Invalid input, skipping.'), 'error');

    return TRUE;
  }

  $default_suspicious_level = 1;
  //random password
  $pass = md5( rand( 1000000, 1000000000 ) ) . md5( $id ) . time();
  
  //new user array
  $array = array(
    'pass' => $pass,
    'name' => $id,
    $profile_lastname => $last_name,
    $profile_firstname => $first_name,
    $profile_middlename => $middle_name,
    'mail' => $email,
    'status' => 1,
    'roles' => array($student_role_rid => 'student'),
    'gml_course_groups' => array($in_course_id => $group), 
    'gml_course_suspicious_level' => $default_suspicious_level,
  );

  $account = user_save( $newuser, $array );
  $site_name = variable_get('site_name', 'GML');

  //send mail if new user created
  if ( $account ) {
    $variables = array(
      '!username' => $id,
      '!site' => $site_name,
      '!password' => $pass,
      '!uri' => $base_url,
      '!uri_brief' => substr($base_url, strlen('http://')),
      '!mailto' => $email,
      '!date' => format_date(time()),
      '!login_uri' => url('user', array('absolute' => TRUE)),
      '!edit_uri' => url('user/' . $account->uid . '/edit', array('absolute' => TRUE)),
      '!login_url' => user_pass_reset_url($account),
    );


    $flag_mail_sent = $language = user_preferred_language($account);

    $mail_subject = _user_mail_text('register_admin_created_subject',   $language, $variables);
    $mail_body = _user_mail_text('register_admin_created_body',   $language, $variables);
    $mail_header = "From: $from\nReply-to: $from\nX-Mailer: Drupal\nReturn-path: $from\nErrors-to: $from";

    $flag_mail_sent = $params = array(
      'subject' => $mail_subject,
      'body' => $mail_body,
    );

    $flag_mail_sent = drupal_mail('gml_course_student', 'send',   $email, $language, $params);

    if ( $flag_mail_sent ) {
      if ( $in_mail_verbose ) {
        drupal_set_message(t('Further registration instructions for the new user %user have been e-mailed to "%email".', array('%user' => $id, '%email' => $email)), 'status');
      }
      common_log_event( false, false, 'Further registration instructions for the new user %user have been e-mailed to "%email".', array('%user' => $id, '%email' => $email), NULL, 'notice' );
      return FALSE;
    }
    else {
      drupal_set_message(t('Error : Failed to send an email to %mail', array('%email' => $email)), 'error');
      common_log_event( false, false, 'Error : Failed to send an email to %mail', array('%mail' => $email) );

    }
  }
  else {
    drupal_set_message(t('Error : unknown error.'), 'error');
    common_log_event( false, false, 'Error : internal error.', array() );
    return TRUE;
  }
}


?>