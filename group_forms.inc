<?php

/**
 * @file
 * GML Course system
 * 
 * Student groups administration interface
 *
 */

/**
 *   Display groups list page
 *
 *   
 */ 
function groups_admin_display($in_course_id) {
  $output = common_html_small_font_class( true );
  $output .= drupal_get_form('groups_admin_display_form', $in_course_id);
  $output .= common_html_small_font_class( false );

  return $output;

}

/**
 *   Display groups list page
 *   Form
 *   
 */ 
 function groups_admin_display_form(&$form_state, $in_course_id) {
  $groups = array();
  $groups = group_fetch_groups($in_course_id);

  $form = array();

  foreach ($groups as $i => $group) {
    $form[$i]['id'] = array(
      '#value' => $group['id'],
    );

    $form[$i]['weight'] = array(
      '#value' => $group['weight'],
    );

    $form[$i]['title'] = array(
      '#value' => $group['title'],
    );

    $form[$i]['edit'] = array(
      '#value' => l(t('edit'), 'course/' . $in_course_id . '/group/' . $group['id'] .  '/edit'),
    );

    $form[$i]['delete'] = array(
      '#value' => l(t('delete'), 'course/' . $in_course_id . '/group/' . $group['id'] .  '/delete'),
    );
  }

  return $form;

}

/**
 *   Display groups list page
 *   Form  - theme function
 *   display as table
 */ 
 function theme_groups_admin_display_form($form) {

  foreach ($form as $name => $element) {
    if (isset($element['title']) && is_array($element['title'])) {
      $rows[] = array(
        drupal_render($element['weight']),
        drupal_render($element['title']),
        drupal_render($element['edit']),
        drupal_render($element['delete']),
      );
      unset($form[$name]);


    }
  }

  $header = array(t('Weight'), t('Title'), array(
      'data' => t('Operations'),
      'colspan' => 2,
    ));
  $output = theme('table', $header, $rows,   array('id' => 'groups'));
  $output .= drupal_render($form);

  return $output;
}

/**
 *   Display groups edit page
 *   
 *   
 */ 
function group_add_edit($in_course_id = NULL, $in_group_id = NULL) {
  $output = drupal_get_form('group_add_edit_form', $in_course_id, $in_group_id);

  return $output;
}

/**
 *   Display groups edit page
 *   Form
 *   
 */ 
function group_add_edit_form(&$form_state, $in_course_id = NULL, $in_group_id = NULL) {

  //allow only for administrator
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  if ($in_course_id) {
    $form['course_id'] = array(
      '#type' => 'value',
      '#value' => $in_course_id,
    );
  }

  $flag_new = true;

  $edit = array();

  if ($in_group_id) {
    $edit = group_fetch_group($in_group_id);
    if ( $edit ) {
      $form['group_id'] = array(
        '#type' => 'value',
        '#value' => $edit['id'],
      );
      $flag_new = false;
    }
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Group title'),
    '#default_value' => $edit['title'],
    '#required' => TRUE,
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $edit['weight'],
    '#description' => t('Group weight'),
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t( $flag_new ? 'Create new group' : 'Submit' ),
    '#weight' => 30,
  );


  return $form;
}

/**
 *   Display groups edit page
 *   Form submit
 *   
 */ 
 function group_add_edit_form_submit($form, &$form_state) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }
  //add or update existing group
  if ($form_state['values']['group_id']) {
    group_update_group(
      $form_state['values']['group_id'], 
      $form_state['values']['title'], $form_state['values']['weight']);

    drupal_set_message(t('group "%title" has been updated', 
      array('%title' => $form_state['values']['title'])));
  }
  else {
    group_add_group(
      $form_state['values']['title'], $form_state['values']['weight'], 
      $form_state['values']['course_id']);

    drupal_set_message(t('group "%title" has been added', 
      array('%title' => $form_state['values']['title'])));
  }

  drupal_goto('course/' . $form_state['values']['course_id'] . '/group');

}

/**
 *   Display group delete page
 *   
 *   
 */ 
 function group_delete($in_course_id = NULL, $in_group_id = NULL) {

  return drupal_get_form('group_delete_page', $in_course_id, $in_group_id);
}

/**
 *   Display group delete page
 *   Form 
 *   
 */ 
function group_delete_page(&$form_state, $in_course_id = NULL, $in_group_id = NULL) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  // get group course

  $group = group_fetch_group($in_group_id);

  if ( !$group ) {
    drupal_goto('course/' . $in_course_id . '/group');
    return;
  }

  $form['group_id'] = array(
    '#type' => 'value',
    '#value' => $group['id'],
  );
  $form['course_id'] = array(
    '#type' => 'value',
    '#value' => $in_course_id,
  );

  return confirm_form(
    $form, 
    t('Are you sure you want to delete the group %name?', array('%name' => $group['title'])), 
    'course/' . $in_course_id . '/group', 
    '', 
    t('Delete'), 
    t('Cancel'));
}

/**
 *   Display group delete page
 *   Form submit
 *   
 */ 
 function group_delete_page_submit($form, &$form_state) {
  if ( !user_access('administer courses') ) {
    return form_return_noaccess();
  }

  // remove group

  group_delete_from_db($form_state['values']['group_id']);

  drupal_goto('course/' . $form_state['values']['course_id'] . '/group');

}

?>