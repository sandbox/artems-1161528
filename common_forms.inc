<?php

/**
 * @file
 * GML Course system
 * 
 * Common user interaction functions
 * 
 *
 */

/**
 *   Clears menu cache then redirect to referer
 *   
 */
function common_form_clear_menu_cache() {
  common_clear_menu_cache();
  drupal_goto( referer_uri() );
}

?>