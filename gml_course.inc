<?php

/**
 * @file
 * GML Course system
 * 
 * Db layer function for course components subsystem
 *
 */

 /**
 *   Insert given course in db
 *
 *   
 */  
function course_insert_to_db($in_course_title, $in_course_short_title, $in_course_active, $in_course_weight, $in_course_reg_open, $in_course_reg_keyword) {
  db_query("INSERT INTO {gmlcourse_courses} (title, short_title, active, weight, reg_open, reg_keyword) VALUES ('%s', '%s', %d, %d, %d, '%s')",  
    $in_course_title,  
	$in_course_short_title, 
    $in_course_active,  
	$in_course_weight,  
	$in_course_reg_open,  
	$in_course_reg_keyword);
}


 /**
 *   Returns array of current courses
 *
 *   @return array of courses   
 */  
function course_fetch_courses() {
  $result = db_query("SELECT *
					  FROM {gmlcourse_courses}
					  ORDER BY {gmlcourse_courses}.active DESC,
							   {gmlcourse_courses}.weight DESC,
							   {gmlcourse_courses}.id ASC");
  $courses = array();
  while ($course = db_fetch_array($result)) {
    $courses[] = $course;
  }

  return $courses;
}

/**
 *   Returns course info array for given course
 *
 *   @return course info array
 */  
function course_fetch_course($in_course_id) {
  $result = db_query("SELECT * FROM {gmlcourse_courses} WHERE id=%d", $in_course_id);

  return db_fetch_array($result);

}

/**
 *   Updates course info in db
 *
 *   
 */  
 function course_update_course($in_course_id, $in_title, $in_short_title, $in_active, $in_weight, $in_reg_open, $in_reg_keyword) {

  db_query("UPDATE {gmlcourse_courses} SET title='%s', short_title='%s', active=%d, weight=%d, reg_open=%d, reg_keyword='%s' WHERE id=%d",  
          $in_title,  
		  $in_short_title,  
		  $in_active,  
		  $in_weight,  
		  $in_reg_open, 
		  $in_reg_keyword, 
		  $in_course_id);

}

/**
 *   Deletes course info from db
 *   Also deletes all corresponding:
 *   assigns, groups, supervisor groups
 */  
 function course_delete_from_db($in_course_id) {
  db_query("START TRANSACTION");

  db_query("DELETE FROM {gmlcourse_courses} WHERE id=%d", $in_course_id);

  assign_delete_all_from_db($in_course_id, TRUE);
  group_delete_all_from_db($in_course_id, TRUE);
  supgroup_delete_course_from_db($in_course_id, TRUE);

  db_query("COMMIT");
}


?>