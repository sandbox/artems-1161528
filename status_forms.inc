<?php

/**
 * @file
 * GML Course system
 * 
 *  Statuses and assing types administration interface
 *
 */

/**
 *   Display statuses and types list page
 *
 *   
 */ 
function statuses_admin() {
  $output = common_html_bordered_intable_links_class(true);
  
  //statuses for submission and work
  foreach (array('submission', 'work') as $formname) {

    $statuses = status_get_statuses($formname);
    $rows_status = array();
    foreach ($statuses as $i => $status) {
      $rows_status[] = array(
        $status['abbr'],
        $status['title'],
        $status['comment'],
        $status['weight'],
        l(t('edit'), 'course/status/' . $formname . '/' . $status['id'] . '/edit'),
        l(t('delete'), 'course/status/' . $formname . '/' . $status['id'] . '/delete'),
      );
    }

    $header_status = array(t('Abbr'), t('Title'), t('Comment'), t('Weight'), array(
        'data' => t('Operations'),
        'colspan' => 4,
      ));
    $table_status = theme('table', $header_status, $rows_status);

    $output .= theme('fieldset', array('#collapsible' => TRUE, '#title' => t( $formname . ' status'), '#children' => $table_status));

  }

  //assing types
  $assign_types = status_get_assign_types();
  $rows_assign_types = array();
  foreach ($assign_types as $i => $assign_type) {
    $rows_assign_types[] = array(
      $assign_type['title'],
      $assign_type['comment'],
      common_get_colored_yesno($assign_type['sum']),
      common_get_colored_yesno($assign_type['last']),
      l(t('edit'), 'course/status/assign/' . $assign_type['id'] . '/edit'),
      l(t('delete'), 'course/status/assign/' . $assign_type['id'] . '/delete'),
    );
  }

  $header_assign_types = array(t('Title'), t('Comment'), t('Sum'), t('Last'), array(
      'data' => t('Operations'),
      'colspan' => 5,
    ));
  $table_assign_types = theme('table', $header_assign_types, $rows_assign_types);

  $output .= theme('fieldset', array('#collapsible' => TRUE, '#title' => t('Assign types'), '#children' => $table_assign_types));


  $output .= common_html_bordered_intable_links_class(false);

  return $output;

}

/**
 *   Display statuses add/edit page
 *
 *   
 */ 
function status_add_edit($in_which_status, $in_status_id = NULL) {
  $output = drupal_get_form('status_add_edit_form', $in_which_status, $in_status_id);

  return $output;
}


/**
 *   Display statuses add/edit page
 *   Form
 *   
 */ 
 function status_add_edit_form(&$form_state, $in_which_status, $in_status_id = NULL) {
  
  //work or submission
  if ($in_status_id) {
    $edit = status_get_status($in_which_status, $in_status_id);
    $form['status_edit_id'] = array(
      '#type' => 'value',
      '#value' => $in_status_id,
    );
  }

  $form['which_status'] = array(
    '#type' => 'value',
    '#value' => $in_which_status,
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Status title'),
    '#default_value' => $edit['title'],
    '#required' => TRUE,
  );

  $form['abbr'] = array(
    '#type' => 'textfield',
    '#title' => t('Abbr'),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('Status abbreviation'),
    '#default_value' => $edit['abbr'],
    '#required' => TRUE,
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#description' => t('Status weight. Status with minimal weight will be set by default!'),
    '#default_value' => $edit['weight'] ? $edit['weight'] : 0,
    '#required' => FALSE,
  );

  $form['comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Comment'),
    '#description' => t('Comment'),
    '#default_value' => $edit['comment'],
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $in_status_id ? t('Submit') : t('Create new status'),
    '#weight' => 30,
  );

  return $form;
}

/**
 *   Statuses add/edit page
 *   Form submit
 *   
 */ 
function status_add_edit_form_submit($form, &$form_state) {
  if ($form_state['values']['status_edit_id']) {
    status_update_status(
      $form_state['values']['which_status'], 
      $form_state['values']['status_edit_id'], 
      $form_state['values']['title'], 
      $form_state['values']['abbr'], 
      $form_state['values']['comment'], 
      $form_state['values']['weight']);
  }
  else {
    status_add_status(
      $form_state['values']['which_status'], 
      $form_state['values']['title'], 
      $form_state['values']['abbr'], 
      $form_state['values']['comment'], 
      $form_state['values']['weight']);
  }
  drupal_goto('course/status');
}


/**
 *   Assing type add/edit page
 *   
 *   
 */ 
function assign_type_add_edit($in_id = NULL) {
  $output = drupal_get_form('assign_type_add_edit_form', $in_id);

  return $output;
}


/**
 *   Assing type add/edit page
 *   Form  
 *   
 */ 
 function assign_type_add_edit_form(&$form_state, $in_id ) {
  
  if ($in_id) {
    $edit = status_get_assign_type($in_id);
    $form['assign_type_edit_id'] = array(
      '#type' => 'value',
      '#value' => $in_id,
    );
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Status title'),
    '#default_value' => $edit['title'],
    '#required' => TRUE,
  );

  $form['sum'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display sum at grades page.'),
    '#default_value' => $edit['sum'],
  );

  $form['last'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display last result at grades page.'),
    '#default_value' => $edit['last'],
  );


  $form['comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Comment'),
    '#description' => t('Comment'),
    '#default_value' => $edit['comment'],
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $in_id ? t('Submit') : t('Create new assign type'),
    '#weight' => 30,
  );

  return $form;
}


/**
 *   Assing type add/edit page
 *   Form submit
 *   
 */ 
function assign_type_add_edit_form_submit($form, &$form_state) {
  
  if ($form_state['values']['assign_type_edit_id']) {
    status_update_assign_type(
      $form_state['values']['assign_type_edit_id'], 
      $form_state['values']['title'], 
      $form_state['values']['comment'], 
      $form_state['values']['sum'], 
	  $form_state['values']['last']);
  }
  else {
    status_add_assign_type(
      $form_state['values']['title'], 
      $form_state['values']['comment'], 
      $form_state['values']['sum'], 
	  $form_state['values']['last']);
  }
  drupal_goto('course/status');
}


/**
 *   Status delete page
 *   
 *   
 */ 
 function status_delete($in_which_status, $in_status_id) {
  return drupal_get_form('status_delete_page', $in_which_status, $in_status_id);
}

/**
 *   Status delete page
 *   Form
 *   
 */ 
 function status_delete_page(&$form_state, $in_which_status, $in_status_id) {

  $form['which_status'] = array(
    '#type' => 'value',
    '#value' => $in_which_status,
  );
  $form['status_id'] = array(
    '#type' => 'value',
    '#value' => $in_status_id,
  );

  $status = status_get_status($in_which_status, $in_status_id);

  return confirm_form(
    $form,  
    t('Are you sure you want to delete the status "%name"?', array('%name' => $status['title'])),  
    'course/status', 
    '',  
    t('Delete'),  
    t('Cancel'));
}

/**
 *   Status delete page
 *   Form submit
 *   
 */ 
function status_delete_page_submit($form, &$form_state) {
  
  status_delete_status($form_state['values']['which_status'], $form_state['values']['status_id']);
  drupal_goto('course/status');
}

/**
 *   Assing type delete page
 *   
 *   
 */ 
function assign_type_delete($in_type_id) {
  return drupal_get_form('assign_type_delete_page', $in_type_id);
}

/**
 *   Assing type delete page
 *   Form
 *   
 */ 
 function assign_type_delete_page(&$form_state, $in_type_id) {

  $form['assign_type_id'] = array(
    '#type' => 'value',
    '#value' => $in_type_id,
  );

  $assign_type = status_get_assign_type($in_type_id);

  return confirm_form(
    $form,  
    t('Are you sure you want to delete the assign type "%name"?', array('%name' => $assign_type['title'])),  
    'course/status', 
    '',  
    t('Delete'),  
    t('Cancel'));
}

/**
 *   Assing type delete page
 *   Form submit  
 *   
 */ 
 function assign_type_delete_page_submit($form, &$form_state) {
  status_delete_assign_type($form_state['values']['assign_type_id']);
  drupal_goto('course/status');
}


?>