<?php

/**
 * @file
 * GML Course system
 * 
 * Supervisor groups control - db functions
 *
 */

 /**
 *   Get all supervisor groups
 *
 *   @return - group array
 */ 
function supgroups_fetch_supgroups() {
  $result = db_query( "SELECT * FROM {gmlcourse_supgroups} ORDER BY weight ASC" );

  $supgroups = array();
  while ( $supgroup = db_fetch_array($result) ) {
    $supgroups[] = $supgroup;
  }

  return $supgroups;
}

/**
 *   Get supervisor group by id
 *
 *   
 */ 
 function supgroup_fetch_supgroup( $in_group_id ) {
  $result = db_fetch_array( db_query( "SELECT *
										FROM {gmlcourse_supgroups}
										WHERE group_id = %d", 
										$in_group_id ) );

  return $result;
}

/**
 *   Get supervisor group by id
 *
 *   
 */ 
 function supgroup_add_group( $in_title, $in_weight, $in_description ) {
  db_query( "INSERT INTO {gmlcourse_supgroups}
		    (title, weight, description)
		    VALUES ('%s', %d, '%s')", 
		    $in_title, 
		    $in_weight, 
		    $in_description );
}

/**
 *   Update supervisor group data
 *
 *   
 */ 
function supgroup_update_group( $in_group_id, $in_title, $in_weight, $in_description ) {
  db_query( "UPDATE {gmlcourse_supgroups} SET
		    title = '%s', weight = %d, description = '%s'
		    WHERE group_id = %d",  
		    $in_title, 
		    $in_weight, 
		    $in_description, 
			$in_group_id );
}

/**
 *   Delete all supervisor group data from db
 *
 *   
 */
 function supgroup_delete_from_db( $in_group_id ) {
  db_query("START TRANSACTION");

  db_query("DELETE FROM {gmlcourse_users2supgroups} WHERE group_id = %d", $in_group_id);
  db_query("DELETE FROM {gmlcourse_supgroups2permissions} WHERE group_id = %d", $in_group_id);
  db_query("DELETE FROM {gmlcourse_supgroups} WHERE group_id = %d", $in_group_id);

  db_query("COMMIT");
}

/**
 *   Delete assign permission data from db
 *
 *   
 */
 function supgroup_delete_assign_from_db( $in_course_id, $in_assign_id ) {
  db_query("DELETE FROM {gmlcourse_supgroups2permissions}
				WHERE course_id = %d AND
					  assign_id = %d", 
				$in_course_id, $in_assign_id );
}

/**
 *   Delete user-supgroup relations from db
 *
 *   
 */
function supgroup_delete_user_from_db( $in_user_id ) {
  db_query("DELETE FROM {gmlcourse_users2supgroups}
				WHERE user_id = %d", 
				$in_user_id );
}

/**
 *   Delete supervisor group from db
 *
 *   
 */
function supgroup_delete_course_from_db( $in_course_id, $in_transaction ) {
  db_query("DELETE FROM {gmlcourse_supgroups2permissions} WHERE course_id = %d", $in_course_id);
}

/**
 *   Get all supgroup members as array
 *
 *   
 */
function supgroups_fetch_members( $in_group_id ) {
  $result = db_query( "SELECT {gmlcourse_users2supgroups}.*,
							  {gmlcourse_userinfo}.*
					   FROM {gmlcourse_users2supgroups}, {gmlcourse_userinfo}
					   WHERE {gmlcourse_users2supgroups}.group_id = %d AND
							 {gmlcourse_userinfo}.student_id = {gmlcourse_users2supgroups}.user_id", 
					   $in_group_id );

  $members = array();
  while ( $member = db_fetch_array($result) ) {
    $members[] = $member;
  }

  return $members;
}

/**
 *   Check user in supgroup
 *       
 *   
 */
function supgroup_test_member( $in_group_id, $in_user_id ) {
  $result = db_fetch_array( db_query( "SELECT *
										FROM {gmlcourse_users2supgroups}
										WHERE group_id = %d AND
											  user_id = %d", 
										$in_group_id, $in_user_id ) );

  return $result;
}

/**
 *   Add member to supgroup
 *       
 *   
 */
 function supgroups_add_member( $in_group_id, $in_user_id ) {
  db_query( "INSERT INTO {gmlcourse_users2supgroups}
				( group_id, user_id )
				VALUES (%d, %d)", 
				$in_group_id, $in_user_id );
}

/**
 *   Remove member from supgroup
 *       
 *   
 */
 function supgroup_remove_member( $in_group_id, $in_user_id ) {
  db_query("DELETE FROM {gmlcourse_users2supgroups}
				WHERE group_id = %d AND
					  user_id = %d", 
				$in_group_id, $in_user_id );
}

/**
 *   Returns supervisor groups permissions array
 *       
 *   
 */
 function supgroup_fetch_supgroup_permissions( $in_group_id ) {
  $result = db_query( "SELECT *
						FROM {gmlcourse_supgroups2permissions}
						WHERE group_id = %d
						ORDER BY course_id ASC, assign_id ASC", 
						$in_group_id );

  $permissions = array();

  while ( $permission = db_fetch_array( $result ) ) {
    $permissions[] = $permission;
  }

  return $permissions;
}

?>