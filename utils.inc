<?php

/**
 * @file
 * GML Course system
 * 
 * Misc useful functions
 *
 */

function date_str2arr($in_date_string) {
  return sscanf($in_date_string, "%04d-%02d-%02d %d:%d:%d");
}


function date_arr2str($in_year, $in_month, $in_day, $in_hour = NULL, $in_minute = NULL, $in_second = NULL) {
  if ($in_hour) {
    return sprintf("%04d-%02d-%02d %d:%d:%d", $in_year, $in_month, $in_day, $in_hour, $in_minute, $in_second);
  }
  else {
    return sprintf("%04d-%02d-%02d", $in_year, $in_month, $in_day);
  }
}


function edit_date_to_db_date($in_edit_date) {
  sscanf($in_edit_date, "%d.%d.%d %d:%d", $day, $month, $year, $hour, $minute);
  $date = $year . "-" . $month . "-" . $day . " " . $hour . ":" . $minute;
  return $date;
}

//////////////////////////////////////////////////////////////////////////////

/// @param in_date date in format "yy-mm-dd hh:mm"

function db_date_to_edit_date($in_db_date) {
  sscanf($in_db_date, "%d-%d-%d %d:%d", $year, $month, $day, $hour, $minute);

  if ( intval( $day ) < 10 ) {
    $day = '0' . $day;
  }
  if ( intval( $month ) < 10 ) {
    $month = '0' . $month;
  }
  if ( intval( $hour ) < 10 ) {
    $hour = '0' . $hour;
  }
  if ( intval( $minute ) < 10 ) {
    $minute = '0' . $minute;
  }

  $date = $day . "." . $month . "." . $year . " " . $hour . ":" . $minute;
  return $date;
}



/// @param in_date date in format "yy-mm-dd hh:mm"

function db_date_to_edit_date_use_html($in_db_date) {
  sscanf($in_db_date, "%d-%d-%d %d:%d", $year, $month, $day, $hour, $minute);

  if ( intval( $day ) < 10 ) {
    $day = '0' . $day;
  }
  if ( intval( $month ) < 10 ) {
    $month = '0' . $month;
  }
  if ( intval( $hour ) < 10 ) {
    $hour = '0' . $hour;
  }
  if ( intval( $minute ) < 10 ) {
    $minute = '0' . $minute;
  }

  $date = $day . "." . $month . "." . $year . "&nbsp;&nbsp;|&nbsp;&nbsp;" . $hour . ":" . $minute;
  return $date;
}

//////////////////////////////////////////////////////////////////////////////
function return_bytes_from_ini($val) {
  $val = trim($val);
  $last = strtolower($val{strlen($val) -1});
  switch ($last) {
    // The 'G' modifier is available since PHP 5.1.0

    case 'g':
      $val *= 1024;
    case 'm':
      $val *= 1024;
    case 'k':
      $val *= 1024;
  }

  return $val;
}

//////////////////////////////////////////////////////////////////////////////
//old - left from 5.0 api version - not used now
function own_file_upload_max_size() {
  static $max_size = -1;

  if ($max_size < 0) {
    $upload_max = return_bytes_from_ini(ini_get('upload_max_filesize'));
    // sanity check- a single upload should not be more than 

    // 2/3 of the size limit of the total post

    $post_max = return_bytes_from_ini(ini_get('post_max_size')) * 2 / 3;
    $max_size = ($upload_max < $post_max) ? $upload_max : $post_max;
  }
  return floor($max_size  / 1024 / 1024);
}

?>