<?php

/**
 * @file
 * GML Course system
 * 
 * Submission admin interface
 *
 */
 
 /**
 *   Display submission list for assing
 *
 *   
 */ 

function submissions_admin_display($in_course_id, $in_assign_number) {
  $output .= common_html_small_font_class( true );
  $output .= common_html_bordered_intable_links_class(true);
  $output .= drupal_get_form('submissions_admin_display_form', $in_course_id, $in_assign_number);
  $output .= common_html_bordered_intable_links_class(false);
  $output .= common_html_small_font_class( false );

  return $output;

}


/**
 *   Display submission list for assing
 *   Form
 *   
 */ 
function submissions_admin_display_form(&$form_state, $in_course_id, $in_assign_number) {
  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);
  $submissions = submission_fetch_submissions_brief($in_course_id, $in_assign_number);

  $form = array();
  //create rows for table
  foreach ($submissions as $i => $submission) {

    $student_name = $submission['last_name'] . ' ' . $submission['first_name'] . ' ' . $submission['middle_name'];


    $form[$i]['login'] = array(
      '#value' => $submission['login'],
    );

    $form[$i]['name'] = array(
      '#value' => $student_name,
    );

    $form[$i]['comp_sum'] = array(
      '#value' => common_str_get_grade_string($submission['comp_sum']),
    );

    $form[$i]['status'] = array(
      '#value' => $submission['status'],
    );

    $form[$i]['works'] = array(
      '#value' => l(t('works'), 'course/' . $in_course_id . '/assign/' . $in_assign_number .  '/submissions/' . $submission['student_id'] . '/works'),
    );

    $form[$i]['edit'] = array(
      '#value' => l(t('edit'), 'course/' . $in_course_id . '/assign/' . $in_assign_number .  '/submissions/' . $submission['student_id'] . '/edit'),
    );

    $form[$i]['delete'] = array(
      '#value' => l(t('delete'), 'course/' . $in_course_id . '/assign/' . $in_assign_number .  '/submissions/' . $submission['student_id'] . '/delete'),
    );
  }


  return $form;
}

/**
 *   Display submission list for assing
 *   hook_theme
 *   
 */ 
 function theme_submissions_admin_display_form($form) {
  foreach ($form as $name => $element) {
    if (isset($element['name']) && is_array($element['name'])) {
      $rows[] = array(
        drupal_render($element['login']),
        drupal_render($element['name']),
        drupal_render($element['comp_sum']),
        drupal_render($element['status']),
        drupal_render($element['works']),
        drupal_render($element['edit']),
        drupal_render($element['delete']),
      );
      unset($form[$name]);
    }
  }

  $header = array(
    t('Login'),
    t('Name'),
    t('Grade'),
    t('Status'),
    array(
      'data' => t('Operations'),
      'colspan' => 3,
    ),
  );
  //display as table
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;

}

/**
 *   Submission edit page
 *   
 *   
 */ 
function submission_add_edit($in_course_id = NULL, $in_assign_number = NULL, $in_student_id = NULL) {
  $output = drupal_get_form('submission_add_edit_form', $in_course_id, $in_assign_number, $in_student_id);
  return $output;
}

/**
 *   Submission edit page
 *   Form
 *   
 */ 
 function submission_add_edit_form(&$form_state, $in_course_id = NULL, $in_assign_number = NULL, $in_student_id = NULL) {

  if ($in_course_id) {
    $form['course_id'] = array(
      '#type' => 'value',
      '#value' => $in_course_id,
    );
  }

  if ($in_assign_number) {
    $form['assign_number'] = array(
      '#type' => 'value',
      '#value' => $in_assign_number,
    );
  }

  $edit = array();


  if ($in_student_id) {
    $edit = submission_fetch_submission_by_student_id($in_course_id, $in_assign_number, $in_student_id);

    // get user name from id and store it in $edit

    $student_name = student_get_name_from_id($in_student_id);

    $edit['name'] = $student_name;

    $form['student_id'] = array(
      '#type' => 'value',
      '#value' => $in_student_id,
    );
    $form['student_name'] = array(
      '#type' => 'value',
      '#value' => $student_name,
    );

    // get supervisor name from id and store in $edit

    $supervisor_name = student_get_name_from_id($edit['supervisor_id']);
    $edit['supervisor_name'] = $supervisor_name;
  }
  else {
    $form['student_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Student name'),
      '#size' => 60,
      '#maxlength' => 250,
      '#description' => t('Student name (last, first, middle)'),
      '#autocomplete_path' => 'user/autocomplete_userinfo_4course/' . $in_course_id,
      '#default_value' => $edit['name'],
      '#required' => TRUE,
    );
  }

  $form['supervisor_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Supervisor name'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Supervisor name (last, first, middle)'),
    '#autocomplete_path' => 'user/autocomplete_userinfo_supervisor',
    '#default_value' => $edit['supervisor_name'],
    '#required' => FALSE,
  );

  // get assign components

  $components = component_fetch_components($in_course_id, $in_assign_number);

  $grade_fields = array();

  $form['components'] = array('#tree' => TRUE);


  foreach ($components as   $i => $component) {
    $comp_name = 'comp_' . $component['db_number'];

    $form['components'][$comp_name] = array(
      '#type' => 'textfield',
      '#title' => $component['title'],
      '#size' => 4,
      '#maxlength' => 5,
      '#description' => t('Max : ') . $component['max'],
      '#default_value' => $edit[$comp_name],
      '#required' => FALSE,
    );

    array_push( $grade_fields, array('name' => $comp_name, 'max' => floatval( $component['max'] )) );
  }


  $form['value_grade_fields'] = array(
    '#type' => 'value',
    '#value' => $grade_fields,
  );


  // prepare array with status names

  $statuses = status_get_statuses("submission");
  $status_names = array();
  foreach ($statuses as $status) {
    $status_names[$status['id']] = $status['title'];
  }

  // form select with statuses

  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Submission status'),
    '#description' => t('Submission status'),
    '#default_value' => ($edit['status'] ? $edit['status'] : 1),
    '#options' => $status_names,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t($in_assign_number ? 'Submit' : 'Add submission'),
    '#weight' => 30,
  );

  return $form;
}

/**
 *   Submission edit page
 *   Form validate
 *   
 */ 
function submission_add_edit_form_validate($form, &$form_state) {
  $student_id = student_get_id_from_name($form_state['values']['student_name']);
  $supervisor_id = student_get_id_from_name($form_state['values']['supervisor_name']);

  if (!$student_id) {
    form_set_error( 'student_name', t('Could not find a student with such name or multiple students found.') );
  }

  if (!$form_state['values']['student_id']) {
    // check if there is already submission from this user

    $submission = submission_fetch_submission_by_student_id($form_state['values']['course_id'], $form_state['values']['assign_number'], $student_id);

    if ($submission) {
      form_set_error( 'student_name', t('This user already has a submission for this assign!') );
    }
  }
  //check supervisor access
  if ( !student_verify_course_assign_access( $student_id, $form_state['values']['course_id'], $form_state['values']['assign_number'] ) ) {
    form_set_error( 'student_name', t('Error : specified student has no access to selected course and/or assignment.') );
  }

  if ($form_state['values']['supervisor_name'] && !$supervisor_id) {
    form_set_error( 'supervisor_name', t('Could not find a supervisor with such name or multiple supervisors found.') );
  }

  $supervisor = user_load( array('uid' => $supervisor_id) );

  if ( ( !$supervisor || !$supervisor_id ) && $form_state['values']['supervisor_name'] ) {
    form_set_error( 'supervisor_name', t('Could not find a supervisor with such name or multiple supervisors found.') );
  }

  if ( $supervisor && $supervisor_id ) {
    if ( user_access( 'access supervisor front-end', $supervisor ) ) {
      if ( !user_access( 'administer courses', $supervisor ) ) {
        if ( !supervisor_verify_course_assign_num_pair( $supervisor_id, $form_state['values']['course_id'], $form_state['values']['assign_number'] ) ) {
          form_set_error( 'supervisor_name', t('Error : specified supervisor has no access to selected course and/or assignment.') );
        }
      }
    }
    else {
      form_set_error( 'supervisor_name', t('Error : specified supervisor has no supervisor privileges.') );
    }
  }

  $grade_fields = $form_state['values']['value_grade_fields'];

  //check submission components
  if ( $grade_fields != NULL ) {
    foreach ( $grade_fields as $field ) {
      $current_val = $form_state['values']['components'][$field['name']];

      if ( $current_val ) {
        if ( is_numeric( $current_val ) ) {
          if ( floatval( $current_val ) > $field['max'] ) {
            form_set_error( 'components][' . $field['name'], t('Grade exceeds maximum allowed value.') );
          }
        }
        else {
          form_set_error( 'components][' . $field['name'], t('Entered value is not a valid numeric value.') );
        }
      }
    }
  }
}

/**
 *   Submission edit page
 *   Form Submit handler
 *   
 */ 
function submission_add_edit_form_submit($form, &$form_state) {

  $student_id = student_get_id_from_name( $form_state['values']['student_name'] );

  if ( !$student_id ) {
    drupal_set_message(t('Could not find a student with such name or multiple students found.'), 'error');
    return;
  }
  //check student
  if ( !student_verify_course_assign_access( $student_id, $form_state['values']['course_id'], $form_state['values']['assign_number'] ) ) {
    drupal_set_message( t('Error : specified student has no access to selected course and/or assignment.'), 'error' );
    return;
  }

  $supervisor_id = student_get_id_from_name($form_state['values']['supervisor_name']);
  $supervisor = user_load( array('uid' => $supervisor_id) );
  //check supervisor
  if ( ( !$supervisor || !$supervisor_id ) && $form_state['values']['supervisor_name'] ) {
    drupal_set_message(t('Could not find a supervisor with such name or multiple supervisors found.'), 'error');
    return;
  }

  if ( $supervisor && $supervisor_id ) {
    if ( user_access( 'access supervisor front-end', $supervisor ) ) {
      if ( !user_access( 'administer courses', $supervisor ) ) {
        if ( !supervisor_verify_course_assign_num_pair( $supervisor_id, $form_state['values']['course_id'], $form_state['values']['assign_number'] ) ) {
          drupal_set_message( t('Error : specified supervisor has no access to selected course and/or assignment.'), 'error' );
          return;
        }
      }
    }
    else {
      drupal_set_message( t('Error : specified supervisor has no supervisor privileges.'), 'error' );
      return;
    }
  }
  //update submission
  if ($form_state['values']['student_id']) {
    submission_update_submission(
    $form_state['values']['course_id'], 
    $form_state['values']['assign_number'], 
    $student_id, 
    $supervisor_id, 
    $form_state['values']['components'], 
    $form_state['values']['status']);

    drupal_set_message(t('Submission of %name has been updated', 
      array('%name' => $form_state['values']['student_name'])));
  }
  else {
    ///Add submisssion node here

    $gml_node = array();
    $gml_node = (object) $gml_node;
    $gml_node->created = time();
    $gml_node->nid = NULL;
    $gml_node->type = 'gml_course_submission';
    $gml_node->status = 1; //published

    $gml_node->promote = 0;
    $gml_node->sticky = 0;
    $gml_node->comment = 2;
    $gml_node->uid = 1;
    $gml_node->readmore = FALSE;
    $gml_node->body = '';
    $gml_node->path = 'user/' . $student_id . '/course/' . $form_state['values']['course_id'] . '/subm/' . $form_state['values']['assign_number'];
    $gml_node->title = $form_state['values']['student_name'] . ' submission view';
    node_save( $gml_node );

    ///Add supervisor comment

    if ($form_state['values']['comment']) {
      $comment_fields = array();
      $comment_fields['values']['author'] = $supervisor->name;
      $comment_fields['values']['subject'] = t("Assign submission comment");
      $comment_fields['values']['comment'] = $form_state['values']['comment'];
      $comment_fields['values']['op'] = t('Save');
      $comment_fields['values']['nid'] = $gml_node->nid;

      comment_form_submit("comment_form", $comment_fields);
    }


    //add submission
    submission_add_submission(
    $form_state['values']['course_id'], 
    $form_state['values']['assign_number'], 
    $student_id, 
    $supervisor_id, 
    $form_state['values']['components'], 
    $form_state['values']['status'], 
	$gml_node->nid);

    drupal_set_message(t('Submission of %name has been added', 
      array('%name' => $form_state['values']['student_name'])));
    drupal_set_message($errs);
  }
  //goto submission list
  drupal_goto('course/' . $form_state['values']['course_id'] . '/assign/' . $form_state['values']['assign_number'] . '/submission');

}

/**
 *   Submission delete page
 *   
 *   
 */
function submission_delete($in_course_id = NULL, $in_assign_number = NULL, $in_student_id = NULL) {
  return drupal_get_form('submission_delete_page', $in_course_id, $in_assign_number, $in_student_id);
}


/**
 *   Submission delete page
 *   Form
 *   
 */
function submission_delete_page(&$form_state, $in_course_id, $in_assign_number, $in_student_id) {

   if ( !user_access('administer courses') ) form_return_noaccess();
   
  $form['course_id'] = array(
    '#type' => 'value',
    '#value' => $in_course_id,
  );
  $form['assign_number'] = array(
    '#type' => 'value',
    '#value' => $in_assign_number,
  );
  $form['student_id'] = array(
    '#type' => 'value',
    '#value' => $in_student_id,
  );

  $component = submission_fetch_submission_by_student_id($in_course_id, $in_assign_number, $in_student_id);
  $info = $component['name'];

  return confirm_form(
    $form, 
    t('Are you sure you want to delete the submission of %name?', array('%name' => $info)), 
    'course/' . $in_course_id . '/assign/' . $in_assign_number . '/submission', 
    '', 
    t('Delete'), 
    t('Cancel'));
}

/**
 *   Submission delete page
 *   Form submit
 *   
 */
function submission_delete_page_submit($form, &$form_state) {

  if ( !user_access('administer courses') ) form_return_noaccess();
  // remove submission

  submission_delete_from_db($form_state['values']['course_id'], $form_state['values']['assign_number'], $form_state['values']['student_id']);
  //goto submission list
  drupal_goto('course/' . $form_state['values']['course_id'] . '/assign/' . $form_state['values']['assign_number'] . '/submissions');
}

?>