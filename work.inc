<?php

/**
 * @file
 * GML Course system
 * 
 * Works db functions
 *
 */

/**
 *   Update work info in db
 *
 *   @param in_date date in format "yy-mm-dd hh:mm"
 */ 
function work_update(
  $in_work_id, 
  $in_form_name, 
  $in_course_id, 
  $in_assign_number, 
  $in_student_id, 
  $in_date, 
  $in_status, 
  $in_confirmed, 
  $in_supervisor_private_comment, 
  $in_intransaction = FALSE) {

  if ( !$in_intransaction ) {
    db_query("START TRANSACTION");
  }

  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);


  $result = db_query(
    "UPDATE {gmlcourse_works}
     SET
       student_id = %d,
       assign_id = %d,
       date = '%s',
       status = %d,
       confirmed = %d,
       supervisor_private_comment = '%s'
    WHERE id=%d", 
    $in_student_id, 
    $assign['id'], 
    $in_date, 
    $in_status, 
    $in_confirmed, 
    $in_supervisor_private_comment, 
    $in_work_id);


  $path = work_get_upload_name($in_course_id, $in_assign_number, $in_student_id, $in_work_id, FALSE, TRUE); // create dirs


  $validators = array(
    'file_validate_extensions' => array('zip'),
    'file_validate_size' => array(own_file_upload_max_size() * 1024 * 1024),
  );


  $file = file_save_upload($in_form_name, $validators, $path, FILE_EXISTS_REPLACE);

  if ($file) {
    file_move($file, $path, FILE_EXISTS_REPLACE);
  }


  if (!$file) {
    if (!$in_intransaction) {
      db_query("ROLLBACK");
    }

    return FALSE;
  }

  // update table with filesize 
  
  // todo: we need to do this in a single insert query somehow

  $filesize = filesize($path);
  $result = db_query(
    "UPDATE {gmlcourse_works}
     SET filesize=%d WHERE id=%d", 
    $filesize, 
    $in_work_id);

  if ( !$in_intransaction ) {
    db_query("COMMIT");
  }

  return TRUE;
}

/**
 *   Update only status and commnet for work
 *
 *   
 */ 
function work_update_supervisor_data(
  $in_work_id, 
  $in_status, 
  $in_supervisor_private_comment) {

  db_query(
		"UPDATE {gmlcourse_works}
		 SET
		   status = %d,
		   supervisor_private_comment = '%s'
		WHERE id = %d", 
		$in_status, 
		$in_supervisor_private_comment, 
		$in_work_id);

  return;
}


/**
 *  Insert new work in db
 *
 *  @param in_date date in db format 
 */ 
function work_insert(
  $in_form_name, 
  $in_course_id, 
  $in_assign_number, 
  $in_student_id, 
  $in_date, 
  $in_status, 
  $in_confirmed, 
  $in_supervisor_private_comment, 
  $in_intransaction = FALSE) {

  if (!$in_intransaction) {
    db_query("START TRANSACTION");
  }

  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);

  $result = db_query(
    "INSERT INTO {gmlcourse_works}
    (student_id, assign_id, date, status, confirmed, supervisor_private_comment)
    VALUES (%d, %d, '%s', %d, %d, '%s')", 
    $in_student_id, 
    $assign['id'], 
    $in_date, 
    $in_status, 
    $in_confirmed, 
    $in_supervisor_private_comment);


  $work_id = db_last_insert_id("gmlcourse_works", "id");

  $path = work_get_upload_name($in_course_id, $in_assign_number, $in_student_id, $work_id, FALSE, TRUE); // create dirs

  //actual file saving, we checked previosly if file is ok
  $validators = array(
    'file_validate_extensions' => array('zip'),
    'file_validate_size' => array(own_file_upload_max_size() * 1024 * 1024),
  );

  $file = file_save_upload($in_form_name, $validators, $path);

  if ($file) {
    file_move($file, $path, FILE_EXISTS_REPLACE);
  }

  if (!$file) {
    if (!$in_intransaction) {
      db_query("ROLLBACK");
    }

    return FALSE;
  }

  // update table with filesize 

  $filesize = filesize($path);
  $result = db_query(
    "UPDATE {gmlcourse_works}
     SET filesize=%d WHERE id=%d", 
    $filesize, 
    $work_id);

  if (!$in_intransaction) {
    db_query("COMMIT");
  }

  return TRUE;
}

/**
 *  Delete work from db
 *
 *  Uploaded work file will be also deleted
 */ 
function work_delete_from_db($in_work_id) {
  $work = work_fetch_work($in_work_id);
  $assign = assign_fetch_assign($work['assign_id']);

  $path = work_get_upload_name($assign['course_id'], $assign['number'], $work['student_id'], $in_work_id);

  // delete work file

  $success = unlink($path);

  if (!$success) {
    return FALSE;
  }

  // delete work info from db

  $result = db_query("DELETE FROM {gmlcourse_works} WHERE id=%d", $in_work_id);

  if (!$result) {
    return FALSE;
  }

  return TRUE;
}

/**
 *  Delete all works for given student from db from db
 *
 *  Uploaded work file will be also deleted
 */ 
 function work_delete_all_from_db($in_course_id, $in_assign_number, $in_student_id, $in_intransaction = FALSE) {
  if (!$in_intransaction) {
    db_query("START TRANSACTION");
  }

  $works = work_fetch_works($in_course_id, $in_assign_number, $in_student_id);

  foreach ($works as $work) {
    work_delete_from_db($work['id'], TRUE);
  }

  if (!$in_intransaction) {
    db_query("COMMIT");
  }
}

/**
 *  Check if this download is our download
 *
 *  
 */ 
function work_fetch_by_path($in_path) {
  
  $matched = preg_match("/submission\/(\d+)\/(\d+)\/(\d+)\.zip/", $in_path, $matches);
  return $matches[3] ? work_fetch_work($matches[3]) : NULL;
}

/**
 *  Returns uploaded work file name
 *
 *  
 */ 
function work_get_upload_name($in_course_id, $in_assign_number, $in_student_id, $in_work_id, $in_relative_path = FALSE, $in_create_dirs = FALSE) {
  $path = _work_get_upload_dir($in_course_id, $in_assign_number, $in_student_id, $in_relative_path, $in_create_dirs);
  return $path . '/' . $in_work_id . '.zip';
}

/**
 *  Returns array of work info arrays
 *
 *  
 */ 
 function work_fetch_works($in_course_id, $in_assign_number, $in_student_id) {
  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);

  $result = db_query("
    SELECT gmlcourse_works.*, {gmlcourse_work_statuses}.title as status_title FROM {gmlcourse_works}, {gmlcourse_work_statuses}
    WHERE assign_id=%d AND student_id=%d AND {gmlcourse_works}.status={gmlcourse_work_statuses}.id
    ORDER BY date ASC", 
    $assign['id'], 
    $in_student_id);

  $works = array();
  while ($work = db_fetch_array($result)) {
    $works[] = $work;
  }

  return $works;
}

/**
 *  Returns work info array for given work id
 *
 *  
 */ 
function work_fetch_work($in_work_id) {
  $result = db_query("
    SELECT gmlcourse_works.*, {gmlcourse_work_statuses}.title as status_title FROM {gmlcourse_works}, {gmlcourse_work_statuses}
    WHERE gmlcourse_works.id=%d AND {gmlcourse_works}.status={gmlcourse_work_statuses}.id", 
    $in_work_id);

  return db_fetch_array($result);
}

/**
 *  Set "confirmed" flag for work with given id
 *
 *  
 */ 
function work_confirm($in_work_id) {
  $result = db_query(
    "UPDATE {gmlcourse_works}
     SET confirmed=%d WHERE id=%d", 
    1, 
    $in_work_id);
}

/**
 *  Check file upload directory
 *  and create or modify permission for it
 *  
 */ 
 function gml_file_check_directory(&$directory, $mode = 0, $form_item = NULL) {
  $directory = rtrim($directory, '/\\');

  // Check if directory exists. 

  if (!is_dir($directory)) {
    if (($mode & FILE_CREATE_DIRECTORY) && @mkdir($directory)) {
      //drupal_set_message(t('The directory %directory has been created.', array('%directory' => $directory))); 

      @chmod($directory, 0775); // Necessary for non-webserver users. 

    }
    else {
      if ($form_item) {
        form_set_error($form_item, t('The directory %directory does not exist.', array('%directory' => $directory)));
      }
      return FALSE;
    }
  }

  // Check to see if the directory is writable. 

  if (!is_writable($directory)) {
    if (($mode & FILE_MODIFY_PERMISSIONS) && @chmod($directory, 0775)) {
      //drupal_set_message(t('The permissions of directory %directory have been changed to make it writable.', array('%directory' => $directory))); 

    }
    else {
      form_set_error($form_item, t('The directory %directory is not writable', array('%directory' => $directory)));
      watchdog('file system', 'The directory %directory is not writable, because it does not have the correct permissions set.', array('%directory' => $directory), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  if ((file_directory_path() == $directory || file_directory_temp() == $directory) && !is_file("$directory/.htaccess")) {
    $htaccess_lines = "SetHandler Drupal_Security_Do_Not_Remove_See_SA_2006_006\nOptions None\nOptions +FollowSymLinks";
    if (($fp = fopen("$directory/.htaccess", 'w')) && fputs($fp, $htaccess_lines)) {
      fclose($fp);
    }
    else {
      $message = t("Security warning: Couldn't write .htaccess file. Please create a .htaccess file in your %directory directory which contains the following lines: <code>!htaccess</code>", array('%directory' => $directory, '!htaccess' => '<br />' . nl2br(check_plain($htaccess_lines))));
      form_set_error($form_item, $message);
      
      watchdog('security', "Security warning: Couldn't write .htaccess file. Please create a .htaccess file in your %directory directory which contains the following lines: <code>!htaccess</code>", array('%directory' => $directory, '!htaccess' => '<br />' . nl2br(check_plain($htaccess_lines))), WATCHDOG_ERROR);
    }
  }

  return TRUE;
}

/**
 *  Returns path to work upload dir
 *  
 *  
 */ 
function _work_get_upload_dir($in_course_id, $in_assign_number, $in_student_id, $in_relative_path = FALSE, $in_create_dirs = FALSE) {
  $assign = assign_fetch_assign_by_number($in_course_id, $in_assign_number);

  $basepath = file_directory_path();
  $fullpath = $basepath;
  gml_file_check_directory($fullpath, $in_create_dirs ? FILE_CREATE_DIRECTORY : 0);

  $path = 'submission';
  $fullpath = $basepath . '/' . $path;
  gml_file_check_directory($fullpath, $in_create_dirs ? FILE_CREATE_DIRECTORY : 0);

  $path = $path . '/' . $in_course_id;
  $fullpath = $basepath . '/' . $path;
  gml_file_check_directory($fullpath, $in_create_dirs ? FILE_CREATE_DIRECTORY : 0);

  $path = $path . '/' . $assign['id'];
  $fullpath = $basepath . '/' . $path;
  gml_file_check_directory($fullpath, $in_create_dirs ? FILE_CREATE_DIRECTORY : 0);

  return $in_relative_path ? $path : $basepath . '/' . $path;

}

/**
 *  Returns count of student works for given assign
 *  
 *  
 */ 
function work_get_number_of_student_works_for_assign( $in_student_id, $in_course_id, $in_assign_num ) {
  $result = db_fetch_array(
				db_query( "SELECT COUNT({gmlcourse_works}.id)
							FROM {gmlcourse_works}, {gmlcourse_assigns}
							WHERE {gmlcourse_works}.student_id = %d AND
								  {gmlcourse_assigns}.id = {gmlcourse_works}.assign_id AND
								  {gmlcourse_assigns}.course_id = %d AND
								  {gmlcourse_assigns}.number = %d", 
							$in_student_id, $in_course_id, $in_assign_num ) );

  if ( $result ) {
    list($key, $val) = each( $result );
    return intval( $val );
  }
  else {
    return 0;
  }
}

/**
 *  Check if maximum number of allowed works per student per assign exceeded
 *  
 *  
 */ 
function work_is_number_of_works_exceeded( $in_student_id, $in_course_id, $in_assign_num ) {
  $max_number_of_works = variable_get('gmlcourse_max_works_per_assign', GML_COURSE_MAX_WORKS_PER_ASSIGN);
  $this_number_of_works = work_get_number_of_student_works_for_assign( $in_student_id, $in_course_id, $in_assign_num );

  if ( $this_number_of_works >= $max_number_of_works ) {
    return true;
  }
  else {
    return false;
  }
}

?>