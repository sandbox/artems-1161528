<?php

/**
 * @file
 * GML Course system
 * 
 * Assign components subsystem user interface
 *
 */


/**
 *   Display components admin interface
 *
 *   @return - drupal page
 */ 
function components_admin_display($in_course_id, $in_assign_number) {

  $output = common_html_small_font_class( true );
  $output .= common_html_bordered_intable_links_class(true);
  $output .= drupal_get_form('components_admin_display_form', $in_course_id, $in_assign_number);
  $output .= common_html_bordered_intable_links_class(false);
  $output .= common_html_small_font_class( false );

  return $output;
}



/**
 *   Display components admin interface - form
 *
 *   @return - drupal form
 */ 
function components_admin_display_form(&$form_state, $in_course_id, $in_assign_number) {
  $components = component_fetch_components($in_course_id, $in_assign_number);

  array_pop($components); //remove last element with number 9999 - used for auto penality

  $form = array();

  foreach ($components as $i => $component) {
    $form[$i]['number'] = array(
      '#value' => $component['number'],
    );

    $form[$i]['title'] = array(
      '#value' => $component['title'],
    );

    $form[$i]['max'] = array(
      '#value' => $component['max'],
    );

    $form[$i]['edit'] = array(
      '#value' => l(t('edit'), 'course/' . $in_course_id . '/assign/' . $in_assign_number .  '/component/' . $component['number'] . '/edit'),
    );

    $form[$i]['delete'] = array(
      '#value' => l(t('delete'), 'course/' . $in_course_id . '/assign/' . $in_assign_number .  '/component/' . $component['number'] . '/delete'),
    );
  }

  return $form;
}

/**
 *   Display components admin interface - theme function
 *
 *   
 */ 
function theme_components_admin_display_form($form) {
  foreach ($form as $name => $element) {
    if (isset($element['title']) && is_array($element['title'])) {
      $rows[] = array(
        drupal_render($element['number']),
        drupal_render($element['title']),
        drupal_render($element['max']),
        drupal_render($element['edit']),
        drupal_render($element['delete']),
      );
      unset($form[$name]);
    }
  }

  $header = array(t('Number'), t('Title'), t('Max'), array(
      'data' => t('Operations'),
      'colspan' => 2,
    ));
  $output = theme('table', $header, $rows,   array('id' => 'assigns'));
  $output .= drupal_render($form);

  return $output;
}


/**
 *   Display components add/edit page
 *
 *  
 */ 
function component_add_edit($in_course_id = NULL, $in_assign_number = NULL, $in_component_number = NULL) {
  $output = drupal_get_form('component_add_edit_form', $in_course_id, $in_assign_number, $in_component_number);
  return $output;
}

/**
 *   Components add/edit form
 *
 *   @return - drupal form
 */ 
 function component_add_edit_form(&$form_state, $in_course_id = NULL, $in_assign_number = NULL, $in_component_number = NULL) {

  if ($in_course_id) {
    $form['course_id'] = array(
      '#type' => 'value',
      '#value' => $in_course_id,
    );
  }

  if ($in_assign_number) {
    $form['assign_number'] = array(
      '#type' => 'value',
      '#value' => $in_assign_number,
    );
  }


  //will fetch all components for selection one to edit, or to calculate suggested component number

  $edit = array();
  $components = component_fetch_components($in_course_id, $in_assign_number);


  if ($in_component_number) {
    $edit = $components[$in_component_number -1];
    $form['component_number'] = array(
      '#type' => 'value',
      '#value' => $in_component_number,
    );
  }
  else {
    $edit['number'] = count($components);
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 250,
    '#description' => t('Compoment title'),
    '#default_value' => $edit['title'],
    '#required' => TRUE,
  );

  $form['number'] = array(
    '#type' => 'textfield',
    '#title' => t('Number'),
    '#size' => 2,
    '#maxlength' => 2,
    '#default_value' => $edit['number'],
    '#description' => t('Component number (starting from 1)'),
    '#required' => TRUE,
  );

  $form['max'] = array(
    '#type' => 'textfield',
    '#title' => t('Max grade'),
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $edit['max'],
    '#description' => t('Component maximal grade (can be negative)'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t($in_assign_number ? 'Submit' : 'Create new component'),
    '#weight' => 30,
  );


  return $form;
}


/**
 *   Components add/edit form
 *
 *   @return - drupal form
 */ 
function component_add_edit_form_validate($form, &$form_state) {
  // $component = component_fetch_component_by_number($form_state['values']['course_id'], $form_state['values']['assign_number'], $form_state['values']['number']);

  //	kpr($component);


  if ($form_state['values']['component_number']) {
    $component = component_fetch_component_by_number($form_state['values']['course_id'], $form_state['values']['assign_number'], $form_state['values']['number']);
    if ($component && $form_state['values']['component_number'] <> $form_state['values']['number']) {
      form_set_error( 'number', t('Error : Component number already in use.'));
      return;
    }
  }
  else {
    $component = component_fetch_component_by_number($form_state['values']['course_id'], $form_state['values']['assign_number'], $form_state['values']['number']);
    if ($component) {
      form_set_error( 'number', t('Error : Component number already in use.'));
      return;
    }
  }

}


/**
 *   Components add/edit form submit
 *
 *   
 */ 
 function component_add_edit_form_submit($form, &$form_state) {

  if ($form_state['values']['component_number']) {
    component_update_component(
      $form_state['values']['course_id'], 
      $form_state['values']['assign_number'], 
      $form_state['values']['component_number'], // old component number

      $form_state['values']['title'],  
            $form_state['values']['number'], 
            $form_state['values']['max']);

    drupal_set_message(t('Component "%title" has been updated',  
      array('%title' => $form_state['values']['title'])));
  }
  else {
    component_add_component(
      $form_state['values']['course_id'], 
      $form_state['values']['assign_number'], 
      $form_state['values']['title'],  
          $form_state['values']['number'], 
          $form_state['values']['max']);

    drupal_set_message(t('Component "%title" has been added',  
      array('%title' => $form_state['values']['title'])));
  }

  drupal_goto('course/' . $form_state['values']['course_id'] . '/assign/' . $form_state['values']['assign_number'] . '/component');

}

/**
 *   Component delete display
 *
 *   
 */ 
 function component_delete($in_course_id = NULL, $in_assign_number = NULL, $in_component_number = NULL) {
  return drupal_get_form('component_delete_page', $in_course_id, $in_assign_number, $in_component_number);
}


/**
 *   Component delete form
 *
 *   @return - drupal form
 */ 
function component_delete_page(&$form_state, $in_course_id = NULL, $in_assign_number = NULL, $in_component_number = NULL) {
  $form['course_id'] = array(
    '#type' => 'value',
    '#value' => $in_course_id,
  );
  $form['assign_number'] = array(
    '#type' => 'value',
    '#value' => $in_assign_number,
  );
  $form['component_number'] = array(
    '#type' => 'value',
    '#value' => $in_component_number,
  );

  $component = component_fetch_component_by_number($in_course_id, $in_assign_number, $in_component_number);
  $info = $component['title'];

  return confirm_form(
    $form,  
    t('Are you sure you want to delete the component %name?', array('%name' => $info)),  
    'course/' . $in_course_id . '/assign/' . $in_assign_number . '/component',  
    '',  
    t('Delete'),  
    t('Cancel'));
}


/**
 *   Component delete form - submit
 *
 *  
 */ 
function component_delete_page_submit($form, &$form_state) {

  // remove component

  component_delete_from_db($form_state['values']['course_id'], $form_state['values']['assign_number'], $form_state['values']['component_number']);

  drupal_goto('course/' . $form_state['values']['course_id'] . '/assign/' . $form_state['values']['assign_number'] . '/component');
}



?>