<?php

/**
 * @file
 * GML Course system
 * 
 * Statuses and assing types db functions
 *
 */

/**
 *   Return status array from given id
 *
 *   
 */ 
function status_get_status($in_which_status, $in_status_id) {
  $tablename = _tablename_from_which_status($in_which_status);
  $result = db_query("SELECT * from {%s} WHERE id=%d", $tablename, $in_status_id);

  return db_fetch_array($result);
}

/**
 *   Add new status to db
 *
 *   
 */ 
 function status_add_status($in_which_status, $in_title, $in_abbr, $in_comment, $in_weight) {

  $tablename = _tablename_from_which_status($in_which_status);

  $result = db_query(
    "INSERT INTO {%s}
    (title, abbr, comment, weight)
    VALUES ('%s', '%s', '%s', %d)", 
    $tablename, 
    $in_title, 
    $in_abbr, 
    $in_comment, 
    $in_weight);

}

/**
 *   Update status in db
 *
 *   
 */ 
function status_update_status($in_which_status, $in_status_id, $in_title, $in_abbr, $in_comment, $in_weight) {

  $tablename = _tablename_from_which_status($in_which_status);

  $result = db_query(
    "UPDATE {%s} SET title='%s', abbr='%s', comment='%s', weight=%d
    WHERE id=%d", 
    $tablename, 
    $in_title, 
    $in_abbr, 
    $in_comment, 
    $in_weight, 
    $in_status_id   );

}

/**
 *   Delete status from db
 *
 *   
 */ 
function status_delete_status($in_which_status, $in_status_id) {
  $tablename = _tablename_from_which_status($in_which_status);
  $result = DB_QUERY("DELETE FROM {%s} WHERE id=%d", $tablename, $in_status_id);

}

/**
 *   Return work or submission status table name
 *
 *   
 */ 
function _tablename_from_which_status($in_which_status) {
  $tablename =
    $in_which_status == 'submission' ? 'gmlcourse_submission_statuses' :
                                       'gmlcourse_work_statuses';

  return $tablename;
}


/**
 *   Return array of statuses arrays existing in system
 *
 *   
 */ 
function status_get_statuses($in_which_status) {
  $tablename = _tablename_from_which_status($in_which_status);

  $result = db_query("SELECT * from {%s} ORDER BY weight", $tablename);

  $statuses = array();
  while ($status = db_fetch_array($result)) {
    $statuses[] = $status;
  }

  return $statuses;
}

/**
 *   Return array of assing types arrays existing in system
 *
 *   
 */ 
function status_get_assign_types() {
  $result = db_query("SELECT * from {gmlcourse_assign_types} ORDER BY id");

  $statuses = array();
  while ($status = db_fetch_array($result)) {
    $statuses[$status['id']] = $status;
  }

  return $statuses;
}

/**
 *   Return assing type array from given id
 *
 *   
 */ 
 function status_get_assign_type($in_id) {

  $result = db_query("SELECT * from {gmlcourse_assign_types} WHERE id=%d", $in_id);

  return db_fetch_array($result);
}

/**
 *   Add new assing type to db
 *
 *   
 */ 
 function status_add_assign_type($in_title, $in_comment, $in_sum, $in_last) {

  $result = db_query(
    "INSERT INTO {gmlcourse_assign_types}
    (title, comment, sum, last)
    VALUES ('%s', '%s', %d, %d)", 
    $in_title, 
    $in_comment, 
    $in_sum, 
	$in_last);

}

/**
 *   Update assing type in db
 *
 *   
 */ 
 function status_update_assign_type($in_type_id, $in_title, $in_comment, $in_sum, $in_last) {

  $result = db_query(
    "UPDATE {gmlcourse_assign_types} SET title='%s', comment='%s', sum=%d, last=%d
    WHERE id=%d", 
    $in_title, 
    $in_comment, 
    $in_sum, 
	$in_last, 
    $in_type_id);

}

/**
 *   Delete assing type from db
 *
 *   
 */ 
 function status_delete_assign_type($in_type_id) {
  $result = DB_QUERY('UPDATE gmlcourse_assigns SET type = 0 WHERE type = %d', $in_type_id);

  if ($result) {
    $result = DB_QUERY("DELETE FROM {gmlcourse_assign_types} WHERE id=%d", $in_type_id);
  }

}

/**
 *   Return default status from db
 *   It`s status with minimum weight
 *   
 */ 
 function get_default_status($in_which_status) {
  $tablename = _tablename_from_which_status($in_which_status);

  // find elemement with the minimal weight  

  $result = db_query(
    "select *
     from {%s}
     where weight in
       (select min(weight)
        from {%s})", $tablename, $tablename);


  return db_fetch_array($result);
}

?>