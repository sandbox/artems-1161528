<?php

/**
 * @file
 * GML Course system
 * 
 * User fucntion for customizing installation
 *
 */

 /**
 *   Calculates penality for work from late days
 *
 *   @return  number
 */ 
function common_get_penalty_from_late_days($in_late_days) {

  $in_late_days_int = (int) $in_late_days;

  // zero late
  if ($in_late_days_int <= 0) {
    return 0;
  }

  //  * Late 1 day - penality 0.5
  //  * Late 2 days - penality 1
  //  * More than 2 days - 1 penality point for one day

  if ($in_late_days <= 2) {
    return -0.5 * $in_late_days;
  }


  return -1.0 - 1.0 * ($in_late_days - 2);
}

/**
 *   Safely encrypts url part
 *   This url use to point to work or submission check page.
 *
 *   NOTE: In drupal.org relese we do not publish our encryption\decryption functions!
 *   You are welcome to write your own functions.
 *
 *   @return text
 */
 function common_encrypt_str_URI_safe( $in_str, $in_key ) {
  $str_to_encrypt = $in_str;
/*
  if ( true ) {
    $chunk_size = rand( 5, 32 );
    $chunk_size_str = $chunk_size;
    if ( $chunk_size < 10 ) {
      $chunk_size_str = '0' . $chunk_size_str;
    }
    $str_to_encrypt = '+' . $chunk_size_str .
							substr( md5( rand() ), 0, $chunk_size ) . $str_to_encrypt . md5( rand() );
  }
  else {
    $str_to_encrypt = '=' . $str_to_encrypt;
  }

  $key_str = md5($in_key);

  $result = XOREncode::encode( $str_to_encrypt, $key_str . md5($key_str) . '#56yTKuRc8^%e( iyGDv(763ZX2!dzx5v_p)IOjm\'l;KYb&64' );
  $result = base64_encode( $result );
  $result = str_replace( array('+', '/', '='), array('-', '.', '_'), $result );
*/
  $result = $str_to_encrypt;

  return $result;
}


/**
 *   Decrypts url part
 *   This url use to point to work or submission check page.
 *   @return text
 */
function common_decrypt_str_URI_safe( $in_str, $in_key ) {
  /*
  $result = str_replace( array('-', '.', '_'), array('+', '/', '='), $in_str );
  $result = base64_decode( $result );

  $key_str = md5($in_key);

  $result = XOREncode::decode( $result,   $key_str . md5($key_str) . '#56yTKuRc8^%e( iyGDv(763ZX2!dzx5v_p)IOjm\'l;KYb&64' );

  if ( $result[0] == '+'  ) {
    $chunk_size = intval( substr($result, 1, 2) );
    $str_size = strlen( $result ) - 3 - $chunk_size - 32;
    $result = substr( $result, 3 + $chunk_size, $str_size );
  }
  else {
    $result = substr( $result, 1 );
  }
 */
  $result = $in_str;
  
  return $result;
}


?>