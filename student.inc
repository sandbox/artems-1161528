<?php

/**
 * @file
 * GML Course system
 * 
 * Student manipulation db functions
 *
 */

/**
 *   Constants - default profile addittional fields names
 */ 

define("CONST_PROFILE_LASTNAME_DEFAULT", "profile_lastname");
define("CONST_PROFILE_MIDDLENAME_DEFAULT", "profile_middlename");
define("CONST_PROFILE_FIRSTNAME_DEFAULT", "profile_firstname");

/**
 *   Returns student id from given name 
 *   Function uses sql LIKE
 *   
 */
 function student_get_id_from_name($in_student_name) {
  $result = db_query("
    SELECT student_id
    FROM {gmlcourse_userinfo}
    WHERE LOWER(CONCAT_WS(' ', last_name, first_name, middle_name)) LIKE LOWER('%%%s%%')", $in_student_name);

  $users = array();
  while ($user = db_fetch_array($result)) {
    $users[] = $user;
  }

  if ( count($users) != 1 ) {
    return NULL;
  } // cannot find unique user


  return $users[0]['student_id'];
}

/**
 *   Returns student array from given name 
 *   Function uses sql LIKE
 *   
 */
 function student_get_user_from_name($in_student_name) {
  $student_id = student_get_id_from_name($in_student_name);

  if ($student_id) {
    $user_data = user_load( array('uid' => $student_id) );
    return $user_data;
  }
  return NULL;
}

/**
 *   Returns name from id
 *   Function uses sql concat_ws - in pg sql we use custom function
 *   
 */
 function student_get_name_from_id($in_student_id) {
  $result = db_query("SELECT CONCAT_WS(' ', last_name, first_name, middle_name) AS name
                            FROM {gmlcourse_userinfo}
                            WHERE student_id=%d", $in_student_id);

  $user = db_fetch_array($result);

  return $user['name'];
}

/**
 *   Returns short name from id
 *   only 1 letter from first and middle name used
 *   
 */
 function student_get_short_name_from_id($in_student_id) {
  $result = db_query("SELECT last_name, first_name, middle_name
                            FROM {gmlcourse_userinfo}
                            WHERE student_id=%d", $in_student_id);

  $user = db_fetch_array($result);

  $result = $user['last_name'] . ' ' . mb_substr($user['first_name'], 0, 1, 'UTF-8') . ' ' . mb_substr($user['middle_name'], 0, 1, 'UTF-8');

  return $result;
}

/**
 *   Returns array of courses, student with id attending to
 *   
 *   
 */
function student_get_courses($in_student_id) {
  $result = db_query(
    "SELECT *
     FROM {gmlcourse_courses}
     WHERE id IN
       (SELECT course_id
        FROM {gmlcourse_groups}
        WHERE id IN
          (SELECT group_id
           FROM {gmlcourse_students2groups}
           WHERE student_id=%d))", $in_student_id);

  $courses = array();
  while ($course = db_fetch_array($result)) {
    $courses[] = $course;
  }

  return $courses;
}

/**
 *   Returns array of courses, student with id can register to
 *   
 *   
 */
 function student_get_courses_to_register($in_student_id) {
  $result = db_query(
    "SELECT *
     FROM {gmlcourse_courses}
     WHERE id not IN
       (SELECT course_id
        FROM {gmlcourse_groups}
        WHERE id IN
          (SELECT group_id
           FROM {gmlcourse_students2groups}
           WHERE student_id=%d))", $in_student_id);

  $courses = array();
  while ($course = db_fetch_array($result)) {
    $courses[] = $course;
  }

  return $courses;
}

/**
 *   Returns array of assings, student with id attending to
 *   
 *   
 */
function student_get_assigns($in_student_id) {
  $result = db_query(
	"SELECT * 
	 FROM {gmlcourse_assigns} 
	 WHERE course_id IN 
		(SELECT course_id
        FROM {gmlcourse_groups}
        WHERE id IN
          (SELECT group_id
           FROM {gmlcourse_students2groups}
           WHERE student_id=%d))", $in_student_id);

  $assigns = array();

  while ($assign = db_fetch_array($result)) {
    $assigns[] = $assign;
  }

  return $assigns;
}

/**
 *   Returns array of assings ids, student with id attending to
 *   
 *   
 */
function student_get_assigns_ids($in_student_id) {
  $result = db_query(
	"SELECT id 
	 FROM {gmlcourse_assigns} 
	 WHERE course_id IN 
		(SELECT course_id
        FROM {gmlcourse_groups}
        WHERE id IN
          (SELECT group_id
           FROM {gmlcourse_students2groups}
           WHERE student_id=%d))", $in_student_id);

  $assigns = array();

  while ($assign = db_fetch_array($result)) {
    $assigns[] = $assign;
  }

  return $assigns;
}

/**
 *   Returns array of student arrays in group with group_id
 *   
 *   
 */
 function student_fetch_students_in_group($in_group_id) {
  $result = db_query(
    "SELECT {gmlcourse_students}.student_id, {gmlcourse_userinfo}.* 
     FROM {gmlcourse_students}, {gmlcourse_students2groups},
			{gmlcourse_userinfo}
     WHERE {gmlcourse_students}.student_id = {gmlcourse_students2groups}.student_id AND
			{gmlcourse_students2groups}.group_id = %d AND
			{gmlcourse_userinfo}.student_id = {gmlcourse_students2groups}.student_id
	 ORDER BY {gmlcourse_userinfo}.last_name ASC,
				{gmlcourse_userinfo}.first_name ASC,
				{gmlcourse_userinfo}.middle_name ASC", $in_group_id);

  $students = array();
  while ($student = db_fetch_array($result)) {
    $students[] = $student;
  }

  return $students;
}

/**
 *   Returns student info array(with groups info) for given student id
 *   
 *   
 */
 function student_fetch($in_student_id) {
  $result = db_query("SELECT * FROM {gmlcourse_students} WHERE student_id=%d", $in_student_id);
  $student_info = db_fetch_array($result);

  $result = db_query(
    "SELECT gmlcourse_students2groups.*, course_id
     FROM {gmlcourse_students2groups}, {gmlcourse_groups} WHERE student_id=%d AND gmlcourse_students2groups.group_id=gmlcourse_groups.id", $in_student_id);

  $groups_info = array();
  while ($groups = db_fetch_array($result)) {
    $groups_info[$groups['course_id']] = $groups['group_id'];
  }

  $student_info['groups'] = $groups_info;

  return $student_info;
}

/**
 *   Returns student info array(only base userinfo) for given student id
 *   
 *   
 */
function student_fetch_info($in_student_id) {
  $result = db_query("SELECT * FROM {gmlcourse_userinfo} WHERE student_id = %d", $in_student_id);
  $student_info = db_fetch_array($result);

  return $student_info;
}

/**
 *   Returns student info array(only base userinfo) for given student login
 *   
 *   
 */
function student_fetch_info_from_login($in_student_login) {
  $result = db_query("SELECT * FROM {gmlcourse_userinfo} WHERE login = '%s'", $in_student_login);
  $student_info = db_fetch_array($result);

  return $student_info;
}

/**
 *   Returns student info array for given course id
 *   All student for given course
 *   
 */
 function student_get_students_attending_course($in_course_id) {

  $result = db_query("SELECT {gmlcourse_students}.*,
							 {gmlcourse_userinfo}.*,
							 {gmlcourse_groups}.title as group_title
					  FROM {gmlcourse_students}, {gmlcourse_userinfo},
							{gmlcourse_students2groups}, {gmlcourse_groups}
					  WHERE {gmlcourse_students}.student_id IN
						(SELECT student_id FROM {gmlcourse_students2groups} WHERE group_id IN
							(SELECT id FROM {gmlcourse_groups} WHERE course_id=%d)) AND
							{gmlcourse_userinfo}.student_id = {gmlcourse_students}.student_id AND
							{gmlcourse_students2groups}.student_id = {gmlcourse_students}.student_id AND
							{gmlcourse_groups}.id = {gmlcourse_students2groups}.group_id
					  ORDER BY {gmlcourse_userinfo}.last_name ASC,
								{gmlcourse_userinfo}.first_name ASC,
								{gmlcourse_userinfo}.middle_name ASC", $in_course_id);

  $student_info = array();
  while ($student = db_fetch_array($result)) {
    $student_info[] = $student;
  }

  return $student_info;
}

/**
 *   Delete all gml course module student info for given student id
 *   
 *   
 */
function student_delete_info($in_student_id) {
  db_query("START TRANSACTION");

  db_query("DELETE FROM {gmlcourse_students} WHERE student_id=%d", $in_student_id);
  db_query("DELETE FROM {gmlcourse_userinfo} WHERE student_id=%d", $in_student_id);
  db_query("DELETE FROM {gmlcourse_students2groups} WHERE student_id=%d", $in_student_id);
  supgroup_delete_user_from_db( $in_student_id );

  db_query("COMMIT");
}

/**
 *   Delete all gml course module student info for given group id
 *   
 *   
 */
function student_delete_all_in_group($in_group_id, $in_intransaction) {
  if ( !$in_intransaction ) {
    db_query("START TRANSACTION");
  }

  // delete student info

  db_query("DELETE FROM {gmlcourse_students} WHERE student_id IN
   (SELECT student_id from {gmlcourse_students2groups}
   WHERE group_id=%d)", $in_group_id);
  db_query("DELETE FROM {gmlcourse_students2groups} WHERE group_id=%d", $in_group_id);

  if (!$in_intransaction) {
    db_query("COMMIT");
  }
}

/**
 *   Delete all gml course module student info for given group id
 *   Used in group removing process
 *   
 */
function student_delete_all_from_group($in_group_id, $in_intransaction) {
  if ( !$in_intransaction ) {
    db_query("START TRANSACTION");
  }

  // delete student info

  db_query("DELETE FROM {gmlcourse_students2groups} WHERE group_id=%d", $in_group_id);

  if (!$in_intransaction) {
    db_query("COMMIT");
  }
}


/**
 *   Update all student gml userinfo
 *   
 *   
 */
function student_update_info($in_student_id, 
								$in_login, 
								$in_firstname, 
								$in_middlename, 
								$in_lastname, 
								$in_student_groups, 
								$in_suspicious_level, 
								$in_supervisor_comment, 
								$in_update_infotable) {
  db_query("START TRANSACTION");

  // erasing user information

  db_query("DELETE FROM {gmlcourse_students} WHERE student_id=%d", $in_student_id);

  if ( $in_update_infotable ) {
    // erasing student info from corresponding table

    db_query("DELETE FROM {gmlcourse_userinfo} WHERE student_id=%d", $in_student_id);
  }

  // erasing obsolete user <=> group pairs

  db_query("DELETE FROM {gmlcourse_students2groups} WHERE student_id=%d", $in_student_id);

  // inseting new user information

  db_query("INSERT INTO {gmlcourse_students} VALUES ( %d, %d, '%s' )", 
		  $in_student_id, $in_suspicious_level, $in_supervisor_comment );

  if ( $in_update_infotable ) {
    // inserting user personal data

    db_query("INSERT INTO {gmlcourse_userinfo}
				(student_id, login, first_name, middle_name, last_name)
				VALUES (%d, '%s', '%s', '%s', '%s')", 
				$in_student_id, 
				check_plain($in_login), 
				check_plain($in_firstname), 
				check_plain($in_middlename), 
				check_plain($in_lastname) );
  }

  // inserting new user <=> groups information

  foreach ($in_student_groups as $group_id) {
    db_query("
	 INSERT INTO {gmlcourse_students2groups} (student_id, group_id)
	 VALUES (%d, %d)", $in_student_id,   $group_id);
  }

  db_query("COMMIT");
}

/**
 *   Update student gml supervisor userinfo
 *   
 *   
 */
function student_update_supervisor_info($in_student_id, $in_suspicious_level, $in_supervisor_comment) {
  db_query("START TRANSACTION");

  // erasing user information

  db_query("DELETE FROM {gmlcourse_students} WHERE student_id=%d", $in_student_id);


  // inseting new user information

  db_query("INSERT INTO {gmlcourse_students} VALUES ( %d, %d, '%s' )", 
		  $in_student_id, check_plain($in_suspicious_level), check_plain($in_supervisor_comment) );


  db_query("COMMIT");
}

/**
 *   Update student gml profile userinfo
 *   
 *   
 */
function student_update_profile_info($in_student_id, 
									  $in_login, 
									  $in_firstname, 
									  $in_middlename, 
									  $in_lastname) {
  db_query("START TRANSACTION");

  // erasing student info from corresponding table

  db_query("DELETE FROM {gmlcourse_userinfo} WHERE student_id=%d", $in_student_id);

  // inserting user personal data

  db_query("INSERT INTO {gmlcourse_userinfo}
				(student_id, login, first_name, middle_name, last_name)
				VALUES (%d, '%s', '%s', '%s', '%s')", 
				$in_student_id, 
				$in_login, 
				$in_firstname, 
				$in_middlename, 
				$in_lastname );

  db_query("COMMIT");
}

/**
 *   Parse and add new user from multiline format input
 *   
 *   
 */
function student_parse_multiline( $in_course_id, $in_text, $in_mail_verbose ) {
  global $base_url;

  //some mail variables

  $from = variable_get('site_mail', ini_get('sendmail_from'));
  $site_name = variable_get('site_name', 'GML');

  $lines = explode("\n", $in_text);

  $group_name = "";
  $id = 0;
  $email = "";
  $last_name  = "";
  $first_name  = "";
  $middle_name  = "";

  foreach ($lines as $line) {
    //group_name, id, e-mail, last_name, first_name, middle_name

    list( $tag, $value) = explode( ":", $line );

    switch ($tag) {
      case "Курс":
        /* do nothing for now FIXME */
        break;
      case "Фамилия":
        $last_name = $value;
        break;
      case "Имя":
        $first_name = $value;
        break;
      case "Отчество":
        $middle_name = $value;
        break;
      case "Группа":
        $group_name = $value;
        break;
      case "Студенческий":
        $id = $value;
        break;
      case "e-mail":
        $email = $value;
        break;
      default:
        break;
    }

  }

  // trim everything

  $group_name = trim($group_name);
  $id = trim($id);
  $email = trim($email);
  $last_name = trim($last_name);
  $first_name = trim($first_name);
  $middle_name = trim($middle_name);

  drupal_set_message("Trying to add $last_name $first_name $middle_name ($group_name, $id, $email)...");
  $group = group_in_course_fetch_by_name( $in_course_id, trim($group_name) );
  if ( !$group ) {
    drupal_set_message("Can't parse group " . $group_name . ". Cancelling", 'error');
    return;
  }

  add_new_student_user($group, $id, $email, $last_name, $first_name, $middle_name, $in_mail_verbose);


}

/**
 *   Parse and add new user from csv format input
 *   
 *   
 */
function student_parse_csv( $in_course_id, $in_text, $in_mail_verbose ) {
  global $base_url;

  if (!$in_text) {
    return;
  }

  //some mail variables

  $from = variable_get('site_mail', ini_get('sendmail_from'));
  $site_name = variable_get('site_name', 'GML');

  $errors = array();
  $lines = explode("\n", $in_text);
  foreach ($lines as $line) {
    //group_name, id, e-mail, last_name, first_name, middle_name

    list( $group_name, $id, $email, $last_name, $first_name, $middle_name ) = explode( ",", $line );

    $group = group_in_course_fetch_by_name( $in_course_id, trim($group_name) );
    if ( !$group ) {
      drupal_set_message("Can't parse group " . $group_name . ". Skipping line: " . $line, 'error');
      continue;
    }

    $group_name = trim($group_name);
    $id = trim($id);
    $email = trim($email);
    $last_name = trim($last_name);
    $first_name = trim($first_name);
    $middle_name = trim($middle_name);

    add_new_student_user($group, $id, $email, $last_name, $first_name, $middle_name, $in_mail_verbose);

  }
}


/**
 *   Add new student user
 *   
 *   
 */
 function add_new_student_user($group, $id, $email, $last_name, $first_name, $middle_name, $in_mail_verbose) {
  // find profile fields that we use

  $profile_lastname = variable_get('gmlcourse_student_lastname', CONST_PROFILE_LASTNAME_DEFAULT);
  $profile_middlename = variable_get('gmlcourse_student_middlename', CONST_PROFILE_MIDDLENAME_DEFAULT);
  $profile_firstname = variable_get('gmlcourse_student_firstname', CONST_PROFILE_FIRSTNAME_DEFAULT);
  $student_role_rid = variable_get('gmlcourse_student_role', 3);


  //check if the user exists already

  if ( $id ) {
    $user = user_load( array('name' => $id) );

    // if user already exists we should just modify his course/group information

    if ( $user ) {
      $user_id = $user->uid;

      if ( student_verify_course_access_by_sc_ids( $user_id, $in_course_id ) ) {
        drupal_set_message( t('Error : User already exists and does attend this course, skipping: ') . $line, 'error' );
        return TRUE;
      }
      else {
        db_query("INSERT INTO {gmlcourse_students2groups}
						  ( group_id, student_id )
						  VALUES ( %d, %d )", 
						  $group['id'], $user_id );

        drupal_set_message( t('Warning : User already exists, course relation was added, user personal info was not modified. Line : ') .
								  $line, 'status' );
        return TRUE;
      }
    }
  }

  // note: no middle name is ok		

  if ( !$id || !$email || !$last_name || !$first_name) {
    drupal_set_message(t('Invalid line (no fields), skipping: ') . $line, 'error');
    return TRUE;
  }

  $default_suspicious_level = 1;

  //generate random pass
  $pass = md5( rand( 1000000, 1000000000 ) ) . md5( $id ) . time();

  $array = array(
    'pass' => $pass,
    'name' => $id,
    $profile_lastname => $last_name,
    $profile_firstname => $first_name,
    $profile_middlename => $middle_name,
    'mail' => $email,
    'status' => 1,
    //	 'roles' => array($student_role_rid),
    'roles' => array($student_role_rid => 'student'),
    'gml_course_groups' => array($in_course_id => $group['id']),
    'gml_course_suspicious_level' => $default_suspicious_level,
  );

  $account = user_save( $newuser, $array );
  $site_name = variable_get('site_name', 'GML');


  if ( $account ) {
    $variables = array(
      '!username' => $id,
      '!site' => $site_name,
      '!password' => $pass,
      '!uri' => $base_url,
      '!uri_brief' => substr($base_url, strlen('http://')),
      '!mailto' => $email,
      '!date' => format_date(time()),
      '!login_uri' => url('user', array('absolute' => TRUE)),
      '!edit_uri' => url('user/' . $account->uid . '/edit', array('absolute' => TRUE)),
      '!login_url' => user_pass_reset_url($account),
    );


    $flag_mail_sent = $language = user_preferred_language($account);

    $mail_subject = _user_mail_text('register_admin_created_subject',   $language, $variables);
    $mail_body = _user_mail_text('register_admin_created_body',   $language, $variables);
    $mail_header = "From: $from\nReply-to: $from\nX-Mailer: Drupal\nReturn-path: $from\nErrors-to: $from";

    $flag_mail_sent = $params = array(
      'subject' => $mail_subject,
      'body' => $mail_body,
    );

    $flag_mail_sent = drupal_mail('gml_course_student', 'send',   $email, $language, $params);

    if ( $flag_mail_sent ) {
      if ( $in_mail_verbose ) {
        drupal_set_message( t('Further registration instructions for the new user %user have been e-mailed to "%email".', 
								array('%user' => $id, '%email' => $email) ) );
      }
    }
    else {
      drupal_set_message( t('Error : Failed to send an email to "') . $email . t('" at line: ') . $line, 'error' );
    }
  }
  else {
    drupal_set_message( t('Error : Failed to save user data, skipping: ') . $line, 'error' );
    return TRUE;
  }
}





/**
 *   Verify student thath student in member of group for given course
 *   
 *   
 */
function student_verify_course_access_by_sc_ids( $in_student_id, $in_course_id ) {
  $result = db_query(
			"SELECT COUNT({gmlcourse_students2groups}.student_id)
				FROM {gmlcourse_students2groups}, {gmlcourse_groups}
				WHERE {gmlcourse_students2groups}.student_id = %d AND
					  {gmlcourse_groups}.id = {gmlcourse_students2groups}.group_id AND
					  {gmlcourse_groups}.course_id = %d", 
						$in_student_id, 	$in_course_id);

  list($key, $val) = each( db_fetch_array($result) );

  if ( intval($val) < 1 ) {
    return false;
  }
  else {
    return true;
  }
}

/**
 *   Verify work belongs for assing to student, who is member of group for given course
 *   
 *   
 */
 function student_verify_work_ownership_by_scaw_ids( $in_student_id, $in_course_id, $in_assign_id, $in_work_id ) {
  $result = db_query(
			"SELECT COUNT({gmlcourse_works}.id)
				FROM {gmlcourse_works}, {gmlcourse_assigns},
					 {gmlcourse_students2groups}, {gmlcourse_groups}
				WHERE {gmlcourse_works}.id = %d AND
					  {gmlcourse_works}.student_id = %d AND
					  {gmlcourse_works}.assign_id = %d AND
					  {gmlcourse_assigns}.id = {gmlcourse_works}.assign_id AND
					  {gmlcourse_assigns}.course_id = %d AND
					  {gmlcourse_students2groups}.student_id = {gmlcourse_works}.student_id AND
					  {gmlcourse_students2groups}.group_id = {gmlcourse_groups}.id AND
					  {gmlcourse_groups}.course_id = {gmlcourse_assigns}.course_id", 
						$in_work_id, $in_student_id, $in_assign_id, 	$in_course_id);

  list($key, $val) = each( db_fetch_array($result) );

  if ( intval($val) < 1 ) {
    return false;
  }
  else {
    return true;
  }
}

/**
 *   Verify work belongs to student, who is member of group for given course
 *   
 *   
 */
function student_verify_work_ownership_by_scw_ids( $in_student_id, $in_course_id, $in_work_id ) {
  $result = db_query(
			"SELECT COUNT({gmlcourse_works}.id)
				FROM {gmlcourse_works}, {gmlcourse_assigns},
					 {gmlcourse_students2groups}, {gmlcourse_groups}
				WHERE {gmlcourse_works}.id = %d AND
					  {gmlcourse_works}.student_id = %d AND
					  {gmlcourse_assigns}.id = {gmlcourse_works}.assign_id AND
					  {gmlcourse_assigns}.course_id = %d AND
					  {gmlcourse_students2groups}.student_id = {gmlcourse_works}.student_id AND
					  {gmlcourse_students2groups}.group_id = {gmlcourse_groups}.id AND
					  {gmlcourse_groups}.course_id = {gmlcourse_assigns}.course_id", 
						$in_work_id, $in_student_id, $in_course_id);

  list($key, $val) = each( db_fetch_array($result) );

  if ( intval($val) < 1 ) {
    return false;
  }
  else {
    return true;
  }
}

/**
 *   Verify work(id) belongs for assing(num) to student(id), who is member of group(id) for given course(id)
 *   
 *   
 */
function student_verify_work_ownership_by_scw_ids_a_num( $in_student_id, $in_course_id, $in_work_id, $in_assign_num ) {
  $result = db_query(
			"SELECT COUNT({gmlcourse_works}.id)
				FROM {gmlcourse_works}, {gmlcourse_assigns},
					 {gmlcourse_students2groups}, {gmlcourse_groups}
				WHERE {gmlcourse_works}.id = %d AND
					  {gmlcourse_works}.student_id = %d AND
					  {gmlcourse_assigns}.id = {gmlcourse_works}.assign_id AND
					  {gmlcourse_assigns}.course_id = %d AND
					  {gmlcourse_assigns}.number = %d AND
					  {gmlcourse_students2groups}.student_id = {gmlcourse_works}.student_id AND
					  {gmlcourse_students2groups}.group_id = {gmlcourse_groups}.id AND
					  {gmlcourse_groups}.course_id = {gmlcourse_assigns}.course_id", 
						$in_work_id, $in_student_id, $in_course_id, $in_assign_num );

  list($key, $val) = each( db_fetch_array($result) );

  if ( intval($val) < 1 ) {
    return false;
  }
  else {
    return true;
  }
}

/**
 *   Verify student access to assing with given number in course with given id
 *   
 *   
 */
function student_verify_course_assign_access($in_student_id, $in_course_id, $in_assign_number, $account = NULL) {
  $result = db_query(
			"SELECT COUNT({gmlcourse_assigns}.id)
				FROM {gmlcourse_assigns},
					 {gmlcourse_students2groups}, {gmlcourse_groups}
				WHERE {gmlcourse_assigns}.course_id = %d AND
					  {gmlcourse_assigns}.number = %d AND
					  {gmlcourse_groups}.course_id = {gmlcourse_assigns}.course_id AND
					  {gmlcourse_students2groups}.group_id = {gmlcourse_groups}.id AND
					  {gmlcourse_students2groups}.student_id = %d", 
						$in_course_id, $in_assign_number, $in_student_id );

  list($key, $val) = each( db_fetch_array($result) );

  if ( intval($val) < 1 ) {
    return false;
  }
  else {
    return true;
  }
}


?>